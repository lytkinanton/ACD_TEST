


/*******************************************************************************
                     WSQ - encoder/decoder.
                     Author S.G.Kalmykov   (SGK)
*******************************************************************************/


#define RELEASE
#define NIST
#define SOURCE_FBI         
#define QUANT

#define  dlt         4
#define  dlt2        8
#define  dltd2       2
#define  dlt4       16

#define number_levels 5


#include <math.h>
#include <stdlib.h>
#include <stdio.h>
//#include <fcntl.h>
#include <string.h>


#include "swsq.h"
#include "common.h"

typedef unsigned char byte;
//---------------------------------------------------------------------------
//#pragma package(smart_init)

typedef unsigned short ushort;
typedef struct {
                 int                x1;
                 int                y1;
                 int                x2;
                 int                y2;
                 float              qk;
                 float           sigma;
                 float       bin_width;
                 float  zero_bin_width;
               }band;

//class __declspec(dllexport) SWSQ
//extern "C" __export int wsq_encode(byte *,byte *,int,int,float,int);
//extern "C" __export int wsq_decode(byte *,byte *,int,int *,int *,int);

//int wsq_encode(byte *,byte *,int,int,float,int);
//int wsq_decode(byte *,byte *,int,int *,int *,int);

class  SWSQ
{
    public:
    SWSQ();
    ~SWSQ();
//   private:
             char str[100],str1[20],str2[20],str3[20],str4[20];

             int cols_wsq,rows_wsq;
             int number_group,LASTK[8],size_int_huff[8];
             int number_bytes[8],size_b_image[8];
             int symbol_HUFFVAL[8][256],count_HUFFVAL[8][256];
             int code_length_HUFFVAL[8][256];
             int fprint,Ev,Sf,version_sgk;
             int MaxCoeff,MaxZRun;
             ushort code_HUFFVAL[8][256];

             byte *stream[8],*_symbol[8],*c_data[8],*d_data;
             ushort *HUFFSIZE[8],*HUFFCODE[8],*EHUFCO[8],*EHUFSI[8];
             int *int_huff[8];
             float *y,*a0,*a1;
             int *quant_table;
             float *buf_float;
             float *f0,*f1,*f2,*f3;
             int   *Asymb,*Acoun,*Apriz,*Anode;
             int *nodeN1,*nodeN2,*nodeL1,*nodeL2;
			 byte *d_buffer;
             byte * _symbol_23;
             byte *_symbol_old;
			 char *ver;

             float compression;
             float bin_width[64],zero_bin_width[64],c_coeff;
             float lof4,lof3,lof2,lof1,lof0,hif3,hif2,hif1,hif0;
             float lof_inv4,lof_inv3,lof_inv2,lof_inv1,lof_inv0;
             float hif_inv4,hif_inv3,hif_inv2,hif_inv1,hif_inv0;

             FILE *hh,*hhh;

             struct Sfilters {
                  int filters_type;                                                                     
                  int length_lo;
                  int length_hi;
                  float hi_inv[32];
                  float lo_inv[32];
                };

             struct subb {
                   int  x1;
                   int  y1;
                   int  x2;
                   int  y2;
                   int eox;
                   int eoy;
                } w[5][48];
             struct gr{
                   int begin;
                   int end;
                   int kh_stream;
                   int kh;
                   int kh0;
                } group[8];
             struct bts{
                   byte L[33];
                } BITS[8];
             struct int_bts{
                   int L[33];
                } INT_BITS;

             band bb[64];

void  MaxCZR(int);
void  analysis(int);
void  synthesis_pp(int,float *,float *,float *,int,Sfilters *,int);
void  synthesis_pe(int,float *,float *,float *,int,Sfilters *,int);
void  synthesis_pp_2(int,float *,float *,float *,int, int);
void  synthesis_pe_2(int,float *,float *,float *,int, int);
//void  synthesis_pp_2(int,float *,float *,float *,int,Sfilters *,int);
//void  synthesis_pe_2(int,float *,float *,float *,int,Sfilters *,int);
void  _subband(int,int,int,int,int,int,int);
void  subband_decomp(float *,int,int,int,int);
int   rotate(float *,int,int,int,int,int,int,int);
int   build_from_subband(float *,int,int,int,int,int,int,float *,float *,float *
                                                                   ,Sfilters *);
void  bin_width_iteration(float *,float);
void  quantization(float *,int,int *);
int   code_symbol(int);
int   code_symbol_compliance(int);
void  qs_2(int *,int *,int,int);
void  qs_3(int *,int *,int *,int,int);
void  qs_4(int *,int *,int *,int *,int,int);
int   probability(int);
void  BBTS(int,int);
int   build_huffman_tables(int);
int   huffman_coding(int);
void  form_bits(int,byte *,int &,int &,byte &,byte &,int &,ushort,int);
int   huffman_coding_compliance(int);
int   huffman_decoding(int);
int   next_bits(int,int,int &,byte *,ushort &,ushort &,int &,int &);
int   huffman_decoding_compliance(int);
int   encoding(byte *,byte *,int,int,float, WORD impNumber);
int   decoding(byte *,byte *,int);
int   wsqCheckSum(byte *,int);
int   jump_along_buf(byte *,int);

   public:
//            int wsq_encode(byte *,byte *,int,int,float,int);
//            int wsq_decode(byte *,byte *,int,int *,int *,int);

};


SWSQ::SWSQ(void)
{
   for(int i=0; i<8; i++)
	  {
               stream[i] = NULL;
			  _symbol[i] = NULL;
			   c_data[i] = NULL;
             HUFFSIZE[i] = NULL;
			 HUFFCODE[i] = NULL;
			   EHUFCO[i] = NULL;
			   EHUFSI[i] = NULL;
             int_huff[i] = NULL;
	  }
   _symbol_23=NULL;
   _symbol_old=NULL;
   ver=NULL;
   d_buffer=NULL;
   d_data=NULL;
   y=NULL;
   a0=NULL;
   a1=NULL;
   quant_table=NULL;
   buf_float=NULL;

   f0  = NULL;
   f1  = NULL;
   f2  = NULL;
   f3  = NULL;

   Asymb=NULL;
   Acoun=NULL;
   Apriz=NULL;
   Anode=NULL;

   nodeN1=NULL;
   nodeN2=NULL;
   nodeL1=NULL;
   nodeL2=NULL;
}
SWSQ::~SWSQ(void)
{
   for(int i=0; i<8; i++)
	  {
	   if(   stream[i]!=NULL ) {
		                         delete []   stream[i];    
								 stream[i] = NULL; 
	                           }
	   if(  _symbol[i]!=NULL ) { 
		                         delete []  _symbol[i];   
								 _symbol[i] = NULL; 
	                           }
	   if(   c_data[i]!=NULL ) {
		                         delete []   c_data[i];    
								 c_data[i] = NULL; 
	                           }
       if( HUFFSIZE[i]!=NULL ) {
		                         delete []  HUFFSIZE[i];  
								 HUFFSIZE[i] = NULL; 
	                           }
	   if( HUFFCODE[i]!=NULL ) { 
		                         delete []  HUFFCODE[i];  
								 HUFFCODE[i] = NULL; 
	                           }
	   if(   EHUFCO[i]!=NULL ) { 
		                         delete []  EHUFCO[i];    
								 EHUFCO[i] = NULL; 
	                           }
	   if(   EHUFSI[i]!=NULL ) { 
		                         delete []   EHUFSI[i];    
								 EHUFSI[i] = NULL; 
	                           }
	   if( int_huff[i]!=NULL ) { 
		                         delete []  int_huff[i];  
								 int_huff[i] = NULL; 
	                           }
	  }
   if( d_data!=NULL ) { 
	                    delete []  d_data;
	  			        d_data = NULL; 
                      }
   if( y!=NULL ) { 
	               delete []  y;
	  			   y = NULL; 
                 }
   if( a0!=NULL ) { 
	                delete []  a0;
					a0 = NULL; 
                  }
   if( a1!=NULL ) { 
	                delete []  a1;
					a1 = NULL; 
                  }
   if( quant_table!=NULL ) { 
	                         delete []  quant_table;  
							 quant_table = NULL; 
                           }
   if(   buf_float!=NULL ) { 
	                         delete []  buf_float;
							 buf_float = NULL; 
                           }
   if ( f0!=NULL ) {
					 delete []  f0;
	                 f0  = NULL;
                   }
   if ( f1!=NULL ) {
					 delete []  f1;
	                 f1  = NULL;
                   }
   if ( f2!=NULL ) {
					 delete []  f2;
	                 f2  = NULL;
                   }
   if ( f3!=NULL ) {
					 delete []  f3;
	                 f3  = NULL;
                   }

   if( Asymb!=NULL )
     {
	   delete []  Asymb;
	   Asymb=NULL;
     }
   if( Acoun!=NULL )
     {
	   delete []  Acoun;
	   Acoun=NULL;
     }
   if( Apriz!=NULL )
     {
	   delete []  Apriz;
	   Apriz=NULL;
     }
   if( Anode!=NULL )
     {
	   delete []  Anode;
	   Anode=NULL;
     }


   if( nodeN1!=NULL )
     {
	   delete []  nodeN1;
	   nodeN1=NULL;
     }
   if( nodeN2!=NULL )
     {
	   delete []  nodeN2;
	   nodeN2=NULL;
     }
   if( nodeL1!=NULL )
     {
	   delete []  nodeL1;
	   nodeL1=NULL;
     }
   if( nodeL2!=NULL )
     {
	   delete []  nodeL2;
	   nodeL2=NULL;
     }
   if( d_buffer!=NULL )
     {
	   delete []  d_buffer;
	   d_buffer=NULL;
     }
   if( _symbol_23!=NULL )
     {
	   delete []  _symbol_23;
	   _symbol_23=NULL;
     }
   if( _symbol_old!=NULL )
     {
	   delete []  _symbol_old;
	   _symbol_old=NULL;
     }
   if( ver!=NULL )
     {
	   delete []  ver;
	   ver=NULL;
     }


}


void SWSQ::MaxCZR(int N_group)
{
    if( N_group==0 )
      {
        MaxCoeff=73;
        MaxZRun=100;
        //MaxCoeff=45;
        //MaxZRun=30;
      }
    else
      {
        MaxCoeff=73;
        MaxZRun=100;
        //MaxCoeff=10;
        //MaxZRun=75;
      }
}
void SWSQ::analysis(int an)
{
 int k;
 float *pti0,*pti1,f;

  pti0=a0-1; pti1=a1-1;

//k=0;
      pti0++; pti1++;
  //n=0; n+k=0;
      f=y[0];      (*pti0)=(f*lof0);      (*pti1)=(f*hif1);
  //n=1; n+k=1;
      f=y[1];      (*pti0)+=(f*lof1);     (*pti1)+=(f*hif0);
  //n=2; n+k=2;
      f=y[2];      (*pti0)+=(f*lof2);     (*pti1)+=(f*hif1);
  //n=3; n+k=3;
      f=y[3];      (*pti0)+=(f*lof3);     (*pti1)+=(f*hif2);
  //n=4; n+k=4;
      f=y[4];      (*pti0)+=(f*lof4);     (*pti1)+=(f*hif3);

//k=2;
      pti0++; pti1++;
  //n=-2; n+k=0;
      f=y[0];      (*pti0)=(f*lof2);      (*pti1)=(f*hif3);
  //n=-1; n+k=1;
      f=y[1];      (*pti0)+=(f*lof1);     (*pti1)+=(f*hif2);
  //n=0; n+k=2;
      f=y[2];      (*pti0)+=(f*lof0);     (*pti1)+=(f*hif1);
  //n=1; n+k=3;
      f=y[3];      (*pti0)+=(f*lof1);     (*pti1)+=(f*hif0);
  //n=2; n+k=4;
      f=y[4];      (*pti0)+=(f*lof2);     (*pti1)+=(f*hif1);
  //n=3; n+k=5;
      f=y[5];      (*pti0)+=(f*lof3);     (*pti1)+=(f*hif2);
  //n=4; n+k=6;
      f=y[6];      (*pti0)+=(f*lof4);     (*pti1)+=(f*hif3);

//k=4;
      pti0++; pti1++;
  //n=-4; n+k=0;
      (*pti0)=(y[0]*lof4);
  //n=-3; n+k=1;
      (*pti0)+=(y[1]*lof3);
  //n=-2; n+k=2;
      f=y[2];      (*pti0)+=(f*lof2);     (*pti1)=(f*hif3);
  //n=-1; n+k=3;
      f=y[3];      (*pti0)+=(f*lof1);     (*pti1)+=(f*hif2);
  //n=0; n+k=4;
      f=y[4];      (*pti0)+=(f*lof0);     (*pti1)+=(f*hif1);
  //n=1; n+k=5;
      f=y[5];      (*pti0)+=(f*lof1);     (*pti1)+=(f*hif0);
  //n=2; n+k=6;
      f=y[6];      (*pti0)+=(f*lof2);     (*pti1)+=(f*hif1);
  //n=3; n+k=7;
      f=y[7];      (*pti0)+=(f*lof3);     (*pti1)+=(f*hif2);
  //n=4; n+k=8;
      f=y[8];      (*pti0)+=(f*lof4);     (*pti1)+=(f*hif3);

  for(k=6; k<=an-5; k+=2)
     {
      pti0++; pti1++;
                                (*pti0)=(y[k-4]*lof4);
                                (*pti0)+=(y[k-3]*lof3);
              f=y[k-2];
                                (*pti0)+=(f*lof2);
                                (*pti1)=(f*hif3);
              f=y[k-1];
                                (*pti0)+=(f*lof1);
                                (*pti1)+=(f*hif2);
              f=y[k];
                                (*pti0)+=(f*lof0);
                                (*pti1)+=(f*hif1);
              f=y[k+1];
                                (*pti0)+=(f*lof1);
                                (*pti1)+=(f*hif0);
              f=y[k+2];
                                (*pti0)+=(f*lof2);
                                (*pti1)+=(f*hif1);
              f=y[k+3];
                                (*pti0)+=(f*lof3);
                                (*pti1)+=(f*hif2);
              f=y[k+4];
                                (*pti0)+=(f*lof4);
                                (*pti1)+=(f*hif3);
     }

//k=an-4;
      pti0++; pti1++;
//n=-4;
     (*pti0)=(y[an-8]*lof4);
//n=-3;
     (*pti0)+=(y[an-7]*lof3);
//n=-2;
     f=y[an-6];     (*pti0)+=(f*lof2);     (*pti1)=(f*hif3);
//n=-1;
     f=y[an-5];     (*pti0)+=(f*lof1);     (*pti1)+=(f*hif2);
//n=0;
     f=y[an-4];     (*pti0)+=(f*lof0);     (*pti1)+=(f*hif1);
//n=1;
     f=y[an-3];     (*pti0)+=(f*lof1);     (*pti1)+=(f*hif0);
//n=2;
     f=y[an-2];     (*pti0)+=(f*lof2);     (*pti1)+=(f*hif1);
//n=3;
     f=y[an-2];     (*pti0)+=(f*lof3);     (*pti1)+=(f*hif2);
//k=an-2;
      pti0++; pti1++;
//n=-4;
     (*pti0)=(y[an-8]*lof4);
//n=-3;
     (*pti0)+=(y[an-7]*lof3);
//n=-2;
     f=y[an-6];     (*pti0)+=(f*lof2);     (*pti1)=(f*hif3);
//n=-1;
     f=y[an-5];     (*pti0)+=(f*lof1);     (*pti1)+=(f*hif2);
//n=0;
     f=y[an-4];     (*pti0)+=(f*lof0);     (*pti1)+=(f*hif1);
//n=1;
     f=y[an-3];     (*pti0)+=(f*lof1);     (*pti1)+=(f*hif0);

//k=an;
      pti0++; pti1++;
//n=-4;
     (*pti0)=(y[an-8]*lof4);
//n=-3;
     (*pti0)+=(y[an-7]*lof3);
//n=-2;
     f=y[an-6];     (*pti0)+=(f*lof2);     (*pti1)=(f*hif3);
//n=-1;
     f=y[an-5];     (*pti0)+=(f*lof1);     (*pti1)+=(f*hif2);
}


void SWSQ::synthesis_pp(int an, float *b0, float *b1, float *x, int d, Sfilters *flt
                                                               ,int decr)
{
  float *b0_,*b1_,*_bb,f_e,f_o,_ff;
  int k,ko,n;
if( (*flt).filters_type!=0 )
  {
     b0_=b0+decr-1;
     b1_=b1+decr-2;
     for(n=0; n<an-2; n+=2)
        {
           
           ko=((*flt).length_hi-1 )>>1;
           f_e= *(b0_+ 1) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               _bb=b0_+ 1;
               _ff=(*flt).lo_inv[(k<<1)];
               f_e += ((*(_bb-k)+*(_bb+k))*_ff);
              }
           ko=(*flt).length_lo>>1;
           for(k=0; k<ko; k++)
              {
               _ff=(*flt).hi_inv[1+(k<<1)];
               f_e += ((*(b1_+ 1 -k)+*(b1_+ 2 +k))*_ff);
              }
           *x=f_e;
           x++;

          
           ko=((*flt).length_lo-1 )>>1;
           f_o= *(b1_+ 2) * (*flt).hi_inv[0];
           for(k=1; k<=ko;  k++)
              {
               _bb=b1_+ 2;
               _ff=(*flt).hi_inv[(k<<1)];
               f_o += ((*(_bb-k)+*(_bb+k))*_ff);
              }
           ko=(*flt).length_hi>>1;
           for(k=0; k<ko; k++)
              {
               _ff=(*flt).lo_inv[1+(k<<1)];
               f_o += ((*(b0_+ 1 -k)+*(b0_+ 2 +k))*_ff);
              }

           *x=f_o;
           x++;

           b0_++;  b1_++;
        }
           
           ko=((*flt).length_hi-1 )>>1;
           f_e= *(b0_+ 1) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               _ff=(*flt).lo_inv[(k<<1)];
               _bb=b0_+ 1;
               f_e += ((*(_bb - k)+*(_bb + k))*_ff);
              }
           ko=(*flt).length_lo>>1;
           for(k=0; k<ko; k++)
              {
               _ff=(*flt).hi_inv[1+(k<<1)];
               f_e += ((*(b1_+ 1 -k)+*(b1_+ 2 +k))*_ff);
              }

           *x=f_e;
           x++;

     if( d!=1 ){
                
                 ko=((*flt).length_lo-1 )>>1;
                 f_o= *(b1_+ 2) * (*flt).hi_inv[0];
                 for(k=1; k<=ko;  k++)
                    {
                      _bb=b1_+ 2;
                      _ff=(*flt).hi_inv[(k<<1)];
                      f_o += ((*(_bb-k)+*(_bb+k))*_ff);
                    }
                 ko=(*flt).length_hi>>1;
                 for(k=0; k<ko; k++)
                    {
                      _ff=(*flt).lo_inv[1+(k<<1)];
                      f_o += ((*(b0_+ 1 -k)+*(b0_+ 2 +k))*_ff);
                    }
                 *x=f_o;
               }
  }
else                      
  {
     int kf,dd;
     b0_=b0+decr-2;
     b1_=b1+decr-2;
     for(n=0; n<an-2; n+=2)
        {
           

           ko=(*flt).length_hi; ko>>=1; dd=ko;
           kf=1;
           f_e=*(b0_ + ko) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               f_e = f_e +*(b0_ + ko-k) * (*flt).lo_inv[kf];
               f_e = f_e +*(b0_ + ko+k) * (*flt).lo_inv[kf+1];
               kf+=2;
              }

           ko=(*flt).length_lo; ko>>=1;
           f_e-=(*(b1_ + dd) * (*flt).hi_inv[0]);
           kf=1;
           for(k=1; k<=ko; k++)
              {
               f_e = f_e +*(b1_ + dd-k) * (*flt).hi_inv[kf];
               f_e = f_e -*(b1_ + dd+k) * (*flt).hi_inv[kf+1];
               kf+=2;
              }

           *x=f_e;
           x++;
           
           ko=(*flt).length_hi; ko>>=1; dd=ko;
           kf=2;
           f_o=*(b0_ + ko) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               f_o = f_o +*(b0_ + ko-k) * (*flt).lo_inv[kf];
               f_o = f_o +*(b0_ + ko+k) * (*flt).lo_inv[kf-1];
               kf+=2;
              }

           ko=(*flt).length_lo; ko>>=1;
           f_o+=(*(b1_ + dd) * (*flt).hi_inv[0]);
           kf=2;
           for(k=1; k<=ko; k++)
              {
               f_o = f_o +*(b1_ + dd-k) * (*flt).hi_inv[kf];
               f_o = f_o -*(b1_ + dd+k) * (*flt).hi_inv[kf-1];
               kf+=2;
              }

           *x=f_o;
           x++;
           b0_++;  b1_++;
        }
           

           ko=(*flt).length_hi; ko>>=1; dd=ko;
           kf=1;
           f_e=*(b0_ + ko) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               f_e = f_e +*(b0_ + ko-k) * (*flt).lo_inv[kf];
               f_e = f_e +*(b0_ + ko+k) * (*flt).lo_inv[kf+1];
               kf+=2;
              }

           ko=(*flt).length_lo; ko>>=1;
           f_e-=(*(b1_ + dd) * (*flt).hi_inv[0]);
           kf=1;
           for(k=1; k<=ko; k++)
              {
               f_e = f_e +*(b1_ + dd-k) * (*flt).hi_inv[kf];
               f_e = f_e -*(b1_ + dd+k) * (*flt).hi_inv[kf+1];
               kf+=2;
              }

           *x=f_e;
           x++;
     if( d!=1 ){
           
           ko=(*flt).length_hi; ko>>=1; dd=ko;
           kf=2;
           f_o=*(b0_ + ko) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               f_o = f_o +*(b0_ + ko-k) * (*flt).lo_inv[kf];
               f_o = f_o +*(b0_ + ko+k) * (*flt).lo_inv[kf-1];
               kf+=2;
              }

           ko=(*flt).length_lo; ko>>=1;
           f_o+=(*(b1_ + dd) * (*flt).hi_inv[0]);
           kf=2;
           for(k=1; k<=ko; k++)
              {
               f_o = f_o +*(b1_ + dd-k) * (*flt).hi_inv[kf];
               f_o = f_o -*(b1_ + dd+k) * (*flt).hi_inv[kf-1];
               kf+=2;
              }

           *x=f_o;
           x++;
               }
  }
}

void SWSQ::synthesis_pe(int an, float *b0, float *b1, float *x, int d, Sfilters *flt
                                                               ,int decr)
{
  float *b0_,*b1_,*_bb,f_e,f_o,_ff;
  int k,ko,n;

if( (*flt).filters_type!=0 )        
  {
     b0_=b0+decr-1;
     b1_=b1+decr-2;
     for(n=0; n<an-2; n+=2)
        {
          
           ko=((*flt).length_hi-1 )>>1;
           f_e= *(b0_+ 1) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               _bb=b0_+1;
               _ff=(*flt).lo_inv[(k<<1)];
               f_e += ((*(_bb-k)+*(_bb+k))*_ff);
              }
           ko=(*flt).length_lo>>1;
           for(k=0; k<ko; k++)
              {
               _ff=(*flt).hi_inv[1+(k<<1)];
               f_e += ((*(b1_+ 1 -k)+*(b1_+ 2 +k))*_ff);
              }
           x+=cols_wsq;
           *x=f_e;

          
           ko=((*flt).length_lo-1 )>>1;
           f_o= *(b1_+ 2) * (*flt).hi_inv[0];
           for(k=1; k<=ko;  k++)
              {
               _bb=b1_+ 2;
               _ff=(*flt).hi_inv[(k<<1)];
               f_o += ((*(_bb-k)+*(_bb+k))*_ff);
              }
           ko=(*flt).length_hi>>1;
           for(k=0; k<ko; k++)
              {
               _ff=(*flt).lo_inv[1+(k<<1)];
               f_o += ((*(b0_+ 1 -k)+*(b0_+ 2 +k))*_ff);
              }

           x+=cols_wsq;
           *x=f_o;

           b0_++;  b1_++;
        }
          
           ko=((*flt).length_hi-1 )>>1;
           f_e= *(b0_+ 1) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               _bb=b0_+ 1;
               _ff=(*flt).lo_inv[k<<1];
               f_e += (*(_bb-k)+*(_bb+k))*_ff;
              }
           ko=(*flt).length_lo>>1;
           for(k=0; k<ko; k++)
              {
               _ff=(*flt).hi_inv[1+(k<<1)];
               f_e += ((*(b1_+ 1 -k)+*(b1_+ 2 +k))*_ff);
              }

           x+=cols_wsq;
           *x=f_e;

     if( d!=1 ){
                 
                 ko=((*flt).length_lo-1 )>>1;
                 f_o= *(b1_+ 2) * (*flt).hi_inv[0];
                 for(k=1; k<=ko;  k++)
                    {
                       _bb=b1_+ 2;
                       _ff=(*flt).hi_inv[0+2*k];
                       f_o += ((*(_bb-k)+*(_bb+k))*_ff);
                    }
                 ko=(*flt).length_hi>>1;
                 for(k=0; k<ko; k++)
                    {
                     _ff=(*flt).lo_inv[1+(k<<1)];
                     f_o += ((*(b0_+ 1 -k)+*(b0_+ 2 +k))*_ff);
                    }
                 x+=cols_wsq;
                 *x=f_o;
               }
  }
else                        
  {
//     int kf,dd,sign;
     int kf,dd;
     b0_=b0+decr-2;
     b1_=b1+decr-2;
     for(n=0; n<an-2; n+=2)
        {
           
           ko=(*flt).length_hi; ko>>=1; dd=ko;
           kf=1;
           f_e=*(b0_ + ko) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               f_e = f_e +*(b0_ + ko-k) * (*flt).lo_inv[kf];
               f_e = f_e +*(b0_ + ko+k) * (*flt).lo_inv[kf+1];
               kf+=2;
              }

           ko=(*flt).length_lo; ko>>=1;
           f_e-=(*(b1_ + dd) * (*flt).hi_inv[0]);
           kf=1;
           for(k=1; k<=ko; k++)
              {
               f_e = f_e +*(b1_ + dd-k) * (*flt).hi_inv[kf];
               f_e = f_e -*(b1_ + dd+k) * (*flt).hi_inv[kf+1];
               kf+=2;
              }
           x+=cols_wsq;
           *x=f_e;
          
           ko=(*flt).length_hi; ko>>=1; dd=ko;
           kf=2;
           f_o=*(b0_ + ko) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               f_o = f_o +*(b0_ + ko-k) * (*flt).lo_inv[kf];
               f_o = f_o +*(b0_ + ko+k) * (*flt).lo_inv[kf-1];
               kf+=2;
              }

           ko=(*flt).length_lo; ko>>=1;
           f_o+=(*(b1_ + dd) * (*flt).hi_inv[0]);
           kf=2;
           for(k=1; k<=ko; k++)
              {
               f_o = f_o +*(b1_ + dd-k) * (*flt).hi_inv[kf];
               f_o = f_o -*(b1_ + dd+k) * (*flt).hi_inv[kf-1];
               kf+=2;
              }

           x+=cols_wsq;
           *x=f_o;

           b0_++;  b1_++;

        }
          
           ko=(*flt).length_hi; ko>>=1; dd=ko;
           kf=1;
           f_e=*(b0_ + ko) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               f_e = f_e +*(b0_ + ko-k) * (*flt).lo_inv[kf];
               f_e = f_e +*(b0_ + ko+k) * (*flt).lo_inv[kf+1];
               kf+=2;
              }

           ko=(*flt).length_lo; ko>>=1;
           f_e-=(*(b1_ + dd) * (*flt).hi_inv[0]);
           kf=1;
           for(k=1; k<=ko; k++)
              {
               f_e = f_e +*(b1_ + dd-k) * (*flt).hi_inv[kf];
               f_e = f_e -*(b1_ + dd+k) * (*flt).hi_inv[kf+1];
               kf+=2;
              }
           x+=cols_wsq;
           *x=f_e;
     if( d!=1 ){
           
           ko=(*flt).length_hi; ko>>=1; dd=ko;
           kf=2;
           f_o=*(b0_ + ko) * (*flt).lo_inv[0];
           for(k=1; k<=ko;  k++)
              {
               f_o = f_o +*(b0_ + ko-k) * (*flt).lo_inv[kf];
               f_o = f_o +*(b0_ + ko+k) * (*flt).lo_inv[kf-1];
               kf+=2;
              }

           ko=(*flt).length_lo; ko>>=1;
           f_o+=(*(b1_ + dd) * (*flt).hi_inv[0]);
           kf=2;
           for(k=1; k<=ko; k++)
              {
               f_o = f_o +*(b1_ + dd-k) * (*flt).hi_inv[kf];
               f_o = f_o -*(b1_ + dd+k) * (*flt).hi_inv[kf-1];
               kf+=2;
              }

           x+=cols_wsq;
           *x=f_o;
               }
  }
}




//////////////////////////////////////////////////////////////////////////////
void SWSQ::synthesis_pp_2(int an, float *b0, float *b1, float *x, int d ,int decr)
{

                        *x=
//                        b0[2]*lof_inv0+
//                        (b0[3]+b0[1])*lof_inv2+
//                        (b1[1]+b1[2])*hif_inv1+
//                        (b1[0]+b1[3])*hif_inv3;
                        b0[decr]*lof_inv0+
                        (b0[decr+1]+b0[decr-1])*lof_inv2+
                        (b1[decr-1]+b1[decr])*hif_inv1+
                        (b1[decr-2]+b1[decr+1])*hif_inv3;

                        x++;
                        *x=
//                        (b0[2]+b0[3])*lof_inv1+
//                        (b0[1]+b0[4])*lof_inv3+
//                        b1[2]*hif_inv0+
//                        (b1[1]+b1[3])*hif_inv2+
//                        (b1[0]+b1[4])*hif_inv4;
                        (b0[decr]+b0[decr+1])*lof_inv1+
                        (b0[decr-1]+b0[decr+2])*lof_inv3+
                        b1[decr]*hif_inv0+
                        (b1[decr-1]+b1[decr+1])*hif_inv2+
                        (b1[decr-2]+b1[decr+2])*hif_inv4;

                        x++;
float *b0_,*b1_;
//  b0_=b0+2;
//  b1_=b1+1;
  b0_=b0+decr;
  b1_=b1+decr-1;
  for(int n=2; n<an-2; n+=2)
     {
           *x= ( *(b1_+1) + *(b1_+2) ) * hif_inv1+
               ( *b1_     + *(b1_+3) ) * hif_inv3+
               (            *(b0_+1) ) * lof_inv0+
               ( *b0_     + *(b0_+2) ) * lof_inv2;
           x++;
           *x= (            *(b1_+2) ) * hif_inv0+
               ( *(b1_+1) + *(b1_+3) ) * hif_inv2+
               ( *b1_     + *(b1_+4) ) * hif_inv4+
               ( *(b0_+1) + *(b0_+2) ) * lof_inv1+
               ( *b0_     + *(b0_+3) ) * lof_inv3;
           x++;
           b0_++;  b1_++;
     }
           *x= ( *(b1_+1) + *(b1_+2) ) * hif_inv1+
               ( *b1_     + *(b1_+3) ) * hif_inv3+
               (            *(b0_+1) ) * lof_inv0+
               ( *b0_     + *(b0_+2) ) * lof_inv2;
           x++;
if( d!=1 ){
           *x= (            *(b1_+2) ) * hif_inv0+
               ( *(b1_+1) + *(b1_+3) ) * hif_inv2+
               ( *b1_     + *(b1_+4) ) * hif_inv4+
               ( *(b0_+1) + *(b0_+2) ) * lof_inv1+
               ( *b0_     + *(b0_+3) ) * lof_inv3;
          }

}

void SWSQ::synthesis_pe_2(int an, float *b0, float *b1, float *x, int d, int decr)
{

                        x+=cols_wsq;
                        *x=
//                        b0[2]*lof_inv0+
//                        (b0[3]+b0[1])*lof_inv2+
//                        (b1[1]+b1[2])*hif_inv1+
//                        (b1[0]+b1[3])*hif_inv3;
                        b0[decr]*lof_inv0+
                        (b0[decr+1]+b0[decr-1])*lof_inv2+
                        (b1[decr-1]+b1[decr])*hif_inv1+
                        (b1[decr-2]+b1[decr+1])*hif_inv3;

                        x+=cols_wsq;
                        *x=
//                        (b0[2]+b0[3])*lof_inv1+
//                        (b0[1]+b0[4])*lof_inv3+
//                        b1[2]*hif_inv0+
//                        (b1[1]+b1[3])*hif_inv2+
//                        (b1[0]+b1[4])*hif_inv4;
                        (b0[decr]+b0[decr+1])*lof_inv1+
                        (b0[decr-1]+b0[decr+2])*lof_inv3+
                        b1[decr]*hif_inv0+
                        (b1[decr-1]+b1[decr+1])*hif_inv2+
                        (b1[decr-2]+b1[decr+2])*hif_inv4;

float *b0_,*b1_;
//  b0_=b0+2;
//  b1_=b1+1;
  b0_=b0+decr;
  b1_=b1+decr-1;

      for(int n=2; n<an-2; n+=2)
         {
           x+=cols_wsq;
           *x= ( *(b1_+1) + *(b1_+2) ) * hif_inv1+
               ( *b1_     + *(b1_+3) ) * hif_inv3+
               (            *(b0_+1) ) * lof_inv0+
               ( *b0_     + *(b0_+2) ) * lof_inv2;

           x+=cols_wsq;
           *x= (            *(b1_+2) ) * hif_inv0+
               ( *(b1_+1) + *(b1_+3) ) * hif_inv2+
               ( *b1_     + *(b1_+4) ) * hif_inv4+
               ( *(b0_+1) + *(b0_+2) ) * lof_inv1+
               ( *b0_     + *(b0_+3) ) * lof_inv3;

           b0_++;  b1_++;
         }

           x+=cols_wsq;
           *x= ( *(b1_+1) + *(b1_+2) ) * hif_inv1+
               ( *b1_     + *(b1_+3) ) * hif_inv3+
               (            *(b0_+1) ) * lof_inv0+
               ( *b0_     + *(b0_+2) ) * lof_inv2;

if(d!=1){
           x+=cols_wsq;
           *x= (            *(b1_+2) ) * hif_inv0+
               ( *(b1_+1) + *(b1_+3) ) * hif_inv2+
               ( *b1_     + *(b1_+4) ) * hif_inv4+
               ( *(b0_+1) + *(b0_+2) ) * lof_inv1+
               ( *b0_     + *(b0_+3) ) * lof_inv3;

           b0_++;  b1_++;
        }

}
//////////////////////////////////////////////////////////////////////////////
void SWSQ::_subband(int x1,int y1, int x2, int y2, int level, int zone, int type)
{
  int dltx,dlty,dx=x2-x1,dy=y2-y1;

if( (dx%2)==0 ) dltx=0;
           else dltx=1;
if( (dy%2)==0 ) dlty=0;
           else dlty=1;

if( level>0 )
  {
         switch(type)
         {
           case 0:
                   dltx=1;    dlty=1;
                   break;
           case 1:
                   dltx=1;    dlty=-1;
                   break;
           case 2:
                   dltx=-1;   dlty=1;
                   break;
           case 3:
                   dltx=-1;   dlty=-1;
                   break;
          default:
                   break;
         }
  } else level=-level;
  if( (dx%2)==0 ) {
                           w[level][zone  ].x1=x1;
                           w[level][zone  ].x2=x1+(dx>>1);

                           w[level][zone+1].x1=x1+(dx>>1);
                           w[level][zone+1].x2=x2;

                           w[level][zone+2].x1=x1;
                           w[level][zone+2].x2=x1+(dx>>1);

                           w[level][zone+3].x1=x1+(dx>>1);
                           w[level][zone+3].x2=x2;
                           w[level][zone  ].eox=2;
                           w[level][zone+1].eox=2;
                           w[level][zone+2].eox=2;
                           w[level][zone+3].eox=2;
                  }
             else {
                           w[level][zone  ].x1=x1;
                           w[level][zone  ].x2=x1+((dx+dltx)>>1);

                           w[level][zone+1].x1=x1+((dx+dltx)>>1);
                           w[level][zone+1].x2=x2;

                           w[level][zone+2].x1=x1;
                           w[level][zone+2].x2=x1+((dx+dltx)>>1);

                           w[level][zone+3].x1=x1+((dx+dltx)>>1);
                           w[level][zone+3].x2=x2;
                           w[level][zone  ].eox=1;
                           w[level][zone+1].eox=1;
                           w[level][zone+2].eox=1;
                           w[level][zone+3].eox=1;
                  }
  if( (dy%2)==0 ) {
                           w[level][zone  ].y1=y1;
                           w[level][zone  ].y2=y1+(dy>>1);

                           w[level][zone+1].y1=y1;
                           w[level][zone+1].y2=y1+(dy>>1);

                           w[level][zone+2].y1=y1+(dy>>1);
                           w[level][zone+2].y2=y2;

                           w[level][zone+3].y1=y1+(dy>>1);
                           w[level][zone+3].y2=y2;
                           w[level][zone  ].eoy=2;
                           w[level][zone+1].eoy=2;
                           w[level][zone+2].eoy=2;
                           w[level][zone+3].eoy=2;
                  }
             else {
                           w[level][zone  ].y1=y1;
                           w[level][zone  ].y2=y1+((dy+dlty)>>1);

                           w[level][zone+1].y1=y1;
                           w[level][zone+1].y2=y1+((dy+dlty)>>1);

                           w[level][zone+2].y1=y1+((dy+dlty)>>1);
                           w[level][zone+2].y2=y2;

                           w[level][zone+3].y1=y1+((dy+dlty)>>1);
                           w[level][zone+3].y2=y2;
                           w[level][zone  ].eoy=1;
                           w[level][zone+1].eoy=1;
                           w[level][zone+2].eoy=1;
                           w[level][zone+3].eoy=1;
                  }
}

void SWSQ::subband_decomp(float *buf_float,int x1,int y1, int x2, int y2)
{
  int dx=x2-x1,dy=y2-y1,cd2,rd2,length,i,j,k2;
  float *pti_f,*p,*p_,*pp,*pp_,*ooo,*ooo_;
  int gx=(dx<<1)+dlt2-2-dx;

  if( (dx%2)==0 )   cd2=dx>>1;
            else    cd2=(dx+1)>>1;
  if( (dy%2)==0 )   rd2=dy>>1;
            else    rd2=(dy+1)>>1;

  k2=dltd2-cd2;
  if( (dx%2)==0 )   length=dx+dlt4-2;
             else   length=dx+dlt4-1;
  p_=y+dlt-1;
  ooo = buf_float+(y1-1)*cols_wsq-1+x1;
  for(i=y1; i<y2; i++)
     {
       ooo += cols_wsq;
       pti_f=ooo;
       p=p_;
       for(j=x1; j<x2; j++)
          {
            pti_f++;
            p++;
            *p=*pti_f;
          }



           y[3]=y[5];
           y[2]=y[6];
           y[1]=y[7];
           y[0]=y[8];



           pp_ = y+dx+4;
           pp  = y+gx-4;
           *pp_=*pp;
           *(pp_+1)=*(pp-1);
           *(pp_+2)=*(pp-2);
           *(pp_+3)=*(pp-3);

       analysis(length);

       pti_f=ooo;
       for(j=dltd2; j<cd2+dltd2; j++)
          {
           pti_f++;
           *pti_f=a0[j];
          }

       pti_f=ooo+cd2;
       for(j=cd2+k2; j<dx+k2; j++)
          {
           pti_f++;
           *pti_f=a1[j];
          }
     }
//______________________________________________________________________________

  k2=dltd2-rd2;
  if( (dy%2)==0 )   length=dy+dlt4-2;   //length=(rd2-1+dlt2)<<1;
             else   length=dy+dlt4-1;   //length=(rd2-1+dlt2)<<1;
  p_=y+dlt-1;
  ooo_ = buf_float + (y1-1)*cols_wsq;
  for(j=x1; j<x2; j++)
     {
       p = p_;
       ooo = ooo_+j;
       for(i=y1; i<y2; i++)
          {
            p++;
            ooo += cols_wsq;
            *p=*ooo;
          }



           y[3]=y[5];
           y[2]=y[6];
           y[1]=y[7];
           y[0]=y[8];



           y[dy+4]=y[dy+2];
           y[dy+5]=y[dy+1];
           y[dy+6]=y[dy  ];
           y[dy+7]=y[dy-1];

       analysis(length);

       for(i=y1; i<y1+rd2; i++)
           *(buf_float+i*cols_wsq+j)=a0[i+dltd2-y1];
       for(i=rd2; i<dy; i++)
           *(buf_float+(i+y1)*cols_wsq+j)=a1[i+k2];
     }
}

int SWSQ::rotate(float *buf_float,int x1,int y1,int x2,int y2,int level,int zone,int type)
{

  //float *f0,*f1,*f2,*f3;
  float *ind;

  f0  = NULL;
  f1  = NULL;
  f2  = NULL;
  f3  = NULL;
  ind = NULL;

  int i,j,k,dx=(x2-x1),dy=(y2-y1);


  if( level>0 && type!=0 )
    {
      f0 = new float[dx*dy+40];
      if(f0==NULL)  return -1;
             ind=f0;
             k=(w[level][zone].y1-1)*cols_wsq;
             x1=k+w[level][zone].x1;  x2=k+w[level][zone].x2;
             for(j=w[level][zone].y1; j<w[level][zone].y2; j++)
                {
                  x1+=cols_wsq;
                  x2+=cols_wsq;
                  for(i=x1; i<x2; i++)
                     {
                       *ind = *(buf_float+i);
                       ind++;
                     }
                }

      f1 = new float[dx*dy+40];
      if(f1==NULL)  {
                      delete []  f0;  f0=NULL;
                      return -1;
                    }
             ind=f1;
             k=(w[level][zone+1].y1-1)*cols_wsq;
                  x1=k+w[level][zone+1].x1;  x2=k+w[level][zone+1].x2;
             for(j=w[level][zone+1].y1; j<w[level][zone+1].y2; j++)
                {
                  x1+=cols_wsq;
                  x2+=cols_wsq;
                  for(i=x1; i<x2; i++)
                     {
                       *ind = *(buf_float+i);
                       ind++;
                     }
                }

      f2 = new float[dx*dy+40];
      if(f2==NULL)  {
                      delete []  f0;  f0=NULL;
                      delete []  f1;  f1=NULL;
                      return -1;
                    }
             ind=f2;
             k=(w[level][zone+2].y1-1)*cols_wsq;
             x1=k+w[level][zone+2].x1;  x2=k+w[level][zone+2].x2;
             for(j=w[level][zone+2].y1; j<w[level][zone+2].y2; j++)
                {
                  x1+=cols_wsq;
                  x2+=cols_wsq;
                  for(i=x1; i<x2; i++)
                     {
                       *ind = *(buf_float+i);
                       ind++;
                     }
                }

      f3 = new float[dx*dy+40];
      if(f3==NULL)  {
                      delete []  f0;  f0=NULL;
                      delete []  f1;  f1=NULL;
                      delete []  f2;  f2=NULL;
                      return -1;
                    }
             ind=f3;
             k=(w[level][zone+3].y1-1)*cols_wsq;
             x1=k+w[level][zone+3].x1;  x2=k+w[level][zone+3].x2;
             for(j=w[level][zone+3].y1; j<w[level][zone+3].y2; j++)
                {
                  x1+=cols_wsq;
                  x2+=cols_wsq;
                  for(i=x1; i<x2; i++)
                     {
                       *ind = *(buf_float+i);
                       ind++;
                     }
                }
int jf,i1,i2;
      switch(type)
        {
           case 1:
//////////////////////////////////////////////////////////////////////////////
             ind=f2;
             dy=w[level][zone+0].y2-w[level][zone+0].y1;
             jf=(w[level][zone+2].y1-1-dy)*cols_wsq;
             i1=w[level][zone+2].x1+jf;
             i2=w[level][zone+2].x2+jf;
             for(j=w[level][zone+2].y1; j<w[level][zone+2].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }
             ind=f0;
             dy=w[level][zone+2].y2-w[level][zone+2].y1;
             jf=(w[level][zone+0].y1-1+dy)*cols_wsq;
             i1=w[level][zone+0].x1+jf;
             i2=w[level][zone+0].x2+jf;
             for(j=w[level][zone+0].y1; j<w[level][zone+0].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }

             ind=f3;
             dy=w[level][zone+1].y2-w[level][zone+1].y1;
             jf=(w[level][zone+3].y1-1-dy)*cols_wsq;
             i1=w[level][zone+3].x1+jf;
             i2=w[level][zone+3].x2+jf;
             for(j=w[level][zone+3].y1; j<w[level][zone+3].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }
             ind=f1;
             dy=w[level][zone+3].y2-w[level][zone+3].y1;
             jf=(w[level][zone+1].y1-1+dy)*cols_wsq;
             i1=w[level][zone+1].x1+jf;
             i2=w[level][zone+1].x2+jf;
             for(j=w[level][zone+1].y1; j<w[level][zone+1].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }
//////////////////////////////////////////////////////////////////////////////

                    break;
           case 2:
//////////////////////////////////////////////////////////////////////////////
             ind=f1;
             dx=w[level][zone+0].x2-w[level][zone+0].x1;
             jf=(w[level][zone+1].y1-1)*cols_wsq -dx;
             i1=w[level][zone+1].x1+jf;
             i2=w[level][zone+1].x2+jf;
             for(j=w[level][zone+1].y1; j<w[level][zone+1].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }

             ind=f0;
             dx=w[level][zone+1].x2-w[level][zone+1].x1;
             jf=(w[level][zone+0].y1-1)*cols_wsq+dx;
             i1=w[level][zone+0].x1+jf;
             i2=w[level][zone+0].x2+jf;
             for(j=w[level][zone+0].y1; j<w[level][zone+0].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }

             ind=f3;
             dx=w[level][zone+2].x2-w[level][zone+2].x1;
             jf=(w[level][zone+3].y1-1)*cols_wsq-dx;
             i1=w[level][zone+3].x1+jf;
             i2=w[level][zone+3].x2+jf;
             for(j=w[level][zone+3].y1; j<w[level][zone+3].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }

             ind=f2;
             dx=w[level][zone+3].x2-w[level][zone+3].x1;
             jf=(w[level][zone+2].y1-1)*cols_wsq+dx;
             i1=w[level][zone+2].x1+jf;
             i2=w[level][zone+2].x2+jf;
             for(j=w[level][zone+2].y1; j<w[level][zone+2].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }
//////////////////////////////////////////////////////////////////////////////
                    break;
           case 3:

//////////////////////////////////////////////////////////////////////////////
             ind=f3;
             dx=w[level][zone+0].x2-w[level][zone+0].x1;
             dy=w[level][zone+0].y2-w[level][zone+0].y1;
             jf=(w[level][zone+3].y1-1-dy)*cols_wsq-dx;
             i1=w[level][zone+3].x1+jf;
             i2=w[level][zone+3].x2+jf;
             for(j=w[level][zone+3].y1; j<w[level][zone+3].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }

             ind=f0;
             dx=w[level][zone+3].x2-w[level][zone+3].x1;
             dy=w[level][zone+3].y2-w[level][zone+3].y1;
             jf=(w[level][zone+0].y1-1+dy)*cols_wsq+dx;
             i1=w[level][zone+0].x1+jf;
             i2=w[level][zone+0].x2+jf;
             for(j=w[level][zone+0].y1; j<w[level][zone+0].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }

             ind=f2;
             dx=w[level][zone+1].x2-w[level][zone+1].x1;
             dy=w[level][zone+1].y2-w[level][zone+1].y1;
             jf=(w[level][zone+2].y1-1-dy)*cols_wsq+dx;
             i1=w[level][zone+2].x1+jf;
             i2=w[level][zone+2].x2+jf;
             for(j=w[level][zone+2].y1; j<w[level][zone+2].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }
             ind=f1;
             dx=w[level][zone+2].x2-w[level][zone+2].x1;
             dy=w[level][zone+2].y2-w[level][zone+2].y1;
             jf=(w[level][zone+1].y1-1+dy)*cols_wsq-dx;
             i1=w[level][zone+1].x1+jf;
             i2=w[level][zone+1].x2+jf;
             for(j=w[level][zone+1].y1; j<w[level][zone+1].y2; j++)
                {
                  i1+=cols_wsq;
                  i2+=cols_wsq;
                  for(i=i1; i<i2; i++)
                     {
                       *(buf_float+i)=*ind;
                       ind++;
                     }
                }

                    break;
        }
      delete []  f0;  f0=NULL;
      delete []  f1;  f1=NULL;
      delete []  f2;  f2=NULL;
      delete []  f3;  f3=NULL;

    }
  return 0;
}

int SWSQ::build_from_subband(float *buf_float,int x1,int y1, int x2, int y2,
      int level, int zone, float *x, float *b0, float *b1, Sfilters *flt)
{
  int dx=x2-x1,dy=y2-y1;
  int cd2,rd2;
  float *ooo,*oo0,*oo,*o0;
  int length,i,j,kk=0,wzx,wzy;
  float *old=x;
  int decr=2;

  if( (*flt).length_lo > (*flt).length_hi )   decr=(*flt).length_lo/2;
                                       else   decr=(*flt).length_hi/2;
  if( decr%2 ) decr++;
////////////////////////////////////////////////////////////////////////////////
  decr++;
////////////////////////////////////////////////////////////////////////////////
int decr2=decr<<1;
  cd2=dx>>1;
  rd2=dy>>1;

  wzx=abs(w[level][zone].eox);
  wzy=abs(w[level][zone].eoy);

  if( wzx==1 ) cd2++;
  if( wzy==1 ) rd2++;

  kk=dy-rd2;

  length=dy;
o0=buf_float+(y1-1)*cols_wsq;
int rd2clswsq=rd2*cols_wsq;
if( (*flt).filters_type!=0 )      
  {

for(j=x1; j<x2; j++)
{
                 oo0=o0+j;
                 for(i=decr; i<kk+decr; i++)
                    {
                      oo0+=cols_wsq;
                      b0[i]=*oo0;
                      b1[i]=*(oo0+rd2clswsq);
                    }
    if( wzy==1 ) {
                     oo0+=cols_wsq;
                     b0[i]=*oo0;



                   for(i=0; i<decr; i++)
//                       b0[i] = b0[2*decr-i];
                       b0[i] = b0[decr2-i];



                   for(i=0; i<decr; i++)
                       b0[rd2+decr+i] = b0[rd2+decr-2-i];



                   for(i=0; i<decr; i++)
//                       b1[i] = b1[2*decr-i-1];
                       b1[i] = b1[decr2-i-1];



                   for(i=0; i<decr; i++)
                       b1[kk+decr+i] = b1[kk+decr-1-i];
                 }
           else  {


                   for(i=0; i<decr; i++)
//                       b0[i] = b0[2*decr-i];
                       b0[i] = b0[decr2-i];



                   for(i=0; i<decr; i++)
                       b0[rd2+decr+i] = b0[rd2+decr-1-i];



                   for(i=0; i<decr; i++)
//                       b1[i] = b1[2*decr-i-1];
                       b1[i] = b1[decr2-i-1];



                   for(i=0; i<decr; i++)
                       b1[kk+decr+i] = b1[kk+decr-2-i];
                 }
x=o0+j;
//if( (*flt).filters_type!=2 )  synthesis_pe(length,b0,b1,x,wzy,flt,decr);
//                        else  synthesis_pe_2(length,b0,b1,x,wzy,flt,decr);
if( (*flt).filters_type!=2 )  synthesis_pe(length,b0,b1,x,wzy,flt,decr);
                        else  synthesis_pe_2(length,b0,b1,x,wzy,decr);

}
  }
else      
  {
for(j=x1; j<x2; j++)
{
                 oo0=o0+j;
                 for(i=decr; i<kk+decr; i++)
                    {
                      oo0+=cols_wsq;
                      b0[i]=*oo0;
                      b1[i]=*(oo0+rd2clswsq);
                    }
    if( wzy==1 ) {
                     oo0+=cols_wsq;
                     b0[i]=*oo0;



                   for(i=0; i<decr; i++)
//                       b0[i] = b0[2*decr-i-1];
                       b0[i] = b0[decr2-i-1];


                   for(i=0; i<decr; i++)
                       b0[rd2+decr+i] = b0[rd2+decr-2-i];


                   for(i=0; i<decr; i++)
//                       b1[i] = -b1[2*decr-i-1];
                       b1[i] = -b1[decr2-i-1];



                   b1[kk+decr] = 0.;
                   for(i=1; i<decr; i++)
                       b1[kk+decr+i] = -b1[kk+decr-i];
                 }
           else  {


                   for(i=0; i<decr; i++)
//                       b0[i] = b0[2*decr-i-1];
                       b0[i] = b0[decr2-i-1];


                   for(i=0; i<decr; i++)
                       b0[rd2+decr+i] = b0[rd2+decr-1-i];


                   for(i=0; i<decr; i++)
//                       b1[i] = -b1[2*decr-i-1];
                       b1[i] = -b1[decr2-i-1];



                   for(i=0; i<decr; i++)
                       b1[kk+decr+i] = -b1[kk+decr-1-i];
                 }
x=o0+j;
//if( (*flt).filters_type!=2 )  synthesis_pe(length,b0,b1,x,wzy,flt,decr);
//                        else  synthesis_pe_2(length,b0,b1,x,wzy,flt,decr);
if( (*flt).filters_type!=2 )  synthesis_pe(length,b0,b1,x,wzy,flt,decr);
                        else  synthesis_pe_2(length,b0,b1,x,wzy,decr);

}
  }

  x=old;

//______________________________________________________________________________

  kk=dx-cd2;

length=dx;
oo=buf_float+(y1-1)*cols_wsq+x1-1;
if( (*flt).filters_type!=0 )        
  {
for(i=y1; i<y2; i++)
{
  oo+=cols_wsq;

  ooo=oo;
  for(j=decr; j<kk+decr; j++)
     {
      ooo++;
      b0[j]=*ooo;
      b1[j]=*(ooo+cd2);
     }

    if( wzx==1 ) {
                   ooo++;
                   b0[j]=*ooo;



                   for(j=0; j<decr; j++)
//                       b0[j] = b0[2*decr-j];
                       b0[j] = b0[decr2-j];



                   for(j=0; j<decr; j++)
                       b0[cd2+decr+j] = b0[cd2+decr-2-j];



                   for(j=0; j<decr; j++)
//                       b1[j] = b1[2*decr-j-1];
                       b1[j] = b1[decr2-j-1];



                   for(j=0; j<decr; j++)
                       b1[kk+decr+j] = b1[kk+decr-1-j];
                 }
           else  {


                   for(j=0; j<decr; j++)
//                       b0[j] = b0[2*decr-j];
                       b0[j] = b0[decr2-j];



                   for(j=0; j<decr; j++)
                       b0[cd2+decr+j] = b0[cd2+decr-1-j];



                   for(j=0; j<decr; j++)
//                       b1[j] = b1[2*decr-j-1];
                       b1[j] = b1[decr2-j-1];



                   for(j=0; j<decr; j++)
                       b1[kk+decr+j] = b1[kk+decr-2-j];
                 }
x=oo+1;
//if( (*flt).filters_type!=2 )  synthesis_pp(length,b0,b1,x,wzx,flt,decr);
//                        else  synthesis_pp_2(length,b0,b1,x,wzx,flt,decr);
if( (*flt).filters_type!=2 )  synthesis_pp(length,b0,b1,x,wzx,flt,decr);
                        else  synthesis_pp_2(length,b0,b1,x,wzx,decr);
}
  }
else        
  {
for(i=y1; i<y2; i++)
{
  oo+=cols_wsq;

  ooo=oo;
  for(j=decr; j<kk+decr; j++)
     {
      ooo++;
      b0[j]=*ooo;
      b1[j]=*(ooo+cd2);
     }
    if( wzx==1 ) {
                   ooo++;
                   b0[j]=*ooo;



                   for(j=0; j<decr; j++)
//                       b0[j] = b0[2*decr-j-1];
                       b0[j] = b0[decr2-j-1];



                   for(j=0; j<decr; j++)
                       b0[cd2+decr+j] = b0[cd2+decr-2-j];



                   for(j=0; j<decr; j++)
//                       b1[j] = -b1[2*decr-j-1];
                       b1[j] = -b1[decr2-j-1];



                   b1[kk+decr] = 0.;
                   for(j=1; j<decr; j++)
                       b1[kk+decr+j] = -b1[kk+decr-j];
                 }
           else  {


                   for(j=0; j<decr; j++)
//                       b0[j] = b0[2*decr-j-1];
                       b0[j] = b0[decr2-j-1];



                   for(j=0; j<decr; j++)
                       b0[cd2+decr+j] = b0[cd2+decr-1-j];



                   for(j=0; j<decr; j++)
//                       b1[j] = -b1[2*decr-j-1];
                       b1[j] = -b1[decr2-j-1];



                   for(j=0; j<decr; j++)
                       b1[kk+decr+j] = -b1[kk+decr-1-j];
                 }
x=oo+1;
//if( (*flt).filters_type!=2 )  synthesis_pp(length,b0,b1,x,wzx,flt,decr);
//                        else  synthesis_pp_2(length,b0,b1,x,wzx,flt,decr);
if( (*flt).filters_type!=2 )  synthesis_pp(length,b0,b1,x,wzx,flt,decr);
                        else  synthesis_pp_2(length,b0,b1,x,wzx,decr);
}
  }
return 0;
}

/*
int build_from_subband(float *buf_float,int x1,int y1, int x2, int y2,
      int level, int zone, float *x, float *b0, float *b1, Sfilters *flt)
{
  int dx=x2-x1,dy=y2-y1;
  int cd2,rd2;
  float *ooo,*oo0,*oo,*o0;
  int length,i,j,kk,wzx,wzy;
  float *old=x;
  int decr=2;

  if( (*flt).length_lo > (*flt).length_hi )   decr=(*flt).length_lo/2;
                                       else   decr=(*flt).length_hi/2;
  if( decr%2 ) decr++;
////////////////////////////////////////////////////////////////////////////////
  decr++;
////////////////////////////////////////////////////////////////////////////////

  cd2=dx>>1;
  rd2=dy>>1;

  wzx=abs(w[level][zone].eox);
  wzy=abs(w[level][zone].eoy);

  if( wzx==1 ) cd2++;
  if( wzy==1 ) rd2++;

  kk=dy-rd2;

  length=dy;
o0=buf_float+(y1-1)*cols_wsq;
int rd2clswsq=rd2*cols_wsq;

for(j=x1; j<x2; j++)
{
                 oo0=o0+j;
                 for(i=decr; i<kk+decr; i++)
                    {
                      oo0+=cols_wsq;
                      b0[i]=*oo0;
                      b1[i]=*(oo0+rd2clswsq);
                    }
if( (*flt).filters_type!=0 )
  {
    if( wzy==1 ) {
                     oo0+=cols_wsq;
                     b0[i]=*oo0;



                   for(i=0; i<decr; i++)
                       b0[i] = b0[2*decr-i];



                   for(i=0; i<decr; i++)
                       b0[rd2+decr+i] = b0[rd2+decr-2-i];



                   for(i=0; i<decr; i++)
                       b1[i] = b1[2*decr-i-1];



                   for(i=0; i<decr; i++)
                       b1[kk+decr+i] = b1[kk+decr-1-i];
                 }
           else  {


                   for(i=0; i<decr; i++)
                       b0[i] = b0[2*decr-i];



                   for(i=0; i<decr; i++)
                       b0[rd2+decr+i] = b0[rd2+decr-1-i];



                   for(i=0; i<decr; i++)
                       b1[i] = b1[2*decr-i-1];



                   for(i=0; i<decr; i++)
                       b1[kk+decr+i] = b1[kk+decr-2-i];
                 }
  }
else        
  {
    if( wzy==1 ) {
                     oo0+=cols_wsq;
                     b0[i]=*oo0;



                   for(i=0; i<decr; i++)
                       b0[i] = b0[2*decr-i-1];


                   for(i=0; i<decr; i++)
                       b0[rd2+decr+i] = b0[rd2+decr-2-i];


                   for(i=0; i<decr; i++)
                       b1[i] = -b1[2*decr-i-1];



                   b1[kk+decr] = 0.;
                   for(i=1; i<decr; i++)
                       b1[kk+decr+i] = -b1[kk+decr-i];
                 }
           else  {


                   for(i=0; i<decr; i++)
                       b0[i] = b0[2*decr-i-1];


                   for(i=0; i<decr; i++)
                       b0[rd2+decr+i] = b0[rd2+decr-1-i];


                   for(i=0; i<decr; i++)
                       b1[i] = -b1[2*decr-i-1];



                   for(i=0; i<decr; i++)
                       b1[kk+decr+i] = -b1[kk+decr-1-i];
                 }
  }
x=o0+j;
if( (*flt).filters_type!=2 )  synthesis_pe(length,b0,b1,x,wzy,flt,decr);
                        else  synthesis_pe_2(length,b0,b1,x,wzy,flt,decr);

}

  x=old;

//______________________________________________________________________________

  kk=dx-cd2;

length=dx;
oo=buf_float+(y1-1)*cols_wsq+x1-1;
for(i=y1; i<y2; i++)
{
  oo+=cols_wsq;

  ooo=oo;
  for(j=decr; j<kk+decr; j++)
     {
      ooo++;
      b0[j]=*ooo;
      b1[j]=*(ooo+cd2);
     }

if( (*flt).filters_type!=0 )        
  {
    if( wzx==1 ) {
                   ooo++;
                   b0[j]=*ooo;



                   for(j=0; j<decr; j++)
                       b0[j] = b0[2*decr-j];



                   for(j=0; j<decr; j++)
                       b0[cd2+decr+j] = b0[cd2+decr-2-j];



                   for(j=0; j<decr; j++)
                       b1[j] = b1[2*decr-j-1];



                   for(j=0; j<decr; j++)
                       b1[kk+decr+j] = b1[kk+decr-1-j];
                 }
           else  {


                   for(j=0; j<decr; j++)
                       b0[j] = b0[2*decr-j];



                   for(j=0; j<decr; j++)
                       b0[cd2+decr+j] = b0[cd2+decr-1-j];



                   for(j=0; j<decr; j++)
                       b1[j] = b1[2*decr-j-1];



                   for(j=0; j<decr; j++)
                       b1[kk+decr+j] = b1[kk+decr-2-j];
                 }
  }
else        
  {
    if( wzx==1 ) {
                   ooo++;
                   b0[j]=*ooo;



                   for(j=0; j<decr; j++)
                       b0[j] = b0[2*decr-j-1];



                   for(j=0; j<decr; j++)
                       b0[cd2+decr+j] = b0[cd2+decr-2-j];



                   for(j=0; j<decr; j++)
                       b1[j] = -b1[2*decr-j-1];



                   b1[kk+decr] = 0.;
                   for(j=1; j<decr; j++)
                       b1[kk+decr+j] = -b1[kk+decr-j];
                 }
           else  {


                   for(j=0; j<decr; j++)
                       b0[j] = b0[2*decr-j-1];



                   for(j=0; j<decr; j++)
                       b0[cd2+decr+j] = b0[cd2+decr-1-j];



                   for(j=0; j<decr; j++)
                       b1[j] = -b1[2*decr-j-1];



                   for(j=0; j<decr; j++)
                       b1[kk+decr+j] = -b1[kk+decr-1-j];
                 }
  }
x=oo+1;
if( (*flt).filters_type!=2 )  synthesis_pp(length,b0,b1,x,wzx,flt,decr);
                        else  synthesis_pp_2(length,b0,b1,x,wzx,flt,decr);
}
return 0;
}
*/
//void bin_width_iteration(float *buf_float,float press,band *bb_tst)
void SWSQ::bin_width_iteration(float *buf_float,float press)
{
  int i,j,k,l,x1,x2,y1,y2,bands[60],metka,count=0;
  int dx,dy,xx0,xx1,yy0,yy1;
  float *pti_f,*p0;
  double f,s,q,qq,q1,q2,m[60];
  double sigma,max,aver;

  float gamma=2.5;
  float btr[49],cmpr[49],bit_rate=2.25;

  btr[48] = (float)0.01;        cmpr[48] = (float)295.95;
  btr[47] = (float)0.02;        cmpr[47] = (float)233.88;
  btr[46] = (float)0.03;        cmpr[46] = (float)194.00;
  btr[45] = (float)0.04;        cmpr[45] = (float)173.16;
  btr[44] = (float)0.05;        cmpr[44] = (float)144.15;
  btr[43] = (float)0.10;        cmpr[43] = (float)80.20;
  btr[42] = (float)0.15;        cmpr[42] = (float)55.73;
  btr[41] = (float)0.20;        cmpr[41] = (float)45.10;
  btr[40] = (float)0.25;        cmpr[40] = (float)40.73;
  btr[39] = (float)0.30;        cmpr[39] = (float)24.15;
  btr[38] = (float)0.35;        cmpr[38] = (float)21.57;
  btr[37] = (float)0.40;        cmpr[37] = (float)21.57;
  btr[36] = (float)0.45;        cmpr[36] = (float)18.81;
  btr[35] = (float)0.50;        cmpr[35] = (float)18.04;
  btr[34] = (float)0.55;        cmpr[34] = (float)16.76;
  btr[33] = (float)0.60;        cmpr[33] = (float)17.35;
  btr[32] = (float)0.65;        cmpr[32] = (float)17.91;
  btr[31] = (float)0.70;        cmpr[31] = (float)16.24;
  btr[30] = (float)0.75;        cmpr[30] = (float)16.15;
  btr[29] = (float)0.80;        cmpr[29] = (float)16.45;
  btr[28] = (float)0.85;        cmpr[28] = (float)15.39;
  btr[27] = (float)0.90;        cmpr[27] = (float)15.09;
  btr[26] = (float)0.95;        cmpr[26] = (float)14.62;
  btr[25] = (float)1.00;        cmpr[25] = (float)13.93;
  btr[24] = (float)1.05;        cmpr[24] = (float)13.51;
  btr[23] = (float)1.10;        cmpr[23] = (float)12.83;
  btr[22] = (float)1.15;        cmpr[22] = (float)12.31;
  btr[21] = (float)1.20;        cmpr[21] = (float)11.68;
  btr[20] = (float)1.25;        cmpr[20] = (float)11.11;
  btr[19] = (float)1.30;        cmpr[19] = (float)10.57;
  btr[18] = (float)1.35;        cmpr[18] = (float)10.05;
  btr[17] = (float)1.40;        cmpr[17] = (float)9.57;
  btr[16] = (float)1.45;        cmpr[16] = (float)9.08;
  btr[15] = (float)1.50;        cmpr[15] = (float)8.64;
  btr[14] = (float)1.55;        cmpr[14] = (float)8.21;
  btr[13] = (float)1.60;        cmpr[13] = (float)7.79;
  btr[12] = (float)1.65;        cmpr[12] = (float)7.47;
  btr[11] = (float)1.70;        cmpr[11] = (float)7.18;
  btr[10] = (float)1.75;        cmpr[10] = (float)6.90;
  btr[ 9] = (float)1.80;        cmpr[ 9] = (float)6.64;
  btr[ 8] = (float)1.85;        cmpr[ 8] = (float)6.40;
  btr[ 7] = (float)1.90;        cmpr[ 7] = (float)6.17;
  btr[ 6] = (float)1.95;        cmpr[ 6] = (float)5.96;
  btr[ 5] = (float)2.00;        cmpr[ 5] = (float)5.77;
  btr[ 4] = (float)2.25;        cmpr[ 4] = (float)4.92;
  btr[ 3] = (float)2.50;        cmpr[ 3] = (float)4.24;
  btr[ 2] = (float)3.00;        cmpr[ 2] = (float)4.39;
  btr[ 1] = (float)4.00;        cmpr[ 1] = (float)2.25;
  btr[ 0] = (float)5.00;        cmpr[ 0] = (float)1.66;

  if( press<cmpr[ 0] )  bit_rate = btr[ 0];
  if( press>cmpr[48] )  bit_rate = btr[48];
  if( press>=cmpr[ 0] && press<=cmpr[48] )
    {
      for(i=1; i<49; i++)
         {
           if( press>=cmpr[i-1] && press<=cmpr[i] )
             {
              float alf = (press-cmpr[i-1])/(cmpr[i]-cmpr[i-1]);
              bit_rate = btr[i-1]+alf*(btr[i]-btr[i-1]);
              break;
             }
         }
    }

if( press<0. )  bit_rate=-press;

  for(k=0; k<4; k++)
      m[k]=0.0009765625;
  for(k=4; k<51; k++)
      m[k]=0.00390625;
  for(k=51; k<60; k++)
      m[k]=0.0625;

/*
  for(k=0; k<60; k++)
     {
       x1=bb[k].x1;  x2=bb[k].x2;
       y1=bb[k].y1;  y2=bb[k].y2;
       dx=x2-x1; dy=y2-y1;
       m[k]= (double)dx*dy/(cols_wsq*rows_wsq);
     }
*/
  for(k=60; k<64; k++)
     {
       bb[k].sigma=0.;
       bb[k].bin_width=0.;
       bb[k].zero_bin_width=0.;
     }
  for(k=0; k<60; k++)
     {
       x1=bb[k].x1;  x2=bb[k].x2;
       y1=bb[k].y1;  y2=bb[k].y2;

       dx=(3*(x2-x1))/4;    xx0=(x2-x1)/8;       xx0+=x1;    xx1=xx0+dx-1;
       dy=(7*(y2-y1))/16;   yy0=(9*(y2-y1))/32;  yy0+=y1;    yy1=yy0+dy-1;

if( dx<1 || dy<1 ) sigma=0.;
 else {
       aver=0.;
       p0 = buf_float+(yy0-1)*cols_wsq-1+xx0;
       for(i=yy0; i<=yy1; i++)
          {
            p0 += cols_wsq;
            pti_f = p0;
            for(j=xx0; j<=xx1; j++)
               {
                pti_f++;
                aver+=*pti_f;
               }
          }


       j=dx*dy;
       aver/=j;
       sigma=0.;
       p0 = buf_float+(yy0-1)*cols_wsq+xx0;
       for(i=yy0; i<=yy1; i++)
          {
            p0 += cols_wsq;
            pti_f = p0;
            for(j=xx0; j<=xx1; j++)
               {
                 max=aver-(*pti_f);
                 sigma+=max*max;
                 pti_f++;
               }
          }
       l=dx*dy-1;//????
       sigma/=l;
      }

       f=log(sigma);
       sigma=sqrt(sigma);
       bb[k].sigma=(float)sigma;

       if( (sigma*sigma)<=1.01 ) {
                    bands[k]=0;
                    bb[k].bin_width=0.;
                  }
            else  {
                    bands[k]=1;
                    if( k<4 )  bb[k].bin_width=1.;
                        else {
                               bb[k].bin_width=(float)10./(float)f;
                               if( k==52 || k==56 )  bb[k].bin_width*=(float)0.75757575758;    
                               if( k==53 || k==58 )  bb[k].bin_width*=(float)0.9259259259259;  
                               if( k==54 || k==57 )  bb[k].bin_width*=(float)0.7042253521127;  
                               if( k==55 || k==59 )  bb[k].bin_width*=(float)0.9259259259259;  
                             }
                  }

     }


  do{
      s=0;
      for(k=0; k<60; k++)
          if( bands[k]!=0 ) s+=m[k];
      s=1./s;

      qq=1.;
      for(k=0; k<60; k++)
          if( bands[k]!=0 ) {
                              q1=bb[k].sigma/bb[k].bin_width;
                              q2=pow(q1,m[k]);
                              qq=qq*q2;
                            }
      q1=pow(2.0,(bit_rate*s-1));
      q2=pow(qq,(-s));
      q=gamma/(q1*q2);

      metka=0;
      for(k=0; k<60; k++)
         {
          if( bands[k]!=0 && ((bb[k].bin_width*q)>=(2*gamma*bb[k].sigma)) )
            {
              bands[k]=0;
              metka=1;
            }
         }
      count++;
      if( count==60 )  metka=0;

    }while(metka==1);

  for(k=0; k<60; k++)
     {
      bb[k].bin_width*=(float)q;
      bb[k].zero_bin_width=(float)1.2*bb[k].bin_width;
     }
//////////////////////////////////////////////////////////////////////
/*
float qtst_bw,qtst_zbw;
  for(k=0; k<60; k++)
     {
      if(bb_tst[k].bin_width!=0.0)
        {
          qtst_bw=(bb[k].bin_width-bb_tst[k].bin_width)/bb_tst[k].bin_width;
          qtst_bw*=100.;
          if( qtst_bw<0.)
            {
              qtst_bw=qtst_bw;
            }
          qtst_bw=fabs(qtst_bw);
          if( qtst_bw>0.051)
            {
              qtst_bw=qtst_bw;
            }
        }
      else
        {
          bb[k].bin_width=bb[k].bin_width;
        }
      if(bb_tst[k].zero_bin_width!=0.0)
        {
          qtst_zbw=(bb[k].zero_bin_width-bb_tst[k].zero_bin_width)/bb_tst[k].zero_bin_width;
          qtst_zbw*=100.;
          if( qtst_zbw<0. )
            {
              qtst_zbw=qtst_zbw;
            }
          qtst_zbw=fabs(qtst_zbw);
          if( qtst_zbw>0.051)
            {
              qtst_zbw=qtst_zbw;
            }
        }
      else
        {
          bb[k].zero_bin_width=bb[k].zero_bin_width;
        }
     }
*/
//////////////////////////////////////////////////////////////////////
}

void SWSQ::quantization(float *buf_float, int number_subband ,int * quant_table)
{
  int i,x1,x2,j,y1,y2,k,*q_f,*q0;
  float zk,zkd2,qk,a,f,*pti_f,*p0;

  x1=bb[number_subband].x1;
  y1=bb[number_subband].y1;
  x2=bb[number_subband].x2;
  y2=bb[number_subband].y2;
  qk=bb[number_subband].bin_width;
  zk=bb[number_subband].zero_bin_width;

  q0=quant_table+(y1-1)*cols_wsq+x1;
  if( qk!=0 )
    {
     qk=(float)1./qk;
     zkd2=zk*(float)0.5;
     p0=buf_float+(y1-1)*cols_wsq+x1;
     for(i=y1; i<y2; i++)
        {
          q0 += cols_wsq;
          q_f=q0;
          p0 += cols_wsq;
          pti_f=p0;
          for(j=x1; j<x2; j++)
             {
               a=*pti_f;
               if( a<=zkd2 && a>=-zkd2 )  k=0;
                                    else {
                                           if( a>zkd2 ) {
                                                          f=(a-zkd2)*qk;
                                                          if(f>=0)  k=(int)f;
                                                              else  k=(int)f-1;
                                                          k++;
                                                        }
                                                   else {
                                                          f=(a+zkd2)*qk;
                                                          if(f>0)  k=(int)f+1;
                                                             else  k=(int)f;
                                                          k--;
                                                        }
                                         }
               *q_f=k;
               pti_f++;
               q_f++;
             }
        }
    }
  else {
         for(i=y1; i<y2; i++)
            {
              q0 += cols_wsq;
              q_f=q0;
              for(j=x1; j<x2; j++)
                 {
                  *q_f=0;
                  q_f++;
                 }
            }
       }
}
int SWSQ::code_symbol( int N_group )
{
int n,m;
int k_h=0,length=0;
int i;
int *hi;
unsigned char *hc;

hi = int_huff[N_group];
for(i=0; i<group[N_group].kh0; i++)
   {
      n = *(hi+i);

      if( n==0 )  length++;
      else {
            if( length!=0 )
              {
                do{
                     if( length<101 ) {
                                         k_h++; length = 0;
                                      }
                     else
                      {
                        if( length<256 ) {
                                           k_h+=2;
                                            length = 0;
                                         }
                        else {
                              if( length<65536 ) {
                                                   k_h+=3;
                                                   length = 0;
                                                 }
                              else {
                                     k_h+=3; length -= 65536;
                                   }
                             }
                      }
                  }while( length!=0 );
              }
            if( n>-74 && n<75 && n!=0 )
              {
                k_h++;
                continue;
              }
            if( n<-73 && n>-256 )
              {
                k_h+=2;
                continue;
              }
            if( n>74 && n<256 )
              {
                k_h+=2;
                continue;
              }
            if( n>-65536 && n<-255 )
              {
                k_h+=3;
                continue;
              }
            if( n>255 && n<65536 )
              {
                k_h+=3;
              }
           }
      if( n==0 )
        {
          if( length==65536 ){
                               k_h+=3; length = 0;
                             }
        }
   }
if( length!=0 )
  {
    if( length<101 ) {
                       k_h++; length = 0;
                     }
                else {
                       if( length<256 ) {
                                          k_h+=2;
                                          length = 0;
                                        }
                                   else {
                                          k_h+=3;
                                          length = 0;
                                        }
                     }

  }

     _symbol[N_group] = new byte[k_h+40];

k_h=0;
hc = _symbol[N_group];
for(i=0; i<group[N_group].kh0; i++)
   {
      n = *(hi+i);
      if( n==0 )  length++;
      else {
            if( length!=0 )
              {
                do{
                     if( length<101 ) {
                                         *(hc+k_h) = (unsigned char)length;
                                         k_h++; length = 0;
                                      }
                     else
                      {
                        if( length<256 ) {
                                           *(hc+k_h) = 105;
                                           k_h++;
                                           *(hc+k_h) = (unsigned char)length;
                                           k_h++; length = 0;
                                         }
                        else {
                              if( length<65536 ) {
                                                   *(hc+k_h) = 106;
                                                   k_h++;
                                                   m=length>>8;
                                                   *(hc+k_h) = (unsigned char)m;
                                                   k_h++;
                                                   m =length-(m<<8);
                                                   *(hc+k_h) = (unsigned char)m;
                                                   k_h++; length = 0;
                                                 }
                                            else {
                                                   *(hc+k_h) = 106;
                                                   k_h++; length -= 65536;
                                                   //m=256;
                                                   m=255;
                                                   *(hc+k_h) = (unsigned char)m;
                                                   k_h++;
                                                   *(hc+k_h) = (unsigned char)m;
                                                   k_h++;
                                                 }
                             }
                      }
                  }while( length!=0 );
              }
            if( n>-74 && n<75 && n!=0 )
              {
                n+=180;
                *(hc+k_h) = (unsigned char)n;  k_h++;
                continue;
              }
            if( n<-73 && n>-256 )
              {
                *(hc+k_h) = 102; k_h++;
                n=-n;
                *(hc+k_h) = (unsigned char)n; k_h++;
                continue;
              }
            if( n>74 && n<256 )
              {
                *(hc+k_h) = 101; k_h++;
                *(hc+k_h) = (unsigned char)n; k_h++;
                continue;
              }
            if( n>-65536 && n<-255 )
              {
                *(hc+k_h) = 104; k_h++;
                n=-n;
                m=n>>8;
                *(hc+k_h) = (unsigned char)m; k_h++;
                m=n-(m<<8);
                *(hc+k_h) = (unsigned char)m; k_h++;
                continue;
              }
            if( n>255 && n<65536 )
              {
                *(hc+k_h) = 103; k_h++;
                m=n>>8;
                *(hc+k_h) = (unsigned char)m; k_h++;
                m=n-(m<<8);
                *(hc+k_h) = (unsigned char)m; k_h++;
              }
           }
      if( n==0 )
        {
          if( length==65536 ){
                               *(hc+k_h) = 106;
                               k_h++; length = 0;
                               m=255;
                               *(hc+k_h) = (unsigned char)m;
                               k_h++;
                               *(hc+k_h) = (unsigned char)m;
                               k_h++;
                             }
        }
   }
if( length!=0 )
  {
    if( length<101 ) {
                       *(hc+k_h) = (unsigned char)length;
                       k_h++; length = 0;
                     }
                else {
                       if( length<256 ) {
                                          *(hc+k_h) = 105;
                                          k_h++;
                                          *(hc+k_h) = (unsigned char)length;
                                          k_h++; length = 0;
                                        }
                                   else {
                                          *(hc+k_h) = 106;
                                          k_h++;
                                          m=length>>8;
                                          *(hc+k_h) = (unsigned char)m;
                                          k_h++;
                                          m =length-(m<<8);
                                          *(hc+k_h) = (unsigned char)m;
                                          k_h++; length = 0;
                                        }
                     }

  }
group[N_group].kh = k_h;

return 0;
}

int SWSQ::code_symbol_compliance( int N_group )
{
int n,m;
int k_h,k_h_stream,length;
int i;
int *hi;
unsigned char *hc,*hs;

MaxCZR(N_group);

hi = int_huff[N_group];

k_h_stream=0; k_h=0; length=0;
for(i=0; i<group[N_group].kh0; i++)
   {
      n = *(hi+i);
      if( n==0 )  length++;
      else {
            if( length!=0 )
              {
                do{
                     if( length<=MaxZRun )
                       {
                         k_h++; length = 0;
                         k_h_stream++;
                       }
                     else
                      {
                        if( length<256 ) {
                                           k_h++;
                                           length = 0;
                                           k_h_stream+=2;
                                         }
                        else {
                              k_h++;
                              if( length<65536 )   length = 0;
                                            else   length -= 65535;
                              k_h_stream+=3;
                             }
                      }
                  }while( length!=0 );
              }
            if( abs(n)<=MaxCoeff )
              {
                k_h++;
                k_h_stream++;
                continue;
              }
            if( n<-MaxCoeff && n>-256 )
              {
                k_h++;
                k_h_stream+=2;
                continue;
              }
            if( n>MaxCoeff && n<256 )
              {
                k_h++;
                k_h_stream+=2;
                continue;
              }
            if( n>-65536 && n<-255 )
              {
                k_h++;
                k_h_stream+=3;
                continue;
              }
            if( n>255 && n<65536 )
              {
                k_h++;
                k_h_stream+=3;
              }
           }

      if( n==0 )
        {
          if( length==65535 ){
                               k_h++; length = 0;
                               k_h_stream+=3;
                             }
        }

   }

if( length!=0 )
  {
    if( length<=MaxZRun ) {
                            k_h++; length = 0;
                            k_h_stream++;
                          }
                     else {
                            if( length<256 ) {
                                               k_h++; length = 0;
                                               k_h_stream+=2;
                                             }
                                        else {
                                               k_h++; length = 0;
                                               k_h_stream+=3;
                                             }
                          }
  }

_symbol[N_group] = new byte[k_h+40];
stream[N_group] = new byte[k_h_stream+40];

hs = stream[N_group];
hc = _symbol[N_group];
k_h_stream=0; k_h=0; length=0;
for(i=0; i<group[N_group].kh0; i++)
   {
      n = *(hi+i);
      if( n==0 )  length++;
      else {
            if( length!=0 )
              {
                do{
                     if( length<=MaxZRun )
                       {
                         *(hs+k_h_stream) = (unsigned char)length;
                         k_h_stream++;
                         *(hc+k_h) = (unsigned char)length;
                         k_h++; length = 0;
                       }
                     else
                       {
                         if( length<256 )
                           {
                             *(hs+k_h_stream) = 105;
                             k_h_stream++;
                             *(hs+k_h_stream) = (unsigned char)length;
                             k_h_stream++;
                             *(hc+k_h) = 105;
                             k_h++;
                             length = 0;
                           }
                         else
                           {
                             *(hs+k_h_stream) = 106;
                             k_h_stream++;
                             if( length<65536 )
                               {
                                  m=length>>8;
                                  *(hs+k_h_stream) = (unsigned char)m;
                                  k_h_stream++;
                                  m =length-(m<<8);
                                  *(hs+k_h_stream) = (unsigned char)m;
                                  k_h_stream++;
                                  length = 0;
                               }
                             else
                               {
                                  *(hs+k_h_stream) = (unsigned char)255;
                                  k_h_stream++;
                                  *(hs+k_h_stream) = (unsigned char)255;
                                  k_h_stream++;
                                  length -= 65535;
                               }

                             *(hc+k_h) = 106;
                             k_h++;
                           }
                       }
                  }while( length!=0 );
              }
            if( abs(n)<=MaxCoeff )
              {
                n+=180;
                *(hs+k_h_stream) = (unsigned char)n;
                k_h_stream++;
                *(hc+k_h) = (unsigned char)n;  k_h++;
                continue;
              }
            if( n<-MaxCoeff && n>-256 )
              {
                n=-n;
                *(hs+k_h_stream) = 102;
                k_h_stream++;
                *(hs+k_h_stream) = (unsigned char)n;
                k_h_stream++;
                *(hc+k_h) = 102; k_h++;
                continue;
              }
            if( n>MaxCoeff && n<256 )
              {
                *(hs+k_h_stream) = 101;
                k_h_stream++;
                *(hs+k_h_stream) = (unsigned char)n;
                k_h_stream++;
                *(hc+k_h) = 101; k_h++;
                continue;
              }
            if( n>-65536 && n<-255 )
              {
                n=-n;
                *(hs+k_h_stream) = 104;
                k_h_stream++;
                m=n>>8;
                *(hs+k_h_stream) = (unsigned char)m;
                k_h_stream++;
                m =n-(m<<8);
                *(hs+k_h_stream) = (unsigned char)m;
                k_h_stream++;
                *(hc+k_h) = 104; k_h++;
                continue;
              }
            if( n>255 && n<65536 )
              {
                *(hs+k_h_stream) = 103;
                k_h_stream++;
                m=n>>8;
                *(hs+k_h_stream) = (unsigned char)m;
                k_h_stream++;
                m =n-(m<<8);
                *(hs+k_h_stream) = (unsigned char)m;
                k_h_stream++;
                *(hc+k_h) = 103; k_h++;
              }
           }

      if( n==0 )
        {
          if( length==65535 ){
                               *(hs+k_h_stream) = 106;
                               k_h_stream++;
                               m=length>>8;
                               *(hs+k_h_stream) = (unsigned char)m;
                               k_h_stream++;
                               m =length-(m<<8);
                               *(hs+k_h_stream) = (unsigned char)m;
                               k_h_stream++;

                               *(hc+k_h) = 106;
                               k_h++; length = 0;
                             }
        }

   }

if( length!=0 )
  {
    if( length<=MaxZRun ) {
                             *(hs+k_h_stream) = (unsigned char)length;
                             k_h_stream++;

                             *(hc+k_h) = (unsigned char)length;
                             k_h++; length = 0;
                          }
                else {
                       if( length<256 ) {
                                          *(hs+k_h_stream) = 105;
                                          k_h_stream++;
                                          *(hs+k_h_stream) = (unsigned char)length;
                                          k_h_stream++;

                                          *(hc+k_h) = 105;
                                          k_h++; length = 0;
                                        }
                                   else {
                                          *(hs+k_h_stream) = 106;
                                          k_h_stream++;
                                          m=length>>8;
                                          *(hs+k_h_stream) = (unsigned char)m;
                                          k_h_stream++;
                                          m =length-(m<<8);
                                          *(hs+k_h_stream) = (unsigned char)m;
                                          k_h_stream++;

                                          *(hc+k_h) = 106;
                                          k_h++; length = 0;
                                        }
                     }
  }

group[N_group].kh_stream = k_h_stream;
group[N_group].kh = k_h;

return 0;
}


void SWSQ::qs_2(int *A1,int *A2,int left,int right)
{
 register int i,j;
 int x,y;

 i=left; j=right;
 x=(A1[left]+A1[right])>>1;
 do{
     while(A1[i]>x && i<right) i++;
     while(x>A1[j] && j>left) j--;
     if(i<=j)
       {
         y=A1[i];  A1[i]=A1[j];  A1[j]=y;
         y=A2[i];  A2[i]=A2[j];  A2[j]=y;
         i++; j--;
       }
   }while(i<=j);

 if(left<j)  qs_2(A1,A2,left,j);
 if(i<right) qs_2(A1,A2,i,right);

}
void SWSQ::qs_3(int *A1,int *A2,int *A3,int left,int right)
{
 register int i,j;
 int x,y;

 i=left; j=right;
 x=(A1[left]+A1[right])>>1;
 do{
     while(A1[i]>x && i<right) i++;
     while(x>A1[j] && j>left) j--;
     if(i<=j)
       {
         y=A1[i];  A1[i]=A1[j];  A1[j]=y;
         y=A2[i];  A2[i]=A2[j];  A2[j]=y;
         y=A3[i];  A3[i]=A3[j];  A3[j]=y;
         i++; j--;
       }
   }while(i<=j);

 if(left<j)  qs_3(A1,A2,A3,left,j);
 if(i<right) qs_3(A1,A2,A3,i,right);

}
void SWSQ::qs_4(int *A1,int *A2,int *A3,int *A4,int left,int right)
{
 register int i,j;
 int x,y;

 i=left; j=right;
 x=(A1[left]+A1[right])>>1;
 do{
     while(A1[i]>x && i<right) i++;
     while(x>A1[j] && j>left) j--;
     if(i<=j)
       {
         y=A1[i];  A1[i]=A1[j];  A1[j]=y;
         y=A2[i];  A2[i]=A2[j];  A2[j]=y;
         y=A3[i];  A3[i]=A3[j];  A3[j]=y;
         y=A4[i];  A4[i]=A4[j];  A4[j]=y;
         i++; j--;
       }
   }while(i<=j);

 if(left<j)  qs_4(A1,A2,A3,A4,left,j);
 if(i<right) qs_4(A1,A2,A3,A4,i,right);

}

int SWSQ::probability( int N_group )
{
 int i,j,k=0,n,n_node,ii,k1,k2;
 unsigned char *ch;
                 int   *symbol_pr;
                 int   *count_pr;
                 int   *code_length_pr;
 Asymb=NULL;
 Acoun=NULL;
 Apriz=NULL;
 Anode=NULL;

 nodeN1=NULL;
 nodeN2=NULL;
 nodeL1=NULL;
 nodeL2=NULL;

 int number=0;

 ii = group[N_group].kh;

  symbol_pr      = symbol_HUFFVAL[N_group];
  count_pr       = count_HUFFVAL[N_group];
  code_length_pr = code_length_HUFFVAL[N_group];

  for(j=1; j<256; j++)
     {
       int k=0;
       ch = _symbol[N_group];
       for(i=0; i<ii; i++)
          {
            if( *ch==j )  k++;
            ch++;
          }

       *(symbol_pr+j) = j;
       *(count_pr+j) = k;
       if( k>0 )  {
                    number++;
                    *(count_pr+j)=k+1;
                  }
     }
 *(symbol_pr) = 0;
 *(count_pr) = 0;
 *(count_pr+0) = 1;  number++;

if(number==0 )  return -2;

qs_2(count_pr,symbol_pr,0,255);

if( (Asymb = new int [number+40])==NULL )   return -1;
if( (Acoun = new int [number+40])==NULL )   return -1;
if( (Apriz = new int [number+40])==NULL )   return -1;
if( (Anode = new int [number+40])==NULL )   return -1;


if( (nodeN1 = new int [number+40])==NULL )    return -1;
if( (nodeN2 = new int [number+40])==NULL )    return -1;
if( (nodeL1 = new int [number+40])==NULL )    return -1;
if( (nodeL2 = new int [number+40])==NULL )    return -1;

for(i=0; i<number; i++)
   {
     nodeN1[i]=999;
     nodeN2[i]=999;
     nodeL1[i]=999;
     nodeL2[i]=999;
   }


if( number>1 )
{
for(i=0; i<number; i++)
   {
     Asymb[i]=*(symbol_pr+i);
     Acoun[i]=*(count_pr+i);
     Apriz[i]=0;
     Anode[i]=-1;
   }
int NN;
n_node=1;
NN=number;
do{
    k1 = NN-1;   k2 = NN-2;
    qs_4(Acoun,Asymb,Apriz,Anode,0,k1);
    if( Apriz[k2]==0 && Apriz[k1]==0 )
      {
        nodeL1[n_node] = Asymb[k2];
        nodeL2[n_node] = Asymb[k1];
      }
    if( Apriz[k2]==0 && Apriz[k1]==1 )
      {
        nodeN1[n_node] = Anode[k1];
        nodeL1[n_node] = Asymb[k2];
      }
    if( Apriz[k2]==1 && Apriz[k1]==0 )
      {
        nodeN1[n_node] = Anode[k2];
        nodeL1[n_node] = Asymb[k1];
      }
    if( Apriz[k2]==1 && Apriz[k1]==1 )
      {
        nodeN1[n_node] = Anode[k2];
        nodeN2[n_node] = Anode[k1];
      }
    Acoun[k2] = Acoun[k1]+Acoun[k2];
    Asymb[k2] = 999;
    Apriz[k2] = 1;
    Anode[k2] = n_node;
    n_node++;  NN--;
  }while( NN!=1 );

int length;
NN=number-1;

  for(i=0; i<number; i++)
     {
       n = *(symbol_pr+i);
       length = 0;
       for(j=1; j <number; j++)
          {
            if( n==nodeL1[j] || n==nodeL2[j] )
              {
                k = j;  length++;  break;
              }
          }
       while( k!=NN )
            {
              j++;
              if( k==nodeN1[j] || k==nodeN2[j] )
                {
                  k = j;
                  length++;
                }
            }
       *(code_length_pr+i) = length;
     }
}
else {
       *code_length_pr = 1;
     }




number--;



     BITS[N_group].L[0]  = 0;
for(i=1; i<33; i++)
   {
    k=0;

    for(j=0; j<number; j++)
       {
        if( *(code_length_pr+j)==i ) k++;
       }
    BITS[N_group].L[i]  = (byte)k;

   }



 int i2;
 for(i = 0; i <33; i++)
    {
      INT_BITS.L[i]=BITS[N_group].L[i];
    }
j=0;
   for(i = 32; i > 15; i--)/////////////////////////////////////////////////
      {
        while(INT_BITS.L[i] > 0)
                                 {
                                   j=1;
                                   i2 = i - 2;
                                   while(INT_BITS.L[i2] == 0)
                                          i2--;
                                   INT_BITS.L[i] -= 2;
                                   INT_BITS.L[i - 1] += 1;
                                   INT_BITS.L[i2 + 1] += 2;
                                   INT_BITS.L[i2] -= 1;
                                 }
      }
if( j!=0 )
  {
   while(INT_BITS.L[i] == 0)
           i--;
   INT_BITS.L[i] -= 1;
  }
 for(i = 0; i <33; i++)
    {
      if(INT_BITS.L[i]<0) INT_BITS.L[i]=0;
      BITS[N_group].L[i]=(byte)INT_BITS.L[i];
    }

//_________________________________________________________________________;
/*
   for(i = 32; i > 15; i--)/////////////////////////////////////////////////
      {
        while(BITS[N_group].L[i] > 0)
                                 {
                                   i2 = i - 2;
                                   while(BITS[N_group].L[i2] == (byte)0)
                                        {
                                                          i2--;
if( i2<1 )  {
               i=i;
            }
                                        }
                                   BITS[N_group].L[i] -= (byte)2;
                                   BITS[N_group].L[i - 1] += (byte)1;
                                   BITS[N_group].L[i2 + 1] += (byte)2;
                                   BITS[N_group].L[i2] -= (byte)1;
///////////////////////////////////////////
if( BITS[N_group].L[i]<0 ) {
                            i=i;
                           }
j=BITS[N_group].L[i2];
j=j;
///////////////////////////////////////////
                                 }
      }

//   while(BITS[N_group].L[i] == 0)
//           i--;
//   BITS[N_group].L[i] -= 1;

//_________________________________________________________________________;
*/

delete []   Asymb;   Asymb=NULL;
delete []   Acoun;   Acoun=NULL;
delete []   Apriz;   Apriz=NULL;
delete []   Anode;   Anode=NULL;
delete []  nodeN1;  nodeN1=NULL;
delete []  nodeN2;  nodeN2=NULL;
delete []  nodeL1;  nodeL1=NULL;
delete []  nodeL2;  nodeL2=NULL;

return 0;
}

/////////////////////////////////////////////////////////////
///*
void SWSQ::BBTS(int j, int S) //BBTS(size,value);
{
  int i,A=1,B;

  for(i=1; i<=j; i++)
     {
        B = A<<(j-i);
        if( (B&S)==0 )  {
                          fprintf(hh,"0");
                        }

                  else  {
                          fprintf(hh,"1");
                        }
     }
}
//*/
/////////////////////////////////////////////////////////////

int SWSQ::build_huffman_tables( int N_group )
{
 int i,j,k;

     HUFFSIZE[N_group] = new ushort[357];
 if( HUFFSIZE[N_group]==NULL )    return -1;
     HUFFCODE[N_group] = new ushort[357];
 if( HUFFCODE[N_group]==NULL )    return -1;
     EHUFCO[N_group] = new ushort[357];
 if( EHUFCO[N_group]==NULL )    return -1;
     EHUFSI[N_group] = new ushort[357];
 if( EHUFSI[N_group]==NULL )    return -1;

 ushort code,si,*h_size,*h_code;
 h_size = HUFFSIZE[N_group];
 h_code = HUFFCODE[N_group];


if(fprint==1)
  {
    fprintf(hh,"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    fprintf(hh,"##########################################################\n");
    fprintf(hh,"N_group=%1d\n",N_group);
    for(k=0; k<33; k++)
        fprintf(hh,"           L[%2d]=%3d\n",k,BITS[N_group].L[k]);

  }
 for(k=0; k<357; k++)
     HUFFSIZE[N_group][k]=0;
 k=0; j=1;
 i=0;
 do{
     if( j>BITS[N_group].L[i] ) {
                                  i++; j=1;
                                }
                          else  {
                                  *(h_size+k) = (ushort)i;
                                  k++; j++;
                                }
   }while(i<=16);

 HUFFSIZE[N_group][k]=0;
 LASTK[N_group]=k;


 k=0; code=0; si=*h_size;
 do{
    do{
        *(h_code+k) = code;
        code++; k++;
      }while( *(h_size+k)==si );
    if( *(h_size+k)==0 ) break;
    do{
        code=(ushort)(code<<1);
        si++;
      }while( *(h_size+k)!=si );
   }while( *(h_size+k)!=0 );



///*
int code1,code2,size1,size2,size;
for(k=0; k<LASTK[N_group]; k++)
   {
     code1=*(h_code+k);
     if( code1>65535 ) {
                         return -2;
                       }
     size1=*(h_size+k);
     for(i=k+1; i<LASTK[N_group]; i++)
        {
          code2=*(h_code+i);
          size2=*(h_size+i);
          size=size2-size1;
          code=(ushort)(code2>>size);
          if( code1==code ){
                             return -2;
                           }
        }
   }

ushort *e_co,*e_si;
                 int   *symbol_pr;
                 int   *code_length_pr;
                 ushort *code_pr;
  symbol_pr      = symbol_HUFFVAL[N_group];
  code_length_pr = code_length_HUFFVAL[N_group];
  code_pr        = code_HUFFVAL[N_group];


e_co = EHUFCO[N_group];
e_si = EHUFSI[N_group];
    for(i=0; i<357; i++)
       {
         *(e_co+i) = 0XFFFF;
         *(e_si+i) = 0;
       }

for(k=0; k<LASTK[N_group]; k++)
   {
     i = *(symbol_pr+k);
     *(e_co+i) = *(h_code+k);
     *(e_si+i) = *(h_size+k);

     *(code_pr+k) = *(h_code+k);
     *(code_length_pr+k) = *(h_size+k);
   }

if( fprint==1 )
  {
    fprintf(hh,"================ N_group=%d\n",N_group);
//    for(i=0; i<257; i++)
//       {
//         if( *(e_si+i)!=0 )  fprintf(hh,"************    ");
//                       else  fprintf(hh,"                ");
//         fprintf(hh,"symb=%3d size=%2d code =%5d ",i,*(e_si+i),*(e_co+i));
//         if( *(e_si+i)!=0 )  BBTS(*(e_si+i),*(e_co+i)); //BBTS(size,value);
//         fprintf(hh,"\n");
//       }
    fprintf(hh,"================\n");
    for(k=0; k<LASTK[N_group]; k++)
       {
         j=*(symbol_pr+k);
         fprintf(hh,"k=%3d symb=%3d size=%3d code=%5d ",k,j,*(h_size+k),*(h_code+k));
         if( *(e_si+j)!=0 )  BBTS(*(e_si+j),*(e_co+j)); //BBTS(size,value);
         fprintf(hh,"\n");
       }
//    if(N_group==0) fclose(hh);
  }
//fclose(hh);
return 0;
}
int SWSQ::huffman_coding( int N_group )
{
  byte BW,DB,*data;
  unsigned char *ch;
  ushort c,c1,c2;
  int d,i,j,k,size,LB;

  int   *count_pr;
  int   *code_length_pr;

  count_pr       = count_HUFFVAL[N_group];
  code_length_pr = code_length_HUFFVAL[N_group];

k=0;
for(i=0; i<256; i++)
   {
     j = (*count_pr) * (*code_length_pr);
     k += j;
     count_pr++;  code_length_pr++;
   }
i=(k>>3)<<3;
if( k>i )  {
             k >>= 3;
             k += 1;
           }
     else  {
             k >>= 3;
           }
c_data[N_group] = new byte[k+40];
if( c_data[N_group]==NULL )
  {
//    MessageBox(Frm->Handle,"No more memory for c_data","Compress",MB_OK);
    return -1;
  }

ushort *e_co,*e_si;
e_co = EHUFCO[N_group];
e_si = EHUFSI[N_group];

  k = 0;  LB = 0;  BW = 0;
  ch = _symbol[N_group];
  data = c_data[N_group];
  for(i=0; i<group[N_group].kh; i++)
     {
       j = *(ch + i);
       c = *(e_co+j);
       size = *(e_si+j);
       do{
           j = LB + size;
           if( j==8 )
             {
               DB = (byte)c;
//               BW += DB;
               BW = (byte)(BW+DB);
               *(data + k) = BW;
               BW = 0;  LB = 0;  k++;
               d = -10000;
             }
           else
             {
               if( j<8 )
                 {
                   LB += size;
                   d = 8 - LB;
                   DB = (byte)c;
                   DB = (byte)(DB<<d);
//                   BW += DB;
				   BW=(byte)(BW+DB);
                   d = -10000;
                   if( i==(group[N_group].kh-1) )
                     {
                       DB = (byte)(255>>LB);
//                       BW += DB;
                       BW = (byte)(BW+DB);
                       *(data + k) = BW;
                       k++;
                     }
                 }
               else
                 {
                   d = LB + size - 8;
                   c1 = (ushort)(c>>d);
                   DB = (byte)c1;
//                   BW += DB;
                   BW = (byte)(BW+DB);
                   *(data + k) = BW;
                   k++;
                   BW = 0;  LB = 0;  size = d;
                   c2 = (ushort)(c<<(16-d));
                   c = (ushort)(c2>>(16-d));
                 }
             }
         }while( d>0 );
     }
  number_bytes[N_group] = k;

return 0;
}

void SWSQ::form_bits(int N_group,byte *data,int &i,int &k,byte &BW,byte &DB,int &LB,
               ushort c,int size)
{
  int j,d;
  ushort c1,c2;
          do{
              j = LB + size;
              if( j==8 )
                {
                  DB = (byte)c;
//                  BW += DB;
				  BW=(byte)(BW+DB);
                  *(data + k) = BW;
                  BW = 0;  LB = 0;  k++;
                  d = -10000;
                }
              else
                {
                  if( j<8 )
                    {
                      LB += size;
                      d = 8 - LB;
                      DB = (byte)c;
                      DB = (byte)(DB<<d);
//                      BW += DB;
                      BW = (byte)(BW+DB);
                      d = -10000;
                      if( i==(group[N_group].kh_stream-1) )
                        {
                          DB = (byte)(255>>LB);
//                          BW += DB;
                          BW = (byte)(BW+DB);
                          *(data + k) = BW;
                          k++;
                        }
                    }
                  else
                    {
                      d = LB + size - 8;
                      c1 = (ushort)(c>>d);
                      DB = (byte)c1;
//                      BW += DB;
                      BW = (byte)(BW+DB);
                      *(data + k) = BW;
                      k++;
                      BW = 0;  LB = 0;  size = d;
                      c2 = (ushort)(c<<(16-d));
                      c = (ushort)(c2>>(16-d));
                    }
                }
            }while( d>0 );
}

int SWSQ::huffman_coding_compliance( int N_group )
{
  byte BW,DB,*data;
  ushort c;
  int n,i,j,k,size,LB;
  byte *_str;


MaxCZR(N_group);

ushort *e_co,*e_si;
e_co = EHUFCO[N_group];
e_si = EHUFSI[N_group];
_str = stream[N_group];

k=0;

for(i=0; i<group[N_group].kh_stream; i++)
   {
      n = *(_str+i);

      if( n<=MaxZRun ){
                         size = *(e_si+n);
                         k+=size;
                         continue;
                      }
      if( n==105 ){
                     size = *(e_si+n);
                     k+=size;
                     k+=8; i++;
                     continue;
                  }
      if( n==106 ){
                     size = *(e_si+n);
                     k+=size;
                     k+=16; i+=2;
                     continue;
                  }
      if( abs(n-180)<=MaxCoeff ){
                                   size = *(e_si+n);
                                   k+=size;
                                   continue;
                                }
      if( n==101 || n==102 ){
                              size = *(e_si+n);
                              k+=size;
                              k+=8; i++;
                              continue;
                            }
      if( n==103 || n==104 ){
                              size = *(e_si+n);
                              k+=size;
                              k+=16; i+=2;
                            }
   }
j=(k>>3)<<3;
if( k>j )  {
             k >>= 3;
             k += 1;
           }
     else  {
             k >>= 3;
           }

c_data[N_group] = new byte[k+40];
if( c_data[N_group]==NULL )
  {
//    MessageBox(Frm->Handle,"No more memory for c_data","Compress",MB_OK);
    return -1;
  }


k = 0;  LB = 0;  BW = 0;
data = c_data[N_group];

for(i=0; i<group[N_group].kh_stream; i++)
   {
      n = *(_str+i);
      if( n<=MaxZRun )
        {
           c = *(e_co+n);
           size = *(e_si+n);
           if( size!=0 )   form_bits(N_group,data,i,k,BW,DB,LB,c,size);
                    else   {
                              return -2;
                           }
           continue;
        }
      if( n==105 )
        {
           c = *(e_co+n);
           size = *(e_si+n);
           if( size!=0 )   form_bits(N_group,data,i,k,BW,DB,LB,c,size);
                    else   {
                              return -2;
                           }
           i++;
           c=*(_str+i);
           size=8;
           form_bits(N_group,data,i,k,BW,DB,LB,c,size);
           continue;
        }
      if( n==106 )
        {
           c = *(e_co+n);
           size = *(e_si+n);
           if( size!=0 )   form_bits(N_group,data,i,k,BW,DB,LB,c,size);
                    else   {
                              return -2;
                           }
           i++;
           c=*(_str+i);
           size=8;
           form_bits(N_group,data,i,k,BW,DB,LB,c,size);
           i++;
           c=*(_str+i);
           size=8;
           form_bits(N_group,data,i,k,BW,DB,LB,c,size);
           continue;
        }
      if( abs(n-180)<=MaxCoeff )
        {
           c = *(e_co+n);
           size = *(e_si+n);
           if( size!=0 )   form_bits(N_group,data,i,k,BW,DB,LB,c,size);
                    else   {
                              return -2;
                           }
           continue;
        }
      if( n==101 || n==102 )
        {
           c = *(e_co+n);
           size = *(e_si+n);
           if( size!=0 )   form_bits(N_group,data,i,k,BW,DB,LB,c,size);
                    else   {
                              return -2;
                           }
           i++;
           c=*(_str+i);
           size=8;
           form_bits(N_group,data,i,k,BW,DB,LB,c,size);
           continue;
        }
      if( n==103 || n==104 )
        {
           c = *(e_co+n);
           size = *(e_si+n);
           if( size!=0 )   form_bits(N_group,data,i,k,BW,DB,LB,c,size);
                    else   {
                              return -2;
                           }
           i++;
           c=*(_str+i);
           size=8;
           form_bits(N_group,data,i,k,BW,DB,LB,c,size);
           i++;
           c=*(_str+i);
           size=8;
           form_bits(N_group,data,i,k,BW,DB,LB,c,size);
        }
   }


  number_bytes[N_group] = k;

return 0;
}

int SWSQ::huffman_decoding( int N_group )
{
  int i,j,k,r,ff,code_length,length,nb,d1,d2,count;
  ushort *e_co,*e_si,S1,S2,L,DL,code;
  //unsigned char *data,*d_data,B1,B2,B3,B4;
  unsigned char *data,B1,B2,B3,B4;
  ushort *HS,*HC;

  d_data=NULL;
  HS = HUFFSIZE[N_group];
  HC = HUFFCODE[N_group];

  e_co = EHUFCO[N_group];
  e_si = EHUFSI[N_group];
  data = c_data[N_group];

              int ts__nom[256];
              int ts_code[256];
              int ts_size[256];

   for(i=0; i<256; i++)
      {
        ts__nom[i]  = i;
        ts_code[i] = *(e_co+i);
        ts_size[i] = *(e_si+i);
        if( ts_size[i]==0 )  ts_size[i] = 1000;
      }
   qs_3(ts_size,ts_code,ts__nom,0,255);
             int i1_d[17];
             int i2_d[17];

for(j=0; j<17; j++)
   {
      i1_d[j] = 0;
      i2_d[j] = 0;
   }
for(j=1; j<17; j++)
   {
      r = 0;
      for(i=0; i<256; i++)
         {
           if( ts_size[i]==j && r==0 ){
                                        r = 1; i1_d[j] = i;
                                      }
           if( ts_size[i]==(j+1) && r==1 )
                                      {
                                        r = 2; i2_d[j] = i;    break;
                                      }
           if( ts_size[i]==1000 && r==1 )
                                      {
                                        r = 2; i2_d[j] = i;    break;
                                      }
           if( i==255 ) {
                          if( r==1 ){
                                        r = 2; i2_d[j] = 256;  break;
                                    }
                               else  break;
                        }
         }
   }

for(r=0; r<2; r++)
   {
     if( number_bytes[N_group] < 4 ) {
                                       switch(number_bytes[N_group])
                                             {
                                                case  1:
                                                          B1 = *data;
                                                          B2 = 255;
                                                          B3 = 255;
                                                          B4 = 255;
                                                          nb = 1;
                                                          break;
                                                case  2:
                                                          B1 = *data;
                                                          B2 = *(data+1);
                                                          B3 = 255;
                                                          B4 = 255;
                                                          nb = 2;
                                                          break;
                                                case  3:
                                                          B1 = *data;
                                                          B2 = *(data+1);
                                                          B3 = *(data+2);
                                                          B4 = 255;
                                                          nb = 3;
                                                          break;
                                                default:

                                                          return -2;
                                             }
                                     }
                            else     {
                                       B1 = *data;
                                       B2 = *(data+1);
                                       B3 = *(data+2);
                                       B4 = *(data+3);
                                       nb = 4;
                                     }

     length = 0;  ff = 0;
     S1 = (ushort)(B1<<8);  
//	 S1 += (ushort)B2;
     S1 = (ushort)(S1+B2);
	 
     S2 = (ushort)(B3<<8);  
//	 S2 += (ushort)B4;
	 S2 = (ushort)(S2+B4);

   count=0;

   do{


       k=0;
       do{
           j = *(HS+k);
           code = *(HC+k);
           if( j==0 ) {
                        return -2;
                      }
           if( (S1>>(16-j))==code ) {
                                      for(i=i1_d[j]; i<i2_d[j]; i++)
                                         {
                                           if( code==ts_code[i])
                                             {
                                               i = ts__nom[i];
                                               break;
                                             }
                                         }
                                      break;
                                    }
           i=-1;
           k++;
         }while( j!=0 );
       if( i==-1 ){
                     return -2;
                  }
       if( r==1 ) {
                    *(d_data+count) = (unsigned char)i;
                  }
       count++;
       code_length = *(e_si+i);



       S1 = (ushort)(S1<<code_length);
       length += code_length;

       if( length<16 )
         {
           L = (ushort)(S2>>(16-code_length));
//           S1 += L;
           S1 = (ushort)(S1+L);
           S2 = (ushort)(S2<<code_length);

           if( nb>=number_bytes[N_group] )  {
                                               B3 = 255;
                                               B4 = 255;
                                               L = (ushort)(B3<<8);
//                                               L += (ushort)B4;
											   L=(ushort)(L+B4);
                                               DL = (ushort)(L>>(16-code_length));
//                                               S2 += DL;
											   S2 =(ushort)(S2+DL);
                                            }
         }
       else
         {
           if( length==16 )
             {
               L = (ushort)(S2>>(16-code_length));
//               S1 += L;
               S1 = (ushort)(S1+L);

               length = 0;
               if( nb>=number_bytes[N_group] )  {
                                                  B3 = 255;
                                                  B4 = 255;
                                                }
               else  {
                       B3 = *(data+nb); nb++;
                       if( nb>=number_bytes[N_group] )  {
                                                          B4 = 255;
                                                        }
                                                  else  {
                                                          B4 = *(data+nb);
                                                          nb++;
                                                        }
                     }
               S2 = (ushort)(B3<<8);
//               S2 += (ushort)B4;
			   S2=(ushort)(S2+B4);
             }
           else
             {
               d1 = length - 16;
               d2 = code_length - d1;
               L = (ushort)(S2>>(16-d2));
               L = (ushort)(L<<d1);
//               S1 += L;
			   S1=(ushort)(S1+L);
               DL = 0;
               if( nb>=number_bytes[N_group] )  {
                                                  B3 = 255;
                                                  B4 = 255;
                                                  L = (ushort)(B4<<8);
//                                                  L += (ushort)B4;
												  L=(ushort)(L+B4);
                                                  DL = (ushort)(L>>(16-d1));
                                                }
               else  {
                       B3 = *(data+nb); nb++;
                       if( nb>=number_bytes[N_group] )  {
                                                          B4 = 255;
                                                          L = (ushort)(B4<<8);
//                                                          L += (ushort)B4;
														  L=(ushort)(L+B4);
                                                          DL = (ushort)(L>>(16-d1));
                                                        }
                                                  else  {
                                                          B4 = *(data+nb);
                                                          nb++;
                                                        }
                     }
               S2 = (ushort)(B3<<8);  
//			   S2 += (ushort)B4;
			   S2=(ushort)(S2+B4);
               L = (ushort)(S2>>(16-d1));
//               S1 += L;
			   S1=(ushort)(S1+L);
               S2 = (ushort)(S2<<d1);
//               S2 += DL;
			   S2=(ushort)(S2+DL);

               length = d1;

             }
         }

    if( nb>=number_bytes[N_group] && S1==0XFFFF )
      {
       size_b_image[N_group] = count;
       if( r==0 )
         {
           d_data = new byte[count+40];
           if( d_data==NULL )
             {
//               MessageBox(Frm->Handle,"No more memory for d_data","Compress",MB_OK);
               return -1;
             }
         }
       break;
      }
     } while( ff==0 );
   }

  int n,number;
  int *hi;
  unsigned char *_ch,hc;
//  int sif=size_int_huff[N_group];

  hi = int_huff[N_group];
  _ch = d_data;

  j = 0;
  for(i=0; i<size_b_image[N_group]; i++)
     {
//        if( j==sif )
//          {
//            break;
//          }
        hc = *(_ch+i);
        if( hc<101 )  {
                        for(number=0; number<hc; number++)
                           {
                             *(hi+j) = 0; j++;
//                             if( j==sif )
//                               {
//                                 break;
//                               }
                           }
                        continue;
                      }
        if( hc==105 ) {
                        i++;
                        hc = *(_ch+i);
                        for(number=0; number<hc; number++)
                           {
                             *(hi+j) = 0; j++;
//                             if( j==sif )
//                               {
//                                 break;
//                               }
                           }
                        continue;
                      }
        if( hc==106 ) {
                        i++;
                        n = *(_ch+i);
                        n=n<<8;
                        i++;
                        n += *(_ch+i);
                        for(number=0; number<n; number++)
                           {
                             *(hi+j) = 0; j++;
//                             if( j==sif )
//                               {
//                                 break;
//                               }
                           }
                        continue;
                      }
        if( hc>106 )  {
                        number = hc-180;
                        *(hi+j) = number; j++;
//                             if( j==sif )
//                               {
//                                 break;
//                               }
                        continue;
                      }
        if( hc==101 ) {
                        i++;
                        hc = *(_ch+i);
                        *(hi+j) = hc; j++;
//                             if( j==sif )
//                               {
//                                 break;
//                               }
                        continue;
                      }
        if( hc==102 ) {
                        i++;
                        number = *(_ch+i);
                        *(hi+j)= -number;  j++;
//                             if( j==sif )
//                               {
//                                 break;
//                               }
                        continue;
                      }
        if( hc==103 ) {
                        i++;
                        number = *(_ch+i);
                        number = number<<8;
                        i++;
                        number += *(_ch+i);
                        *(hi+j) = number;  j++;
//                             if( j==sif )
//                               {
//                                 break;
//                               }
                        continue;
                      }
        if( hc==104 ) {
                        i++;
                        number = *(_ch+i);
                        number = number<<8;
                        i++;
                        number += *(_ch+i);
                        *(hi+j) = -number;  j++;
//                             if( j==sif )
//                               {
//                                 break;
//                               }
                        continue;
                      }
     }
	 delete []  d_data;  d_data=NULL;
return 0;
}

int SWSQ::next_bits(int N_group,int count,int &nb,byte *data,ushort &S1,ushort &S2,int &length,int &size)
{

  byte B3,B4;
  ushort L,DL;
  int d1,d2,rep;

       if( (length+size)<16 )
         {
           S1 = (ushort)(S1<<size);
           length += size;
           L = (ushort)(S2>>(16-size));
//           S1 += L;
		   S1=(ushort)(S1+L);
           S2 = (ushort)(S2<<size);
           if( nb>=number_bytes[N_group] )  {
                                               B3 = 255;
                                               B4 = 255;
                                               L = (ushort)(B3<<8);
//                                               L += (ushort)B4;
											   L=(ushort)(L+B4);
                                               DL = (ushort)(L>>(16-size));
//                                               S2 += DL;
											   S2=(ushort)(S2+DL);
                                            }
         }
       else
         {
           if( (length+size)==16 )
             {
               S1 = (ushort)(S1<<size);
               L = (ushort)(S2>>(16-size));
//               S1 += L;
			   S1=(ushort)(S1+L);
               length = 0;
               if( nb>=number_bytes[N_group] )  {
                                                  B3 = 255;
                                                  B4 = 255;
                                                }
               else  {
                       B3 = *(data+nb); nb++;
                       if( nb>=number_bytes[N_group] )  {
                                                          B4 = 255;
                                                        }
                                                  else  {
                                                          B4 = *(data+nb);
                                                          nb++;
                                                        }
                     }
               S2 = (ushort)(B3<<8);
//               S2 += (ushort)B4;
			   S2=(ushort)(S2+B4);
             }
           else
             {

              d1 = length+size - 16;
              d2 = 16-length;
              size=d2;
              rep=next_bits(N_group,count,nb,data,S1,S2,length,size);
                  if( rep==-1)
                    {
                      return -1;
                    }
              size=d1;
              rep=next_bits(N_group,count,nb,data,S1,S2,length,size);
                  if( rep==-1)
                    {
                      return -1;
                    }
             }
         }
    if( nb>=number_bytes[N_group] && S1==0XFFFF )
      {
       size_b_image[N_group] = count;
       return -1; 
      }

return 0;
}

int SWSQ::huffman_decoding_compliance( int N_group )
{
  ushort *HS,*HC,S1,S2;
  //byte *data,*d_data,B1=255,B2=255,B3=255,B4=255,cs[65536],symb;
  byte *data,B1=255,B2=255,B3=255,B4=255,cs[65536],symb;
//  byte ui,ui1,ui2;
  int i,j,k,size,nb,length,rep=0,count;
  ushort *e_co,code;

MaxCZR(N_group);

  d_data=NULL;

  e_co = EHUFCO[N_group];
  HS = HUFFSIZE[N_group];
  HC = HUFFCODE[N_group];

  data = c_data[N_group];

  for(i=0; i<65536; i++)
       cs[i]=0;

  for(k=0; k<LASTK[N_group]; k++)
     {
       code=*(HC+k); j=-1;
       for(i=0; i<256; i++)
          {
            if( *(e_co+i)==code )
              {
                 j=0;
                 break;
              }
          }
       if( j==0 ) cs[code]=(byte)i;
             else {
                    return -2;
                  }
     }
if( number_bytes[N_group]>3 )
  {
    B1=*data;
    B2=*(data+1);
    B3=*(data+2);
    B4=*(data+3);
  }
else if( number_bytes[N_group]==1 )
       {
         B1=*data;
         B2=0xff;
         B3=0xff;
         B4=0xff;
       }
else if( number_bytes[N_group]==2 )
       {
         B1=*data;
         B2=*(data+1);
         B3=0xff;
         B4=0xff;
       }
else if( number_bytes[N_group]==3 )
       {
         B1=*data;
         B2=*(data+1);
         B3=*(data+2);
         B4=0xff;
       }

//byte *d_buffer = new byte[rows_wsq*cols_wsq];
  d_buffer = new byte[rows_wsq*cols_wsq+40];
  if( d_buffer==NULL )
    {
       return -1;
    }

byte *d_d,*d_b;
  d_b=d_buffer;

  S1 = (ushort)(B1<<8);  
//  S1 += (ushort)B2;
  S1=(ushort)(S1+B2);
  S2 = (ushort)(B3<<8);
//  S2 += (ushort)B4;
  S2=(ushort)(S2+B4);
  nb=4;  length=0;  count=0;
  do{
      k=0;
      do{
          size=*(HS+k);
          if( size==0 ) {
                           return -2;
                        }
          code=*(HC+k);

          if( (S1>>(16-size))==code )
            {
              symb=cs[code];
              if( symb==0 ) {
                               return -2;
                            }
              //*(d_buffer+count)=symb;
              *d_b=symb; d_b++;
              count++;
              rep=next_bits(N_group,count,nb,data,S1,S2,length,size);
              if( symb==101 || symb==102 || symb==105 )
                {
//                  ushort us;
//                  us=(ushort)(S1>>8);
//                  ui=(byte)us;
//                  *(d_buffer+count)=ui;

                  //*(d_buffer+count)=(byte)(S1>>8);
                  *d_b=(byte)(S1>>8); d_b++;


                  count++;
                  size=8;
                  rep=next_bits(N_group,count,nb,data,S1,S2,length,size);
                  break;
                }
              if( symb==103 || symb==104 || symb==106 )
                {
                  //ushort dd;
                  //dd=S1;
                  //ui1=(byte)(dd>>8);
                  //ui2=(byte)((dd<<8)>>8);
                  //*(d_buffer+count)=ui1;
                  //count++;
                  //*(d_buffer+count)=ui2;

                  *d_b=(byte)(S1>>8); d_b++;
                  *d_b=(byte)((S1<<8)>>8); d_b++;

                  count+=2;

                  size=16;
                  rep=next_bits(N_group,count,nb,data,S1,S2,length,size);
                }
              break;
            }
          k++;
        }while(size!=0);
    }while(rep==0);
  d_data = new byte[count+40];
  if( d_data==NULL )
    {
       delete []  d_buffer;  d_buffer=NULL;
       return -1;
    }
//byte *d_d,*d_b;
  d_d=d_data;
  d_b=d_buffer;
  for(k=0; k<count; k++)
     {
          *d_d=*d_b;
          d_d++; d_b++;
     }
  delete []  d_buffer;  d_buffer=NULL;

  int n,number;
  int *hi;
  unsigned char *_ch,hc;

  hi = int_huff[N_group];
  _ch = d_data;

  j = 0;



  int sif=size_int_huff[N_group];

  for(i=0; i<size_b_image[N_group]; i++)
     {
        if( j==sif )
          {
            break;
          }
        hc = *(_ch+i);
        if( hc==101 ) {
                        i++;
                        hc = *(_ch+i);
                        n=(int)hc;
                        *(hi+j) = n; j++;
                        if( j==sif )
                          {
                            break;
                          }
                        continue;
                      }
        if( hc==102 ) {
                        i++;
                        hc = *(_ch+i);
                        n = (int)hc;
                        *(hi+j)= -n;  j++;
                        if( j==sif )
                          {
                            break;
                          }
                        continue;
                      }
        if( hc==103 ) {
                        i++;
                        hc = *(_ch+i);
                        number = (int)hc;
                        number = number<<8;

                        i++;
                        hc = *(_ch+i);
                        n=hc;
                        number += n;
                        *(hi+j) = number;  j++;
                        if( j==sif )
                          {
                            break;
                          }
                        continue;
                      }
        if( hc==104 ) {
                        i++;
                        hc = *(_ch+i);
                        number = (int)hc;
                        number = number<<8;
                        i++;
                        hc = *(_ch+i);
                        n=(int)hc;
                        number += n;
                        *(hi+j) = -number;  j++;
                        if( j==sif )
                          {
                           break;
                          }
                        continue;
                      }
        if( hc==105 ) {
                        i++;
                        hc = *(_ch+i);
                        n = (int)hc;
                        for(number=0; number<n; number++)
                           {
                             *(hi+j) = 0; j++;
                             if( j==sif )
                               {
                                 break;
                               }
                           }
                        continue;
                      }
        if( hc==106 ) {
                        i++;
                        hc = *(_ch+i);
                        n = (int)hc;
                        n=n<<8;
                        i++;
                        hc = *(_ch+i);
                        int n_=(int)hc;
                        n += n_;
                        for(number=0; number<n; number++)
                           {
                             *(hi+j) = 0;
                             j++;
                             if( j==sif )
                               {
                                  break;
                               }
                           }
                        continue;
                      }
        if( hc<=100 ) {
                        n=(int)hc;
                        for(number=0; number<n; number++)
                           {
                             *(hi+j) = 0; j++;
                             if( j==sif )
                               {
                                 break;
                               }
                           }
                        continue;
                      }
        if( hc>106 )
          {
              number = (int)hc-180;
              *(hi+j) = number; j++;
              if( j==sif )
                {
                  break;
                }
          }
     }

  delete []  d_data;  d_data=NULL;
  return 0;
}


int SWSQ::encoding(byte *image, byte *outbuf, int r_wsq, int c_wsq, float press,
                   WORD impNumber)
{
   int i,j;
   quant_table=NULL;
   buf_float=NULL;
   float MMM,RRR;
   float hif[32],lof[32];	       
//   float hif_inv[32];   	       
   float lof_inv[32];	         

   fprint=0;
   c_coeff=(float)0.44;
   for(i=0; i<8; i++)
      {
        number_bytes[i]=0;
        size_b_image[i]=0;
      }
version_sgk=3;
int vr_sgk=1;

for(i=0; i<3; i++)
for(j=0; j<33; j++)
    BITS[i].L[j]=0;


cols_wsq=c_wsq;
rows_wsq=r_wsq;


number_group=3;
group[0].begin =  0;
group[0].end   = 18;
group[1].begin = 19;
group[1].end   = 51;
group[2].begin = 52;
group[2].end   = 59;

#ifdef SOURCE_FBI
   hif[0] = (float) 0.788485616405660;  //
   hif[1] = (float)-0.418092273222210;  //
   hif[2] = (float)-0.040689417609558;  //
   hif[3] = (float) 0.064538882628938;  //

   lof[0] = (float) 0.852698679009400;  //
   lof[1] = (float) 0.377402855612650;  //
   lof[2] = (float)-0.110624404418420;  //
   lof[3] = (float)-0.023849465019380;  //
   lof[4] = (float) 0.037828455506995;  //
#endif
#ifdef SOURCE_LA
//   lof_inv[0] = 0.788485616405660;  //
//   lof_inv[1] = 0.418092273222210;  //
//   lof_inv[2] =-0.040689417609558;  //
//   lof_inv[3] =-0.064538882628938;  //
//
   lof[0] = 0.852698679009400;      //
   lof[1] = 0.377402855612650;      //
   lof[2] =-0.110624404418420;      //
   lof[3] =-0.023849465019380;      //
   lof[4] = 0.037828455506995;      //
#endif

// ________________________________________________________________________________

#ifdef SOURCE_FBI
//   hif_inv[0] = lof[0];  //
//   hif_inv[1] =-lof[1];  //
//   hif_inv[2] = lof[2];  //
//   hif_inv[3] =-lof[3];  //
//   hif_inv[4] = lof[4];  //

   lof_inv[0] = hif[0];  //
   lof_inv[1] =-hif[1];  //
   lof_inv[2] = hif[2];  //
   lof_inv[3] =-hif[3];  //
#endif
#ifdef SOURCE_LA
//   hif_inv[0] =-lof[0];  //
//   hif_inv[1] = lof[1];  //
//   hif_inv[2] =-lof[2];  //
//   hif_inv[3] = lof[3];  //
//   hif_inv[4] =-lof[4];  //

   hif[0] =-lof_inv[0];  //
   hif[1] = lof_inv[1];  //
   hif[2] =-lof_inv[2];  //
   hif[3] = lof_inv[3];  //
#endif
// ________________________________________________________________________________

 lof4=lof[4];
 lof3=lof[3];
 lof2=lof[2];
 lof1=lof[1];
 lof0=lof[0];

 hif3=hif[3];
 hif2=hif[2];
 hif1=hif[1];
 hif0=hif[0];

   buf_float = new float[rows_wsq*cols_wsq+40];
   if(buf_float==NULL)  return -1;
memset(buf_float,0,(rows_wsq*cols_wsq+40)*4);

   quant_table = new int[rows_wsq*cols_wsq+40];
   if(quant_table==NULL) {
                              delete []  buf_float;  buf_float=NULL;
                              return -1;
                         }
memset(quant_table,0,(rows_wsq*cols_wsq+40)*4);

if(cols_wsq>rows_wsq)
  {
   y = new float[2*(cols_wsq+40)];
   if (!y)  {
                delete []  buf_float;  buf_float=NULL;
                delete [] quant_table;  quant_table=NULL;
                return -1;
            }
memset(y,0,(2*(cols_wsq+40))*4);
    a0 = new float[(cols_wsq+40)/2];
    if (!a0) {
                 delete []  buf_float;  buf_float=NULL;
                 delete []  quant_table;  quant_table=NULL;
                 delete []  y;  y=NULL;
                 return -1;
             }
memset(a0,0,((cols_wsq+40)/2)*4);
    a1 = new float[(cols_wsq+40)/2];
    if (!a1) {
                 delete [] buf_float;  buf_float=NULL;
                 delete [] quant_table;  quant_table=NULL;
                 delete [] y;  y=NULL;
                 delete [] a0;	a0=NULL;
                 return -1;
             }
memset(a1,0,((cols_wsq+40)/2)*4);
  }
else
  {
   y = new float[2*(rows_wsq+40)];
memset(y,0,(2*(rows_wsq+40))*4);
   if (!y)  {
                delete [] buf_float;  buf_float=NULL;
                delete [] quant_table;  quant_table=NULL;
                return -1;
            }
    a0 = new float[(rows_wsq+40)/2];
    if (!a0) {
                 delete [] buf_float;  buf_float=NULL;
                 delete [] quant_table;  quant_table=NULL;
                 delete [] y;  y=NULL;
                 return -1;
             }
memset(a0,0,((rows_wsq+40)/2)*4);
    a1 = new float[(rows_wsq+40)/2];
    if (!a1) {
                 delete [] buf_float;  buf_float=NULL;
                 delete [] quant_table;  quant_table=NULL;
                 delete [] y;  y=NULL;
                 delete [] a0;  a0=NULL;
                 return -1;
             }
memset(a1,0,((rows_wsq+40)/2)*4);
  }

int k;
float min=19999,max=-19999;
//float fl=0.;
byte *pti;
float *pti_f,*pti_f0;

pti=image;
pti_f=buf_float;
double dfl=0;
for(i=0; i<(rows_wsq*cols_wsq); i++)
   {
     k=*pti;
     if( k<min ) min=(float)k;
     if( k>max ) max=(float)k;
     dfl+=(double)k;
     *pti_f=(float)k;
     pti++;
     pti_f++;
   }

/*
for(i=0; i<rows_wsq; i++)
for(j=0; j<cols_wsq; j++)
   {
     k=*pti;
     if( k<min ) min=k;
     if( k>max ) max=k;
     dfl+=(double)k;
     *pti_f=(float)k;
     pti++;
     pti_f++;
   }
*/
dfl/=((double)rows_wsq*cols_wsq);
MMM=(float)dfl;
if( min==max ) RRR=1;
          else {
                 if( fabs(min-MMM)<fabs(max-MMM) )  RRR=(float)fabs(max-MMM)/(128);
                                              else  RRR=(float)fabs(min-MMM)/(128);
               }

float af;
ushort as;
byte scexp;

float rr=1/RRR;
pti_f0=buf_float-cols_wsq-1;
for(i=0; i<rows_wsq; i++)
   {
    pti_f0+=cols_wsq;
    pti_f=pti_f0;
for(j=0; j<cols_wsq; j++)
   {
    pti_f++;
    *pti_f=(*pti_f-MMM)*rr;
   }
   }

int x1,x2,y1,y2,type=0;
/*
int dx,dy;
for(int level=0; level<number_levels; level++)
   {
    dx = cols_wsq>>level;
    dy = rows_wsq>>level;
    switch(level)
          {
           case  0:
                    _subband( 0, 0, dx, dy , vr_sgk*level, 0 ,0);
                    break;
           case  1:
                    for(i=0; i<4; i++)
                        _subband( w[0][i].x1, w[0][i].y1, w[0][i].x2, w[0][i].y2, vr_sgk*level, i*4 ,0);
                    break;
           case  2:
                    for(i=0; i<3; i++)
                        _subband( w[1][i].x1, w[1][i].y1, w[1][i].x2, w[1][i].y2, vr_sgk*level, i*4 ,0);
                    break;
           case  3:
                    for(i=0; i<12; i++)
                        _subband( w[2][i].x1, w[2][i].y1, w[2][i].x2, w[2][i].y2, vr_sgk*level, i*4 ,0);
                    break;
           case  4:
                    _subband( w[3][ 0].x1, w[3][ 0].y1, w[3][ 0].x2, w[3][ 0].y2, vr_sgk*level,  0 ,0);
                    break;
           default:
                    break;
          }
   }
*/

for(int level=0; level<number_levels; level++)
   {
    switch(level)
          {
           case  0:
                    _subband( 0, 0, cols_wsq, rows_wsq , -level, 0 ,0);
                    subband_decomp(buf_float, 0, 0, cols_wsq, rows_wsq);
                    _subband( 0, 0, cols_wsq, rows_wsq , level, 0 ,0);
                    break;
           case  1:
                    for(i=0; i<4; i++)
                       {
                        switch(i)
                           {
                               case 0:  type=0;  break;
                               case 1:  type=2;  break;
                               case 2:  type=1;  break;
                               case 3:  type=3;  break;
                           }
                        _subband( w[0][i].x1, w[0][i].y1, w[0][i].x2, w[0][i].y2, -level, i*4 ,type);
                        subband_decomp(buf_float, w[0][i].x1, w[0][i].y1, w[0][i].x2, w[0][i].y2 );
                        j=rotate(buf_float,w[0][i].x1, w[0][i].y1, w[0][i].x2, w[0][i].y2,vr_sgk*level,i*4,type);
                        if( j!=0 ) return j;
                        _subband( w[0][i].x1, w[0][i].y1, w[0][i].x2, w[0][i].y2, level, i*4 ,type);
                       }
                    break;
           case  2:
                    for(i=0; i<3; i++)
                       {
                        switch(i)
                           {
                               case 0:  type=0;  break;
                               case 1:  type=2;  break;
                               case 2:  type=1;  break;
                           }
                        _subband( w[1][i].x1, w[1][i].y1, w[1][i].x2, w[1][i].y2, -level, i*4 ,type);
                        subband_decomp(buf_float, w[1][i].x1, w[1][i].y1, w[1][i].x2, w[1][i].y2 );
                        j=rotate(buf_float, w[1][i].x1, w[1][i].y1, w[1][i].x2, w[1][i].y2,vr_sgk*level,i*4,type);
                        if( j!=0 ) return j;
                        _subband( w[1][i].x1, w[1][i].y1, w[1][i].x2, w[1][i].y2, level, i*4 ,type);
                       }
                    break;
           case  3:
                    for(i=0; i<12; i++)
                       {
                        switch(i)
                           {
                               case  0:  type=0;  break;
                               case  1:  type=2;  break;
                               case  2:  type=1;  break;
                               case  3:  type=3;  break;

                               case  4:  type=0;  break;
                               case  5:  type=2;  break;
                               case  6:  type=1;  break;
                               case  7:  type=3;  break;

                               case  8:  type=0;  break;
                               case  9:  type=2;  break;
                               case 10:  type=1;  break;
                               case 11:  type=3;  break;
                           }
                        _subband( w[2][i].x1, w[2][i].y1, w[2][i].x2, w[2][i].y2, -level, i*4 ,type);
                        subband_decomp(buf_float, w[2][i].x1, w[2][i].y1, w[2][i].x2, w[2][i].y2 );
                        j=rotate(buf_float, w[2][i].x1, w[2][i].y1, w[2][i].x2, w[2][i].y2,vr_sgk*level,i*4,type);
                        if( j!=0 ) return j;
                        _subband( w[2][i].x1, w[2][i].y1, w[2][i].x2, w[2][i].y2, level, i*4 ,type);
                       }
                    break;
           case  4:
                    _subband( w[3][ 0].x1, w[3][ 0].y1, w[3][ 0].x2, w[3][ 0].y2, -level,  0 ,0);
                    subband_decomp(buf_float, w[3][ 0].x1, w[3][ 0].y1, w[3][ 0].x2, w[3][ 0].y2 );
                    _subband( w[3][ 0].x1, w[3][ 0].y1, w[3][ 0].x2, w[3][ 0].y2, level,  0 ,0);
                    break;
           default:
                    break;
          }
   }

for(i=0; i<4; i++)
   {
     bb[i].x1=w[4][i].x1;
     bb[i].y1=w[4][i].y1;
     bb[i].x2=w[4][i].x2;
     bb[i].y2=w[4][i].y2;
   }

for(i=4; i<51; i++)
   {
     bb[i].x1=w[3][i-3].x1;
     bb[i].y1=w[3][i-3].y1;
     bb[i].x2=w[3][i-3].x2;
     bb[i].y2=w[3][i-3].y2;
   }

     bb[51].x1=w[1][3].x1;
     bb[51].y1=w[1][3].y1;
     bb[51].x2=w[1][3].x2;
     bb[51].y2=w[1][3].y2;

for(i=52; i<64; i++)
   {
     bb[i].x1=w[1][i-48].x1;
     bb[i].y1=w[1][i-48].y1;
     bb[i].x2=w[1][i-48].x2;
     bb[i].y2=w[1][i-48].y2;
   }
                             delete [] y;  y=NULL;
                             delete [] a0;  a0=NULL;
                             delete [] a1;  a1=NULL;









int *o,n,m,s;

for(i=0; i<number_group; i++)
   {
     s=0;
     for(j=group[i].begin; j<=group[i].end; j++)
        {
         s+=((bb[j].x2-bb[j].x1)*(bb[j].y2-bb[j].y1));
        }
     int_huff[i] = new int[s+40];
memset(int_huff[i],0,(s+40)*4);

     if(int_huff[i]==NULL) {
                             delete [] buf_float;  buf_float=NULL;
                             delete [] quant_table;  quant_table=NULL;
                             return -1;
                           }
   }


int re=0;
do{
    if( re!=0 ) press=(float)1.4*press;
    re=0;

    bin_width_iteration(buf_float,press);
//    bin_width_iteration(buf_float,press,bb_tst);

#ifdef QUANT
    for(i=0; i<60; i++)
        quantization(buf_float, i, quant_table );
#endif

for(i=0; i<number_group; i++)
   {
     s=0;
     for(j=group[i].begin; j<=group[i].end; j++)
        {
         s+=((bb[j].x2-bb[j].x1)*(bb[j].y2-bb[j].y1));
        }

int *q,*q0;
     s=0;
     o=int_huff[i];
     for(j=group[i].begin; j<=group[i].end; j++)
        {
          if( bb[j].bin_width!=0.0 )
            {
              x1=bb[j].x1;    x2=bb[j].x2;
              y1=bb[j].y1;    y2=bb[j].y2;
              q0 = quant_table+(y1-1)*cols_wsq+x1;

////////////////////////////////////////////////////////////////////////////////
#ifdef CERTIFICATION_ENCODER
          if(j==number_of_subband)
            {
              n_str=0; n_str_old=0;
              fprintf(h3," %5d:",n_str);
              lseek(hhhhh_nist,7,SEEK_SET);
            }
          int sc=0;
#endif
////////////////////////////////////////////////////////////////////////////////


              for(m=y1; m<y2; m++)
                 {
                   q0 += cols_wsq;
                   q = q0;
                   //q=quant_table+m*cols_wsq+x1;
                   for(n=x1; n<x2; n++)
                      {
////////////////////////////////////////////////////////////////////////////////

                        if( *q>65534 || *q<-65534 )
                          {
                            re=1;
                            break;
                          }
                        *(o+s) = *q;
                        q++; s++;
                      }
                 }
            }
        }
     group[i].kh0 = s;
   }

   }while(re!=0);





  delete [] quant_table;  quant_table=NULL;
  delete [] buf_float;  buf_float=NULL;

for(i=0; i<number_group; i++)
   {
    if( version_sgk<2 )   n = code_symbol(i);
                   else   n = code_symbol_compliance(i);
    if( n!=0 )  return n;
   }
#ifdef NIST
        n = probability(0);
        if( n!=0 )  return n;
        n = build_huffman_tables(0);
        if( n!=0 )  return n;

              int sum_kh;
              sum_kh=group[1].kh+group[2].kh;
//              byte * _symbol_23;
              _symbol_23 = new byte[sum_kh+40];

                      for(j=0; j<group[1].kh; j++)
                          *(_symbol_23+j)=*(_symbol[1]+j);
                      for(j=0; j<group[2].kh; j++)
                          *(_symbol_23+group[1].kh+j)=*(_symbol[2]+j);

//              byte *_symbol_old = new byte[group[1].kh];
              _symbol_old = new byte[group[1].kh+40];

                      for(j=0; j<group[1].kh; j++)
                          *(_symbol_old+j)=*(_symbol[1]+j);

              delete [] _symbol[1];  _symbol[1]=NULL;
              _symbol[1] = new byte[sum_kh+40];
              for(k=0; k<sum_kh; k++)
                  *(_symbol[1]+k)=*(_symbol_23+k);

int kh_old=group[1].kh;
        group[1].kh=sum_kh;
        n = probability(1);
        group[1].kh=kh_old;
        if( n!=0 )  return n;
        n = build_huffman_tables(1);
        if( n!=0 )  return n;

              for(k=1; k<17; k++)
                  BITS[2].L[k] = BITS[1].L[k];
              LASTK[2] = LASTK[1];
              int *pr;
              pr = symbol_HUFFVAL[2];
              for(k=0; k<LASTK[2]; k++)
                  *(pr+k) = *(symbol_HUFFVAL[1]+k);  
              n=build_huffman_tables(2);
              if( n!=0 ) return n;


       delete [] _symbol[1];  _symbol[1]=NULL;
       _symbol[1] = new byte[group[1].kh+40];
       for(j=0; j<group[1].kh; j++)
           *(_symbol[1]+j)=*(_symbol_old+j);


       delete [] _symbol_old;  _symbol_old=NULL;
       delete [] _symbol_23;  _symbol_23=NULL;

#endif
#ifndef NIST
for(i=0; i<number_group; i++)
   {
        n = probability(i);
        if( n!=0 )  return n;
        n = build_huffman_tables(i);
        if( n!=0 )  return n;
   }
#endif
for(i=0; i<number_group; i++)
   {
    if( version_sgk<2 )   n = huffman_coding(i);
                   else   n = huffman_coding_compliance(i);
    if( n!=0 )  return n;
   }

int bt;
ushort shi,shj;
byte bt1,bt2;

  bt = 0;
   *outbuf = 0XFF;  outbuf++;  bt++;  
   *outbuf = 0XA0;  outbuf++;  bt++;  

#ifdef CALIBER_COMMENTS_ENABLE
   *outbuf = 0XFF;  outbuf++;  bt++;  
   *outbuf = 0XA8;  outbuf++;  bt++;  

   shi = 21;
   shj = (ushort)(shi>>8);    bt1 = (byte)shj;
   shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;

   *outbuf = bt1;   outbuf++;  bt++;  
   *outbuf = bt2;   outbuf++;  bt++;  
   *outbuf = 'W';   outbuf++;  bt++;  
   *outbuf = 'S';   outbuf++;  bt++;  
   *outbuf = 'Q';   outbuf++;  bt++;  
   *outbuf = '-';   outbuf++;  bt++;  
   *outbuf = 'C';   outbuf++;  bt++;  
   *outbuf = 'A';   outbuf++;  bt++;  
   *outbuf = 'L';   outbuf++;  bt++;  
   *outbuf = 'I';   outbuf++;  bt++;  
   *outbuf = 'B';   outbuf++;  bt++;  
   *outbuf = 'R';   outbuf++;  bt++;  
   *outbuf = 'E';   outbuf++;  bt++;  
   *outbuf = ' ';   outbuf++;  bt++;  
   *outbuf = 'S';   outbuf++;  bt++;  
   *outbuf = 'G';   outbuf++;  bt++;  
   *outbuf = 'K';   outbuf++;  bt++;  
   *outbuf = ' ';   outbuf++;  bt++;  
   *outbuf = ' ';   outbuf++;  bt++;  
   *outbuf = '0';   outbuf++;  bt++;  
   *outbuf = '3';   outbuf++;  bt++;  

#endif
#ifdef NON_CALIBER_COMMENTS_ENABLE

   *outbuf = 0XFF;  outbuf++;  bt++;  
   *outbuf = 0XA8;  outbuf++;  bt++;  

   shi = 21;
   shj = shi>>8;    bt1 = (byte)shj;
   shj = shi<<8;    shj = shj>>8;    bt2 = (byte)shj;

   *outbuf = bt1;   outbuf++;  bt++;  
   *outbuf = bt2;   outbuf++;  bt++;  
   *outbuf = 'W';   outbuf++;  bt++;  
   *outbuf = 'S';   outbuf++;  bt++;  
   *outbuf = 'Q';   outbuf++;  bt++;  
   *outbuf = ' ';   outbuf++;  bt++;  
   *outbuf = 'K';   outbuf++;  bt++;  
   *outbuf = 'A';   outbuf++;  bt++;  
   *outbuf = 'L';   outbuf++;  bt++;  
   *outbuf = 'M';   outbuf++;  bt++;  
   *outbuf = 'Y';   outbuf++;  bt++;  
   *outbuf = 'K';   outbuf++;  bt++;  
   *outbuf = 'O';   outbuf++;  bt++;  
   *outbuf = 'V';   outbuf++;  bt++;  
   *outbuf = ' ';   outbuf++;  bt++;  
   *outbuf = 'S';   outbuf++;  bt++;  
   *outbuf = '.';   outbuf++;  bt++;  
   *outbuf = 'G';   outbuf++;  bt++;  
   *outbuf = '.';   outbuf++;  bt++;  
   *outbuf = '0';   outbuf++;  bt++;  
   *outbuf = '3';   outbuf++;  bt++;  

#endif

   *outbuf = 0XFF;  outbuf++;  bt++;  
   *outbuf = 0XA4;  outbuf++;  bt++;  

        shi = 58;
        shj = (ushort)(shi>>8);    bt1 = (byte)shj;
        shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
        *outbuf = bt1;   outbuf++;  bt++;  
        *outbuf = bt2;   outbuf++;  bt++;  

        *outbuf = 9;     outbuf++;  bt++;  
        *outbuf = 7;     outbuf++;  bt++;  

unsigned int ai;
   for(j=0; j<5; j++)
      {
        if( lof[j]>=0 ) {
                          *outbuf = 0;     outbuf++;  bt++;  
                        }
                   else {
                          *outbuf = 1;     outbuf++;  bt++;  
                        }
        af = (float)fabs(lof[j]);
        scexp = 0;
        if( af!= 0 )
          {
            if( af<4294967295. )
              {
                while( af<4294967295. )
                     {
                      af *= 10;
                      scexp++;
                     }
                scexp--;
//                af /= 10;
                af *= (float)0.1;
              }
          }
        ai = (unsigned int)af;
        *outbuf = scexp;  outbuf++;  bt++;  
        i = ai;
        k = i>>24;    bt1 = (byte)k;
        *outbuf = bt1;   outbuf++;  bt++;  
        k = i<<8;    k = k>>24;    bt1 = (byte)k;
        *outbuf = bt1;   outbuf++;  bt++;  
        k = i<<16;    k = k>>24;    bt1 = (byte)k;
        *outbuf = bt1;   outbuf++;  bt++;  
        k = i<<24;    k = k>>24;    bt1 = (byte)k;
        *outbuf = bt1;   outbuf++;  bt++;  
      }

   for(j=0; j<4; j++)
      {
        if( hif[j]>=0 ) {
                          *outbuf = 0;     outbuf++;  bt++;  
                        }
                   else {
                          *outbuf = 1;     outbuf++;  bt++;  
                        }
        af = (float)fabs(hif[j]);
        scexp = 0;
        if( af!= 0 )
          {
            if( af<4294967295. )
              {
                while( af<4294967295. )
                     {
                       af *= 10;
                       scexp++;
                     }
                scexp--;
//                af /= 10;
                af *= (float)0.10;
              }
          }
        ai = (unsigned int)af;
        *outbuf = scexp;  outbuf++;  bt++;  
        i = ai;
        k = i>>24;    bt1 = (byte)k;
        *outbuf = bt1;   outbuf++;  bt++;  
        k = i<<8;    k = k>>24;    bt1 = (byte)k;
        *outbuf = bt1;   outbuf++;  bt++;  
        k = i<<16;    k = k>>24;    bt1 = (byte)k;
        *outbuf = bt1;   outbuf++;  bt++;  
        k = i<<24;    k = k>>24;    bt1 = (byte)k;
        *outbuf = bt1;   outbuf++;  bt++;  
      }

   *outbuf = 0XFF;  outbuf++;  bt++;  
   *outbuf = 0XA5;  outbuf++;  bt++;  

        shi = 389;
        shj = (ushort)(shi>>8);    bt1 = (byte)shj;
        shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
        *outbuf = bt1;   outbuf++;  bt++;  
        *outbuf = bt2;   outbuf++;  bt++;  

        af = c_coeff;
        scexp = 0;
   if( af!= 0 )
     {
        if( af<65535 )
          {
            while( af<65535 )
                 {
                   af *= 10;
                   scexp++;
                 }
            scexp--;
//            af /= 10;
            af *= (float)0.10;
          }
     }
   as = (ushort)af;
        *outbuf = scexp;  outbuf++;  bt++;  
        shi = as;
        shj = (ushort)(shi>>8);    bt1 = (byte)shj;
        shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
        *outbuf = bt1;    outbuf++;  bt++;  
        *outbuf = bt2;    outbuf++;  bt++;  

   for(j=0; j<64; j++)
      {
        af = bb[j].bin_width;
        scexp = 0;
        if( af!= 0 )
          {
            if( af<65535 )
              {
                while( af<65535 )
                     {
                       af *= 10;
                       scexp++;
                     }
                scexp--;
//                af /= 10;
                af *= (float)0.10;
              }
          }
        as = (ushort)af;
        *outbuf = scexp;  outbuf++;  bt++;  
        shi = as;
        shj = (ushort)(shi>>8);    bt1 = (byte)shj;
        shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
        *outbuf = bt1;    outbuf++;  bt++;  
        *outbuf = bt2;    outbuf++;  bt++;  

        af = bb[j].zero_bin_width;
        scexp = 0;
        if( af!= 0 )
          {
            if( af<65535 )
              {
                while( af<65535 )
                     {
                       af *= 10;
                       scexp++;
                     }
                scexp--;
//                af /= 10;
                af *= (float)0.10;
              }
          }
        as = (ushort)af;
        *outbuf = scexp;  outbuf++;  bt++;  
        shi = as;
        shj = (ushort)(shi>>8);    bt1 = (byte)shj;
        shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
        *outbuf = bt1;    outbuf++;  bt++;  
        *outbuf = bt2;    outbuf++;  bt++;  
      }
   *outbuf = 0XFF;  outbuf++;  bt++;  
   *outbuf = 0XA2;  outbuf++;  bt++;  

        shi = 17;
        shj = (ushort)(shi>>8);    bt1 = (byte)shj;
        shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
        *outbuf = bt1;   outbuf++;  bt++;  
        *outbuf = bt2;   outbuf++;  bt++;  

        *outbuf = 0;     outbuf++;  bt++;  

        *outbuf = 255;   outbuf++;  bt++;  

        shi = (ushort)rows_wsq;
        shj = (ushort)(shi>>8);    bt1 = (byte)shj;
        shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
        *outbuf = bt1;   outbuf++;  bt++;  
        *outbuf = bt2;   outbuf++;  bt++;  

        shi = (ushort)cols_wsq;
        shj = (ushort)(shi>>8);    bt1 = (byte)shj;
        shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
        *outbuf = bt1;   outbuf++;  bt++;  
        *outbuf = bt2;   outbuf++;  bt++;  

        af = MMM;
        scexp = 0;
        if( af!= 0 )
          {
            if( af<65535 )
              {
                while( af<65535 )
                     {
                       af *= 10;
                       scexp++;
                     }
                scexp--;
//                af /= 10;
                af *= (float)0.10;
              }
          }
        as = (ushort)af;
        *outbuf = scexp;  outbuf++;  bt++;  
        shi = as;
        shj = (ushort)(shi>>8);    bt1 = (byte)shj;
        shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
        *outbuf = bt1;    outbuf++;  bt++;  
        *outbuf = bt2;    outbuf++;  bt++;  

        af = RRR;
        scexp = 0;
        if( af!= 0 )
          {
            if( af<65535 )
              {
                while( af<65535 )
                     {
                       af *= 10;
                       scexp++;
                     }
                scexp--;
//                af /= 10;
                af *= (float)0.10;
              }
          }
        as = (ushort)af;
        *outbuf = scexp;  outbuf++;  bt++;  
        shi = as;
        shj = (ushort)(shi>>8);
        bt1 = (byte)shj;
        shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
        *outbuf = bt1;    outbuf++;  bt++;  
        *outbuf = bt2;    outbuf++;  bt++;  

        *outbuf = 0x1;    outbuf++;  bt++;  
         
              
        *outbuf = (impNumber >> 8) & 0xff;    outbuf++;  bt++;  
        *outbuf = impNumber & 0xff;    outbuf++;  bt++;  
//        *outbuf = 0xb0;    outbuf++;  bt++;  
//        *outbuf = 0x90;    outbuf++;  bt++;  

byte *data;
int   *symbol_pr;

#ifdef NIST
     *outbuf = 0XFF;  outbuf++;  bt++;  
     *outbuf = 0XA6;  outbuf++;  bt++;  


     shi = 2;
     for(i=0; i<2; i++)
        {
          for(j=1; j<17; j++)
//              shi += BITS[i].L[j];
              shi = (ushort)(shi+BITS[i].L[j]);
          shi += (ushort)17;
        }

     shj = (ushort)(shi>>8);    bt1 = (byte)shj;
     shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
     *outbuf = bt1;   outbuf++;  bt++;  
     *outbuf = bt2;   outbuf++;  bt++;  

for(i=0; i<2; i++)
   {
     bt1 = (byte)i;
     *outbuf = bt1;   outbuf++;  bt++; 

     for(j=1; j<17; j++)
        {
          bt1 = BITS[i].L[j];
          *outbuf = bt1;   outbuf++;  bt++;  
        }
     symbol_pr      = symbol_HUFFVAL[i];
     for(j=0; j<LASTK[i]; j++)
        {
          shi = (ushort)(*(symbol_pr+j));
          bt1 = (byte)shi;
          *outbuf = bt1;   outbuf++;  bt++;  
        }
   }
#endif

for(i=0; i<number_group; i++)
   {

#ifndef NIST
     *outbuf = 0XFF;  outbuf++;  bt++;  
     *outbuf = 0XA6;  outbuf++;  bt++;  


     shi = 19;
     for(j=1; j<17; j++)
         shi += BITS[i].L[j];

     shj = (ushort)(shi>>8);    bt1 = (byte)shj;
     shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
     *outbuf = bt1;   outbuf++;  bt++;  
     *outbuf = bt2;   outbuf++;  bt++;  

     bt1 = (byte)i;
     *outbuf = bt1;   outbuf++;  bt++;  

     for(j=1; j<17; j++)
        {
         ///*
          bt1 = BITS[i].L[j];
          *outbuf = bt1;   outbuf++;  bt++;  
         //*/
        }
  symbol_pr      = symbol_HUFFVAL[i];
     for(j=0; j<LASTK[i]; j++)
        {
         ///*
          shi = (ushort)(*(symbol_pr+j));
          bt1 = (byte)shi;
          *outbuf = bt1;   outbuf++;  bt++;  
         //*/
        }
#endif
     *outbuf = 0XFF;  outbuf++;  bt++;  
     *outbuf = 0XA3;  outbuf++;  bt++;  

     shi = 3;
     shj = (ushort)(shi>>8);    bt1 = (byte)shj;
     shj = (ushort)(shi<<8);    shj = (ushort)(shj>>8);    bt2 = (byte)shj;
     *outbuf = bt1;   outbuf++;  bt++;  
     *outbuf = bt2;   outbuf++;  bt++;  
     switch(i)
           {
             case  0:    bt1 = 0; break;
             case  1:    bt1 = 1; break;
             case  2:    bt1 = 1; break;
             default:             break;
           }
//     bt1 = (byte)i;
          *outbuf = bt1;   outbuf++;  bt++;  

     data = c_data[i];
     for(j=0; j<number_bytes[i]; j++)
        {
          bt1 = *data;
          data++;
          *outbuf = bt1;   outbuf++;  bt++;
          if( bt1==0xFF ) {
                              *outbuf = 0;   outbuf++;  bt++;
                          }
        }

   }

   *outbuf = 0XFF;  outbuf++;  bt++;  
   *outbuf = 0XA1;  outbuf++;  bt++;  

for(i=0; i<number_group; i++)
   {
       delete []   int_huff[i];  int_huff[i]=NULL;

       delete []   _symbol[i];  _symbol[i]=NULL;
       delete []   stream[i];  stream[i]=NULL;

       delete []   HUFFSIZE[i];  HUFFSIZE[i]=NULL;
       delete []   HUFFCODE[i];  HUFFCODE[i]=NULL;
       delete []   EHUFCO[i];  EHUFCO[i]=NULL;
       delete []   EHUFSI[i];  EHUFSI[i]=NULL;
       delete []   c_data[i];  c_data[i]=NULL;

   }

return bt;
}

int SWSQ::decoding(byte *image, byte *inpbuf, int sizebuf)
//int decoding_sgk(byte *image, byte *inpbuf, int sizebuf)
{
byte *buf=inpbuf;
//byte bt1,bt2,bt3,bt4,L0,L1,Sign,Em,Er,Ex,Ec,Eq,Ez,Td;
byte bt1,bt2,bt3,bt4,L0,L1,Sign;
unsigned int Em,Er,Ex,Ec,Eq,Ez;
int i,j,last,dx,dy,kk=0,nn,mm;
unsigned int k,l,n,m;
//int *quant_table;
buf_float=NULL;
float MMM,RRR;
MMM = (float)0.0;
RRR = (float)0.0;
float hif_inv[32],lof_inv[32];	
float hif[32],lof[32];      	
Sfilters flt;

//------------------------------------------------------------------------------
#ifdef EXAM
j=0;
hh = fopen("huffm_","w");    //global_o++;
buf = inpbuf;
for(i=0; i<sizebuf-1; i++)
   {
     bt1 = *( buf + i     );
     bt2 = *( buf + i + 1 );

     if( bt1==255 && bt2==0xA0 ) {
                                   fprintf(hh,"\n");

                                 }
     if( bt1==255 && bt2==0xA1 ) {

                                 }
     if( bt1==255 && bt2==0xA2 ) {

                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA3 ) {

                                   bt2 = *( buf + i + 4 );
                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3);
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;
                                   bt2 = *( buf + i + 4);


                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA4 ) {

                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3 );
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;

                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA5 ) {

                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3 );
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;

                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA6 ) {


                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3);
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;
                                   fprintf(hh,"Lh=%d   ",l);
                                   bt2 = *( buf + i + 4 );

                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA7 ) {

                                   i = jump_along_buf(buf,i);
                                 }

     if( bt1==255 && bt2==0xB0 ) {

                                 }
     if( bt1==255 && bt2==0xB1 ) {

                                 }
     if( bt1==255 && bt2==0xB2 ) {

                                 }
     if( bt1==255 && bt2==0xB3 ) {

                                 }
     if( bt1==255 && bt2==0xB4 ) {

                                 }
     if( bt1==255 && bt2==0xB5 ) {

                                 }
     if( bt1==255 && bt2==0xB6 ) {

                                 }
     if( bt1==255 && bt2==0xB7 ) {

                                 }
     if( bt1==255 && bt2==0xA8 ) {
                                   j++;
                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3 );
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;
                                   if( l>21 ) {
                                                j--;
                                                continue;
                                              }

                                   fprintf(hh,"'");
                                   for(n=0; n<(l-2); n++)
                                      {
                                        fprintf(hh,"%c",*( buf + i + 4 +n ));
                                      }
                                   fprintf(hh,"'\n");
                                   i = jump_along_buf(buf,i);
                                 }

   }
fprintf(hh,"\n\n comments :%d\n\n",j);

fclose(hh);
#endif
//------------------------------------------------------------------------------

   fprint=0;
   c_coeff=(float)0.44;
   for(i=0; i<8; i++)
      {
        number_bytes[i]=0;
        size_b_image[i]=0;
      }

version_sgk=3;
///////////////////////////////////////////
fprint=0;
///////////////////////////////////////////
for(i=0; i<3; i++)
for(j=0; j<33; j++)
    BITS[i].L[j]=0;

number_group=3;
group[0].begin =  0;
group[0].end   = 18;
group[1].begin = 19;
group[1].end   = 51;
group[2].begin = 52;
group[2].end   = 59;

for(i=0; i<3; i++)
   number_bytes[i] = 0;

for(i=0; i<32; i++)
   {
     flt.hi_inv[i]=0.;
     flt.lo_inv[i]=0.;
     hif[i]=0.;
     lof[i]=0.;
   }

for(i=0; i<sizebuf-1; i++)
   {
     bt1 = *( buf + i     );
     bt2 = *( buf + i + 1 );

     if( bt1==255 && bt2==0xA2 ) {
                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA3 ) {
                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA4 ) {
                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA5 ) {
                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA6 ) {
                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA7 ) {
                                   i = jump_along_buf(buf,i);
                                 }
     if( bt1==255 && bt2==0xA8 ) {
                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3 );
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;

                                   ver=new char[l];
                                   for(n=0; n<(l-2); n++)
                                      {
                                        ver[n] = *( buf + i + 4 +n );
                                      }
                                   if( strcmp(ver,"WSQ-CALIBRE SGK  01")==0 )
                                     {
                                       version_sgk=1;
                                     }
                                   if( strcmp(ver,"WSQ-1234567 SGK  02")==0 )
                                     {
                                       version_sgk=2;
                                     }
                                   if( strcmp(ver,"WSQ-1234567 SGK  03")==0 )
                                     {
                                       version_sgk=3;
                                     }
                                   delete [] ver;  ver=NULL;
                                   break;
                                 }
   }


int vr_sgk=1;
if(version_sgk>2) vr_sgk=1;
             else vr_sgk=-1;
j=0;

int bar=1,nsh,nsh_i=sizebuf;
//////////////////////////////////////////////////////////////////////////////
    j=0;    nsh=0;
    for(i=0; i<sizebuf-1; i++)
       {
         bt1 = *( buf + i     );
         bt2 = *( buf + i + 1 );

         if( bt1==255 && bt2==0xA0 )
           {
             bt1 = *( buf + i + 2 );
             bt2 = *( buf + i + 3 );
//         if( bt1==255 && (bt2==0xA4 || bt2==0xA2) )
         if( bt1==255 && (bt2==0xA4 || bt2==0xA2 || bt2 == 168) )
           {
             j++;
             if( j==bar )   {
                              nsh=1;
                              nsh_i=i;
                              break;
                            }
           }
           }

       }

    if( nsh==0 )  return -3;
//////////////////////////////////////////////////////////////////////////////



int soi=0,eoi=sizebuf;
    for(i=nsh_i; i>=0; i--)
       {
         bt1 = *( buf + i     );
         bt2 = *( buf + i + 1 );
//         if( bt1==255 && bt2==0xA0 )
         if( bt1==255 && bt2==0xA0 && *(buf+i+2)==255 )
           {
             soi=i;
             break;
           }
       }

    for(i=soi; i<sizebuf-1; i++)
       {
         bt1 = *( buf + i     );
         bt2 = *( buf + i + 1 );
/////////////////////////////////////////////////////////////////////
     if( bt1==255 && bt2==0xA2 ) {

                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3 );
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;
                                   i+=l;
                                   continue;
                                 }
     if( bt1==255 && bt2==0xA4 ) {

                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3 );
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;
                                   i+=l;
                                   continue;
                                 }
     if( bt1==255 && bt2==0xA5 ) {

                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3 );
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;
                                   i+=l;
                                   continue;
                                 }
     if( bt1==255 && bt2==0xA6 ) {

                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3);
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;
                                   i+=l;
                                   continue;
                                 }
     if( bt1==255 && bt2==0xA8 ) {

                                   bt1 = *( buf + i + 2 );
                                   bt2 = *( buf + i + 3 );
                                   n = bt1;  n = n<<8;
                                   l = n + bt2;
                                   i+=l;
                                   continue;
                                 }

/////////////////////////////////////////////////////////////////////
         if( bt1==255 && bt2==0xA1 )
           {
             eoi=i;
             break;
           }			  
       }

inpbuf+=soi;
sizebuf=eoi-soi+1;

int sof=0,dtt=0,dqt=0;
buf = inpbuf;
for(i=0; i<sizebuf-1; i++)
   {
     bt1 = *( buf + i     );
     if( bt1==0XFF )
       {
          bt2 = *( buf + i + 1 );

          if( bt2==0XA1 )  break;
//          if( bt2==0XA2 && sof==0 ) 
          if( bt2==0XA2 && sof==0 )  
            {
              sof=1;
              bt1 = *( buf + i + 2);
              bt2 = *( buf + i + 3);
              n = bt1;  n = n<<8;
              l = n + bt2;

              if( *( buf + i + 2 + l)==0XFF )
                {
                  bt1 = *( buf + i + 6 );
                  bt2 = *( buf + i + 7);
                  n = bt1;  n = n<<8;
                  l = n + bt2;
                  rows_wsq = l;
                  bt1 = *( buf + i + 8 );
                  bt2 = *( buf + i + 9);
                  n = bt1;  n = n<<8;
                  l = n + bt2;
                  cols_wsq = l;

if( image==NULL )
  {
    return -5;
  }
int type=0;
for(int level=0; level<number_levels; level++)
   {
    dx = cols_wsq>>level;
    dy = rows_wsq>>level;
    switch(level)
          {
           case  0:
                    _subband( 0, 0, dx, dy , vr_sgk*level, 0 ,0);
                    break;
           case  1:
                    for(n=0; n<4; n++)
                       {
                        switch(n)
                           {
                               case 0:  type=0;  break;
                               case 1:  type=2;  break;
                               case 2:  type=1;  break;
                               case 3:  type=3;  break;
                           }
                        _subband( w[0][n].x1, w[0][n].y1, w[0][n].x2, w[0][n].y2, vr_sgk*level, n*4 ,type);
                       }
                    break;
           case  2:
                    for(n=0; n<3; n++)
                       {

                        switch(n)
                           {
                               case 0:  type=0;  break;
                               case 1:  type=2;  break;
                               case 2:  type=1;  break;
                           }

                        _subband( w[1][n].x1, w[1][n].y1, w[1][n].x2, w[1][n].y2, vr_sgk*level, n*4 ,type);
                       }
                    break;
           case  3:
                    for(n=0; n<12; n++)
                       {

                        switch(n)
                           {
                               case  0:  type=0;  break;
                               case  1:  type=2;  break;
                               case  2:  type=1;  break;
                               case  3:  type=3;  break;

                               case  4:  type=0;  break;
                               case  5:  type=2;  break;
                               case  6:  type=1;  break;
                               case  7:  type=3;  break;

                               case  8:  type=0;  break;
                               case  9:  type=2;  break;
                               case 10:  type=1;  break;
                               case 11:  type=3;  break;
                           }

                        _subband( w[2][n].x1, w[2][n].y1, w[2][n].x2, w[2][n].y2, vr_sgk*level, n*4 ,type);
                       }
                    break;
           case  4:
                    _subband( w[3][ 0].x1, w[3][ 0].y1, w[3][ 0].x2, w[3][ 0].y2, vr_sgk*level,  0 ,0);
                    break;
           default:
                    break;
          }
   }

for(n=0; n<4; n++)
   {
     bb[n].x1=w[4][n].x1;
     bb[n].y1=w[4][n].y1;
     bb[n].x2=w[4][n].x2;
     bb[n].y2=w[4][n].y2;
   }

for(n=4; n<51; n++)
   {
     bb[n].x1=w[3][n-3].x1;
     bb[n].y1=w[3][n-3].y1;
     bb[n].x2=w[3][n-3].x2;
     bb[n].y2=w[3][n-3].y2;
   }

     bb[51].x1=w[1][3].x1;
     bb[51].y1=w[1][3].y1;
     bb[51].x2=w[1][3].x2;
     bb[51].y2=w[1][3].y2;

for(n=52; n<64; n++)
   {
     bb[n].x1=w[1][n-48].x1;
     bb[n].y1=w[1][n-48].y1;
     bb[n].x2=w[1][n-48].x2;
     bb[n].y2=w[1][n-48].y2;
   }


                  Em = *( buf + i + 10);
                  bt1 = *( buf + i + 11);
                  bt2 = *( buf + i + 12);
                  n = bt1;  n = n<<8;
                  l = n + bt2;
                  MMM = (float)l;
                  for(n=0; n<Em; n++)
                      MMM *= (float)0.10;

                  Er = *( buf + i + 13);
                  bt1 = *( buf + i + 14);
                  bt2 = *( buf + i + 15);
                  n = bt1;  n = n<<8;
                  l = n + bt2;
                  RRR = (float)l;
                  for(n=0; n<Er; n++)
                      RRR *= (float)0.10;
/////////////////////////////////////////////
                  Ev=*( buf + i + 16);
                  bt1 = *( buf + i + 17);
                  bt2 = *( buf + i + 18);
                  n = bt1;  n = n<<8;
                  l = n + bt2;
                  Sf = l;
/////////////////////////////////////////////
                  i+=15;
                }
            }
          if( bt2==0XA4 && dtt==0)
            {
              dtt=1;
              bt1 = *( buf + i + 2);
              bt2 = *( buf + i + 3);
              n = bt1;  n = n<<8;
              l = n + bt2;

              if( *( buf + i + 2 + l )==0XFF )
                {
                  L0 = *( buf + i + 4 );
                  L1 = *( buf + i + 5 );
                  if( (L0%2)==0 && (L1%2)==0 ) flt.filters_type=0;
                     else if( (L0%2)!=0 && (L1%2)!=0 )
                            {
                               if( L0==9 && L1==7 )  flt.filters_type=2;
                                               else  flt.filters_type=1;
                            }
                     else return -4;
                  if( (L0%2)==0 )  last = L0 >> 1;
                             else  last = ( L0 + 1 ) >> 1;

                  i+=6;
                  for(j=0; j<last; j++)
                     {

                       Sign = *(buf + i);   i++;
                       Ex = *(buf + i);     i++;
                       bt1 = *(buf + i);    i++;
                       bt2 = *(buf + i);    i++;
                       bt3 = *(buf + i);    i++;
                       bt4 = *(buf + i);    i++;

                       n = bt1;  n = n<<24;
                       m = bt2;  m = m<<16;
                       k = bt3;  k = k<<8;
                       l = n + m + k + bt4;
                       lof[j] = (float)l;
                       if( Sign!=0 )  lof[j] = -lof[j];
                       for(k=0; k<Ex; k++)
                           lof[j] *= (float)0.10;
                     }
                  flt.length_lo=last;
                  if( (L1%2)==0 )  last = L1 >> 1;
                             else  last = (L1+1) >> 1;
                  for(j=0; j<last; j++)
                     {
                        Sign = *(buf + i);  i++;
                        Ex = *(buf + i);    i++;
                        bt1 = *(buf + i);   i++;
                        bt2 = *(buf + i);   i++;
                        bt3 = *(buf + i);   i++;
                        bt4 = *(buf + i);   i++;

                        n = bt1;  n = n<<24;
                        m = bt2;  m = m<<16;
                        k = bt3;  k = k<<8;
                        l = n + m + k + bt4;
                        hif[j] = (float)l;
                        if( Sign!=0 )  hif[j] = -hif[j];
                        for(n=0; n<Ex; n++)
                            hif[j] *= (float)0.10;
                     }
                  flt.length_hi=last;
// ________________________________________________________________________________

//if( flt.filters_type==1 )  
if( flt.filters_type!=0 )  
  {
#ifdef SOURCE_FBI
   int nf=1,jf;
   for(jf=0; jf<flt.length_lo; jf++)  
      {
        hif_inv[jf] = nf*lof[jf];
        flt.hi_inv[jf]=hif_inv[jf];
        nf*=-1;
      }

   nf=1;
   for(jf=0; jf<flt.length_hi; jf++)  
      {
        lof_inv[jf] = nf*hif[jf];
        flt.lo_inv[jf]=lof_inv[jf];
        nf*=-1;
      }
/*
   hif_inv[0] = lof[0];  //  
   hif_inv[1] =-lof[1];  //
   hif_inv[2] = lof[2];  //
   hif_inv[3] =-lof[3];  //
   hif_inv[4] = lof[4];  //

   lof_inv[0] = hif[0];  //  
   lof_inv[1] =-hif[1];  //
   lof_inv[2] = hif[2];  //
   lof_inv[3] =-hif[3];  //
*/
#endif
#ifdef SOURCE_LA
//   hif_inv[0] =-lof[0];  //  
//   hif_inv[1] = lof[1];  //
//   hif_inv[2] =-lof[2];  //
//   hif_inv[3] = lof[3];  //
//   hif_inv[4] =-lof[4];  //
//
//   lof_inv[0] = -hif[0];  //  
//   lof_inv[1] =  hif[1];  //
//   lof_inv[2] = -hif[2];  //
//   lof_inv[3] =  hif[3];  //
#endif
   lof_inv0=lof_inv[0];  //lof_inv[0] = hif[0];
   lof_inv1=lof_inv[1];  //lof_inv[1] =-hif[1];
   lof_inv2=lof_inv[2];  //lof_inv[2] = hif[2];
   lof_inv3=lof_inv[3];  //lof_inv[3] =-hif[3];

   hif_inv0=hif_inv[0];  //hif_inv[0] = lof[0];
   hif_inv1=hif_inv[1];  //hif_inv[1] =-lof[1];
   hif_inv2=hif_inv[2];  //hif_inv[2] = lof[2];
   hif_inv3=hif_inv[3];  //hif_inv[3] =-lof[3];
   hif_inv4=hif_inv[4];  //hif_inv[4] = lof[4];
  }
else                       // HS-type;
  {
   int nf=1,jf;
   for(jf=0; jf<flt.length_lo; jf++)  
      {
        hif_inv[jf] = nf*lof[jf];
        flt.hi_inv[jf]=hif_inv[jf];
        nf*=-1;
      }

   nf=-1;
   for(jf=0; jf<flt.length_hi; jf++)  
      {
        lof_inv[jf] = nf*hif[jf];
        flt.lo_inv[jf]=lof_inv[jf];
        nf*=-1;
      }
  }

// ________________________________________________________________________________
                  i--;
                }
            }

          if( bt2==0XA5 && dqt==0)  
            {
              dqt=1;
              bt1 = *( buf + i + 2);
              bt2 = *( buf + i + 3);
              n = bt1;  n = n<<8;
              l = n + bt2;
              if( *( buf + i + 2 + l)==0XFF )
                {
                  i+=4;
                  Ec = *( buf + i );   i++;
                  bt1 = *( buf + i );  i++;
                  bt2 = *( buf + i );  i++;
                  n = bt1;  n = n<<8;
                  l = n + bt2;
                  c_coeff = (float)l;
                  for(n=0; n<Ec; n++)
                      c_coeff *= (float)0.10;

                  for(j=0; j<64; j++)
                     {
                       Eq = *(buf + i);     i++;
                       bt1 = *(buf + i);    i++;
                       bt2 = *(buf + i);    i++;
                       n = bt1;  n = n<<8;
                       l = n + bt2;
                       bb[j].bin_width = (float)l;
                       for(n=0; n<Eq; n++)
                           bb[j].bin_width *= (float)0.10;

                       Ez = *(buf + i);      i++;
                       bt3 = *(buf + i);     i++;
                       bt4 = *(buf + i);     i++;
                       n = bt3;  n = n<<8;
                       l = n + bt4;
                       bb[j].zero_bin_width = (float)l;
                       for(n=0; n<Ez; n++)
                           bb[j].zero_bin_width *= (float)0.10;
                     }
                  i--;
                }

            }
       }

   }


buf = inpbuf;
   buf_float = new float[rows_wsq*cols_wsq+40];
   if(buf_float==NULL)
      return -1;
memset(buf_float,0,(rows_wsq*cols_wsq+40)*4);

byte *data;
int *pr;
int n_h_t=0,u;

////////////////////////
//hhh = fopen("fbi__","w");    //global_o++;
////////////////////////

int s;
number_group = 0;
     for(i=0; i<sizebuf; i++)
        {
          bt1 = *( buf + i     );
          if( bt1==0XFF )
            {
              bt2 = *( buf + i + 1 );

              if( bt2==0XA1 && i==(sizebuf-2))  break;
              if( bt2==0XA2 )  
                {

                   bt1 = *( buf + i + 2);
                   bt2 = *( buf + i + 3);
                   n = bt1;  n = n<<8;
                   l = n + bt2;
                   if( *( buf + i + 2 + l)==0XFF )
                     {
                        i += (1+l);
                        continue;
                     }
                }
              bt2 = *( buf + i + 1 );
              if( bt2==0XA4 )  
                {
                   bt1 = *( buf + i + 2);
                   bt2 = *( buf + i + 3);
                   n = bt1;  n = n<<8;
                   l = n + bt2;
                   if( *( buf + i + 2 + l)==0XFF )
                     {
                        i += (1+l);
                        continue;
                     }
                }
              if( bt2==0XA5 )  
                {
                   bt1 = *( buf + i + 2);
                   bt2 = *( buf + i + 3);
                   n = bt1;  n = n<<8;
                   l = n + bt2;
                   if( *( buf + i + 2 + l)==0XFF )
                     {
                        i += (1+l);
                        continue;
                     }
                }
              if( bt2==0XA6 )  
                {
                   bt1 = *( buf + i + 2);
                   bt2 = *( buf + i + 3);
                   n = bt1;  n = n<<8;
                   l = n + bt2;
                   if( *( buf + i + 2 + l)==0XFF )
                     {
                        j = i+4;
                int ndht=0;
                do{
                        //int ththth=*(buf+j);
                        j++; // for Th;
                        LASTK[n_h_t] = 0;
                        for(kk=1; kk<17; kk++)
                           {
                             bt1 = *(buf+j);   j++;
                             BITS[n_h_t].L[kk] = bt1;
                             LASTK[n_h_t] += bt1;
                           }
                        pr = symbol_HUFFVAL[n_h_t];
                        for(u=0; u<LASTK[n_h_t]; u++)
                           {
                             bt1 = *(buf+j);     j++;
                             *(pr+u) = bt1;  
                           }

                     k=build_huffman_tables( n_h_t );
                     if( k!=0 ) return k;
                     n_h_t++;
                     ndht++;
                  }while(j< (i + 2 + (int)l));
          if( ndht==2 )
            {
              for(kk=1; kk<17; kk++)
                  BITS[n_h_t].L[kk] = BITS[1].L[kk];
              LASTK[n_h_t] = LASTK[1];
              pr = symbol_HUFFVAL[n_h_t];
              for(u=0; u<LASTK[n_h_t]; u++)
                  *(pr+u) = *(symbol_HUFFVAL[1]+u);  
              k=build_huffman_tables( n_h_t );
              if( k!=0 ) return k;
              ndht++;
              n_h_t++;
            }
                        i += (1+l);
                        continue;
                     }
                }
            }


              bt2 = *( buf + i + 1 );
              if( bt1==0XFF && bt2==0XA3 )  {
                                               
                                               s=0;
                                               for(j=group[number_group].begin; j<=group[number_group].end; j++)
                                                  {
                                                    s+=((bb[j].x2-bb[j].x1)*(bb[j].y2-bb[j].y1));
                                                  }

                                               size_int_huff[number_group] = s;
                                               int_huff[number_group] = new int[s+40];
                                               if(int_huff[number_group]==NULL)  return -1;

              j = i+5;
              last = 0;

              for(kk=j; kk<sizebuf; kk++)
                 {
                    bt1 = *( buf + kk     );
                    bt2 = *( buf + kk + 1 );

                    if( bt1==0XFF && bt2==0XA1 && kk==(sizebuf-1)) break;
                    if( bt1==0XFF && bt2==0XA3 ) break;
                    if( bt1==0XFF && bt2==0XA6 ) break;
                    if( bt1==0XFF && bt2==0 )
                      {
                        last++; kk++;
                        continue;
                      }
                    last++;
                 }
              number_bytes[number_group] = last;
              c_data[number_group] = new byte[last+40];
              if( c_data[number_group]==NULL ) return -1;

              data = c_data[number_group];

              last=0;
              for(kk=j; kk<sizebuf; kk++)
                 {
                    bt1 = *( buf + kk     );
                    bt2 = *( buf + kk + 1 );

                    if( bt1==0XFF && bt2==0XA1 && kk==(sizebuf-1)) break;
                    if( bt1==0XFF && bt2==0XA3 ) break;
                    if( bt1==0XFF && bt2==0XA6 ) break;
                    if( bt1==0XFF && bt2==0 )
                      {
                        *data = bt1;
                        data++;
                        /////
                        last++;
                        /////
                        kk++;
                        continue;
                      }
                    *data = bt1;
                    data++;
                    /////
                    last++;
                    /////
                 }
                                               number_group++;
                                            }
              if( bt1==0XFF && bt2==0XA1 && kk==(sizebuf-2))  break;          
        }


////////////////////////
//fclose(hhh);
//exit(0);
////////////////////////

if( n_h_t==2 )
  {
    for(kk=1; kk<17; kk++)
       {
         BITS[n_h_t].L[kk] = BITS[1].L[kk];
       }
    LASTK[n_h_t] = LASTK[1];
    pr = symbol_HUFFVAL[n_h_t];
    for(u=0; u<LASTK[n_h_t]; u++)
       {
         *(pr+u) = *(symbol_HUFFVAL[1]+u);  
       }
                     k=build_huffman_tables( n_h_t );
    n_h_t++;

  }

for(i=0; i<number_group; i++)
//for(i=0; i<2; i++)
   {
 	  if( number_bytes[i]==0 ) continue;
      if( version_sgk<2 )   dx = huffman_decoding( i );
                     else   dx = huffman_decoding_compliance( i );
      if( dx!=0 ) return dx;
   }

#ifdef NODEQUANTIZATION
  float *ffll=buf_float;
  byte *img_;
  img_=image;

int *od;
int xd1,xd2,yd1,yd2;
float *fd0,*fd;
for(i=0; i<number_group; i++)
   {
     od=int_huff[i];
     for(j=group[i].begin; j<=group[i].end; j++)
        {
          xd1=bb[j].x1;    xd2=bb[j].x2;
          yd1=bb[j].y1;    yd2=bb[j].y2;
          fd0=buf_float+(yd1-1)*cols_wsq + xd1;
          if( bb[j].bin_width==0. )
            {
              for(mm=yd1; mm<yd2; mm++)
                 {
                   fd0+=cols_wsq;    fd = fd0;
                   for(nn=xd1; nn<xd2; nn++)
                      {
                        *fd=0.0;   fd++;
                      }
                 }
            }
          else
            {
              for(mm=yd1; mm<yd2; mm++)
                 {
                   fd0+=cols_wsq;    fd = fd0;
                   for(nn=xd1; nn<xd2; nn++)
                      {
                        if( *od==0 ) *fd=0.0;
                                else *fd=abs(*od)*1;
                        if( *fd>0 )  *fd=255;
                        od++;   fd++;
                      }
             }
            }
        }
   }
  for(i=0; i<rows_wsq; i++)
  for(j=0; j<cols_wsq; j++)
     {
       *img_=abs(*ffll); img_++; ffll++;
     }
RRR=1.;
MMM=0.;     
return 0;
#endif


int *o;
int x1,x2,y1,y2;
float qk,cqzkd,*f0,*f;

for(i=0; i<number_group; i++)
   {
     o=int_huff[i];
     for(j=group[i].begin; j<=group[i].end; j++)
        {
          x1=bb[j].x1;    x2=bb[j].x2;
          y1=bb[j].y1;    y2=bb[j].y2;
          qk=bb[j].bin_width;
          f0=buf_float+(y1-1)*cols_wsq + x1;



          if( bb[j].bin_width==0. )
            {
              for(mm=y1; mm<y2; mm++)
                 {
                   f0+=cols_wsq;    f = f0;
                   for(nn=x1; nn<x2; nn++)
                      {
                        *f=0.0;   f++;
                      }
                 }
            }
          else
            {
              cqzkd=c_coeff*qk-bb[j].zero_bin_width*(float)0.5;
              for(mm=y1; mm<y2; mm++)
                 {
                   f0+=cols_wsq;    f = f0;
                   for(nn=x1; nn<x2; nn++)
                      {
                        if( *o==0 ) *f=0.0;
                             else {
                                     if( *o>0 )  *f=qk*(*o)-cqzkd;
                                           else  *f=qk*(*o)+cqzkd;
                                  }
                        o++;   f++;
                      }
                 }
            }

        }
   }
//delete [] quant_table;



float *b0,*b1;
float *x;

if(cols_wsq>rows_wsq)
  {
   x = new float[cols_wsq+40];
   if (!x)  {
                return -1;
            }
   b0 = new float[cols_wsq+40];
   if (!b0) {
                delete [] x;  x=NULL;
                return -1;
            }
   b1 = new float[cols_wsq+40];
   if (!b1) {
                delete [] x;  x=NULL;
                delete [] b0;  b0=NULL;
                return -1;
            }
  }
else
  {
   x = new float[rows_wsq+40];
   if (!x)  {
                return -1;
            }
   b0 = new float[(rows_wsq+40)];
   if (!b0) {
                delete [] x;  x=NULL;
                return -1;
            }
   b1 = new float[rows_wsq+40];
   if (!b1) {
                delete [] x;  x=NULL;
                delete [] b0;  b0=NULL;
                return -1;
            }
  }


#ifndef SUBBANDS_FIELD

j=0;
int type=0;
for(int level=number_levels-1; level>=0; level--)
   {
    dx=cols_wsq>>level; dy=rows_wsq>>level;
    switch(level)
          {
           case  0:
                    j = build_from_subband(buf_float, 0, 0, dx, dy, level, 0
                                           ,x,b0,b1,&flt);
                    if( j!=0 ) return j;
                    break;
           case  1:
                    for(i=0; i<4; i++)
                       {
                        switch(i)
                          {
                            case  0:  type=0;  break;
                            case  1:  type=2;  break;
                            case  2:  type=1;  break;
                            case  3:  type=3;  break;
                          }
                        j=rotate(buf_float,w[0][i].x1, w[0][i].y1, w[0][i].x2, w[0][i].y2,vr_sgk*level,i*4,type);
                        if( j!=0 ) return j;
                        j = build_from_subband(buf_float, w[0][i].x1, w[0][i].y1, w[0][i].x2, w[0][i].y2, level, i*4
                                               ,x,b0,b1,&flt);
                        if( j!=0 ) return j;
                       }
                    break;
           case  2:
                    for(i=0; i<3; i++)
                       {
                        switch(i)
                          {
                            case  0:  type=0;  break;
                            case  1:  type=2;  break;
                            case  2:  type=1;  break;
                          }
                        j=rotate(buf_float, w[1][i].x1, w[1][i].y1, w[1][i].x2, w[1][i].y2,vr_sgk*level,i*4,type);
                        if( j!=0 ) return j;
                        j = build_from_subband(buf_float, w[1][i].x1, w[1][i].y1, w[1][i].x2, w[1][i].y2, level, i*4
                                               ,x,b0,b1,&flt);
                        if( j!=0 ) return j;
                       }
                    break;
           case  3:
                    for(i=0; i<12; i++)
                       {
                        switch(i)
                          {
                            case  0:  type=0;  break;
                            case  1:  type=2;  break;
                            case  2:  type=1;  break;
                            case  3:  type=3;  break;
                            case  4:  type=0;  break;
                            case  5:  type=2;  break;
                            case  6:  type=1;  break;
                            case  7:  type=3;  break;
                            case  8:  type=0;  break;
                            case  9:  type=2;  break;
                            case 10:  type=1;  break;
                            case 11:  type=3;  break;
                          }
                        j=rotate(buf_float, w[2][i].x1, w[2][i].y1, w[2][i].x2, w[2][i].y2,vr_sgk*level,i*4,type);

//if( i==4 )
//  {
//    for(int jh=w[2][i].y1; jh<w[2][i].y2; jh++)
//    for(int ih=w[2][i].x1; ih<w[2][i].x2; ih++)
//        *(buf_float+jh*cols_wsq+ih)=255.;
//  }


                        if( j!=0 ) return j;
                        j = build_from_subband(buf_float, w[2][i].x1, w[2][i].y1, w[2][i].x2, w[2][i].y2, level, i*4
                                               ,x,b0,b1,&flt);
                        if( j!=0 ) return j;
                       }
                    break;
           case  4:

//i=15;
//for(int jh=w[3][i].y1; jh<w[3][i].y2; jh++)
//for(int ih=w[3][i].x1; ih<w[3][i].x2; ih++)
//    *(buf_float+jh*cols_wsq+ih)=255.;

                    j = build_from_subband(buf_float, w[3][0].x1, w[3][0].y1, w[3][0].x2, w[3][0].y2, level, 0
                                           ,x,b0,b1,&flt);
                    if( j!=0 ) return j;
                    break;
           default:
                    break;
          }
   }

#endif

float *fl;
byte *img;
float fff;
#ifndef RELEASE
  float rmse=0.0;
  int sk;
  int k1;

  fl=buf_float;
  img=image;
  for(i=0; i<rows_wsq; i++)
  for(j=0; j<cols_wsq; j++)
     {
//       fff = *(buf_float+i*cols_wsq+j)*RRR+MMM;
       fff = *fl*RRR+MMM+0.5;
       k1 = (int)fff;
       if(k1<0) k1 = 0;
       if(k1>255) k1 = 255;
//       sk =*( image+i*cols_wsq+j);
       sk =*img;  img++;
       nn = k1-sk;
       nn = nn*nn;
       rmse += nn;
       fl++;
     }
  rmse = rmse/(cols_wsq*rows_wsq);
  rmse = sqrt(rmse);

  if(rmse==0.0) rmse=1.0;
  psnr = 20*log10(255/rmse);
  j=0;
  for(i=0; i<8; i++)
//     j+=size_b_image[i];
     j+=number_bytes[i];
  compression = (rows_wsq*cols_wsq);
  compression /= (float)j;
  compression = compression;
///////////////////////////////////////    psnr = 32.111461639;
#endif

  fl=buf_float;
  img=image;
  MMM=MMM+(float)0.5;
  for(i=0; i<(rows_wsq*cols_wsq); i++)
     {
      fff=*fl*RRR+MMM; fl++;
      if(fff<0.)     fff=0.;
      if(fff>255.)   fff=255.;

      *img=(byte)fff; img++;
     }



/*
  for(i=0; i<rows_wsq; i++)
  for(j=0; j<cols_wsq; j++)
     {
//        iiiii=i;
//        jjjjj=j;
//      fff=*(buf_float+i*cols_wsq+j)*RRR+MMM;

      fff=*fl*RRR+MMM+0.5; fl++;
//      fff=*fl*RRR+MMM; fl++;

      nn=(int)fff;
#ifndef SUBBANDS_FIELD
#ifdef DIFFER
//      nn-=image[i][j];
      nn-=img;
#endif
#endif
      if(nn<0) {
                 nn=0;
               }
      if(nn>255) {
                   nn=255;
                 }
#ifndef SUBBANDS_FIELD
#ifdef DIFFER
      if(nn>0) nn=(255*nn)/((int)rmse+1);
#endif
#endif
//      *(image+i*cols_wsq+j)=(byte)nn;
      *img=(byte)nn; img++;
//      image[i][j]=255;
     }
*/
  for(i=0; i<number_group; i++)
     {
       delete [] int_huff[i];  int_huff[i]=NULL;
       delete [] HUFFSIZE[i];  HUFFSIZE[i]=NULL;
       delete [] HUFFCODE[i];  HUFFCODE[i]=NULL;
       delete []   EHUFCO[i];    EHUFCO[i]=NULL;
       delete []   EHUFSI[i];    EHUFSI[i]=NULL;
       delete []   c_data[i];    c_data[i]=NULL;
     }
delete [] buf_float;  buf_float=NULL;
delete [] x;                  x=NULL;
delete [] b0;                b0=NULL;
delete [] b1;                b1=NULL;


//fclose(hhh);
//////////////////////////////////////////////////////////////////////////////
#ifdef EXAM
fclose(hh);
#endif
//////////////////////////////////////////////////////////////////////////////
//return 0;
return Sf;
}
int SWSQ::jump_along_buf( byte *buf,  int i)
{
  byte bt1,bt2;
  int n;
     bt1 = *( buf + i + 2 );
     bt2 = *( buf + i + 3);
     n = (int)bt1;  n = n<<8;
     i += (n + (int)bt2);
     return i;
}

int SWSQ::wsqCheckSum( byte *src,  int lensrc)
{
   int sum=0;
   for(int i=0; i<lensrc; i+=10)
       sum+=(*(src+i)+(i<<1));
   return sum;
}
//extern "C" __declspec(dllexport) int wsq_encode(byte *src, byte *outbuf,
//                                               int Maxx, int Maxy, float press)
/*
extern "C" __export int wsq_encode(byte *src, byte *outbuf,
                                               int Maxx, int Maxy, float press, int chksum)
*/

/*
extern "C" __export int cwsq_decode(byte *outbuf,byte *inpbuf,
                   int *rows,int *cols,int er_Y,int er_Cb,int er_Cr, int chksum)
*/



int wsq_encode(byte *src,byte *outbuf,int Maxx,int Maxy,float press,int chksum, 
               WORD impNumber)
{
    if( Maxx<128 || Maxy<128 ) return ERROR_SIZE;

    SWSQ *WsqObj = NULL;
    WsqObj = new SWSQ;
    if( !WsqObj ) return ERROR_MEMORY;

	try{
           int ret;
           if ( WsqObj->wsqCheckSum(src,(Maxx*Maxy)) != chksum )  return ERROR_CHECK_SUM;
           ret=WsqObj->encoding(src,outbuf,Maxy,Maxx,press, impNumber);
           delete [] WsqObj;   WsqObj = NULL;


           return ret;
       }
	catch(...)
	{
           if( WsqObj != NULL) delete [] WsqObj;
		   WsqObj = NULL;
		   return ERROR_EXCEPTION;

	}


}

int wsq_decode(byte *imagedst,byte *src,int lensrc,int *Maxx,int *Maxy)
{

    SWSQ *WsqObj = NULL;
    WsqObj = new SWSQ;
    if( !WsqObj ) return ERROR_MEMORY;

	try{
           int k;
           byte *image=imagedst,*inpbuf=src;
           k=WsqObj->decoding(NULL, inpbuf, lensrc);
           *Maxx=WsqObj->cols_wsq;
           *Maxy=WsqObj->rows_wsq;

           //if ( WsqObj->wsqCheckSum(src,lensrc) != chksum )  return ERROR_CHECK_SUM;

           k=WsqObj->decoding(image, inpbuf, lensrc);

           delete WsqObj;   WsqObj = NULL;
           return k;
	   }
	catch(...)
	{
           if( WsqObj != NULL) delete WsqObj;   
		   WsqObj = NULL;
		   return ERROR_EXCEPTION;
          
	}
}




