#ifndef WSQ_H_
#define WSQ_H_

//#ifdef SWSQ_EXPORTS
//   #define WSQ_EI __declspec(dllexport)
//#else  
//   #define WSQ_EI __declspec(dllimport) 
//#endif
#define WSQ_EI 

#define  ERROR_MEMORY               -1    // memory error
#define  ERROR_PROCESSING           -2    // processing error 
#define  ERROR_FILE_CONTENT         -3    // wrong content of WSQ file
#define  ERROR_FILTERS              -4    // error in WSQ filters
#define  EMPTY_SOURCE	            -5    // pointer to source buffer is NULL
#define  ERROR_SIZE                 -6    // one of source image size is less than 128
#define  ERROR_EXCEPTION            -7    // unknown exception raised
#define  ERROR_BITRATE		         -8    // wrong bitrate parameter
#define  ERROR_CHECK_SUM          -666    // control sum error

typedef int (*type_encode)(unsigned char *idata, unsigned char *odata, int height, int width, float r_bitrate, int);
typedef int (*type_decode)(unsigned char *odata, unsigned char *idata, int ilen, int *, int *, int);

extern "C" {
int WSQ_EI wsq_encode(unsigned char *idata, unsigned char *odata, int width, int height, float r_bitrate, int chksum);
int WSQ_EI wsq_decode (unsigned char *imageDst, unsigned char *src,	int lensrc, int *maxX, int *maxY );
};
#endif
