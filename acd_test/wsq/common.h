/*****************************************************************************
	common.h - the common header file

******************************************************************************
	
	Copyright (C) 2009 Sonda Technologies Ltd.
	
	Version 1.0.0 of 27.03.2009
	    
*******************************************************************************/
 
#ifndef COMMON_H__
#define COMMON_H__

#include "win2lin.h"	

#ifndef __cplusplus
   typedef char      bool;
   #define true          1
   #define false         0 
#endif

// make compatible with UNIX and ARM platforms
#if defined(_WINDOWS)
   
#else
#endif

#define _DBG_INFO
#define _DBG_INFO_1
#define _DBG_INFO_2

#ifdef __cplusplus
   extern "C"{
#endif
/******************************************************************
          THE CONSTANRS
******************************************************************/
// Finger quality values
enum NIST_QUALITY
{
   QUAL_POOR      =  20,    // NFIQ value 5
   QUAL_FAIR      =  40,    // NFIQ value 4
   QUAL_GOOD      =  60,    // NFIQ value 3
   QUAL_VGOOD     =  80,    // NFIQ value 2
   QUAL_EXCELLENT = 100     // NFIQ value 1
};

// Impression type codes
enum IMPRESSION_TYPE
{
   IMPTYPE_LP  = 0x00,       // Live-scan plain
   IMPTYPE_LR  = 0x01,       // Live-scan rolled
   IMPTYPE_NP  = 0x02,       // Nonlive-scan plain
   IMPTYPE_NR  = 0x03,       // Nonlive-scan rolled
   IMPTYPE_L_I = 0x04,       // Latent impression
   IMPTYPE_L_T = 0x05,       // Latent tracing
   IMPTYPE_L_P = 0x06,       // Latent photo
   IMPTYPE_L_L = 0x07,       // Latent lift
   IMPTYPE_SW  = 0x08,       // swipe
   IMPTYPE_LCL = 0x09        // Live-scan contactless
};

// Finger position codes
enum FINGERS
{
   FINGPOS_UK     =  0,       // Unknown finger
   FINGPOS_RT     =  1,       // Right thumb
   FINGPOS_RI     =  2,       // Right index finger
   FINGPOS_RM     =  3,       // Right middle finger
   FINGPOS_RR     =  4,       // Right ring finger
   FINGPOS_RL     =  5,       // Right little finger
   FINGPOS_LT     =  6,       // Left thumb
   FINGPOS_LI     =  7,       // Left index finger
   FINGPOS_LM     =  8,       // Left middle finger
   FINGPOS_LR     =  9,       // Left ring finger
   FINGPOS_LL     = 10,       // Left little finger

   FINGPOS_P_RT   = 11,       // Plain right thumb
   FINGPOS_P_LT   = 12,       // Plain left thumb
   FINGPOS_P_RS   = 13,       // Plain right slap (right four fingers)
   FINGPOS_P_LS   = 14        // Plain left  slap (left  four fingers)
};

#define MAX_WIDTH              (2 * 1024)   // maximum image width
#define MAX_HEIGHT             (2 * 1024)	  // maximum image height	

#pragma pack (push, 1)

// structure keeps information about RAW image
struct P_PACKED_1 RawImage
{
   WORD width;               // size of Scanned Image in x direction, pixels        
   WORD height;              // size of Scanned Image in y direction, pixels
   BYTE* image;              // pointer to RAW image. The raw image should be uncompressed, upright, 
                             // 8 bit per pixel, 500 DPI.       
   BYTE finger;              // fnger Position, 0...10 refer to ANSI/NIST standard 
   BYTE impression_type;

#ifdef __cplusplus
   RawImage()
   {
      width             = MAX_WIDTH;
      height            = MAX_HEIGHT;
      image             = 0;
      finger            = FINGPOS_UK;
      impression_type   = IMPTYPE_LP;
   }
#endif
};

// structure keeps information about DIB image
struct P_PACKED_1 DIBImage
{
	BYTE* image;              // pointer to DIB image. The image should be uncompressed, upright, 
	// 8 bit per pixel, 500 DPI.       
	BYTE finger;              // finger Position, 0...10 refer to ANSI/NIST standard 
	BYTE impression_type;

#ifdef __cplusplus
   DIBImage()
	{
		image             = 0;
		finger            = FINGPOS_UK;
		impression_type   = IMPTYPE_LP;
	}
#endif
};


#pragma pack (pop)

#ifdef __cplusplus
   } // extern "C"{
#endif

#endif // COMMON_H__



