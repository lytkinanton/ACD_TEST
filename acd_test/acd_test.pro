#-------------------------------------------------
#
# Project created by QtCreator 2013-06-11T18:10:15
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = acd_test
TEMPLATE = app


SOURCES += main.cpp\
        mw.cpp \
    acd5service.cpp \
    wsq/wsq.cpp

HEADERS  += mw.h \
    StatTimer.h \
    Host.h \
    acd5service.h \
    acd_protocol_v2.h \
    wsq/common.h \
    wsq/swsq.h \
    wsq/win2lin.h

FORMS    += mw.ui

INCLUDEPATH += /usr/include/qt4/QtOpenGL/
