#ifndef ass_H
#define ass_H

#include <QTimer>
#include <QRect>
#include <QObject>
#include <QtNetwork>
#include <QObject>
#include <QThread>

#include <QTcpSocket>
#include <acd_protocol_v2.h>
#include <pthread.h>

#define BIOS "BIOS"
#define ACD5 "ACD5"
#define ACD7 "ACD7"
#define IPADDR "IP_ADDRESS"
#define PORT "PORT"
#define BUID "BIOS_ID"
#define FIND_STR "FIND_STR"
#define TYPE "TYPE"
#define LED1 "STANDBY_LED"
#define LED2 "PROCESS_LED"
#define LED3 "DENIAL_LED"
#define LED4 "ACCESS_LED"
#define SOUND1 "SUCCESS_SOUND_LEN"
#define SOUND2 "FAILURE_SOUND_LEN"
#define RESDEL "RESULT_DELAY"
#define FP_CL "FP_CONTRLIMIT"
#define FP_CD "FP_CONTRDELTA"
#define EMULATE_BIOS "EMULATE_BIOS"
#define BIOS_SAVE_DUMP "SAVE_DUMP_IMAGE"
#define LOG_PATH "LOG_PATH"
#define FLIP_H "FLIP_H"
#define FLIP_V "FLIP_V"
#define IMAGE_INVERSE "INVERT_IMAGE"
#define NEED_LOG "LOG"
#define DUMP_BADS "DUMP_BADS"
#define ROI_X "ROI_X"
#define ROI_Y "ROI_Y"
#define ROI_W "ROI_W"
#define ROI_H "ROI_H"

typedef struct _acd5_obj {
    char ip[32];
    quint32 port;
    quint8 standbyLed;
    quint8 processLed;
    quint8 denialLed;
    quint8 accessLed;
    quint8 successsnd;
    quint8 failuresnd;
    quint32 resultdelay;
    quint32 cl;
    quint32 cd;
    quint8 flip_h;
    quint8 flip_v;
    QRect roi;
    quint8 inverse_image;
    char reserv[32];
    bool dump_bad;
    bool need_log;
    volatile quint32 jpeg_len;
    volatile int active, has_finger;

} acd5_obj;




class ass : public QThread
{
    Q_OBJECT
public:
    explicit ass(QObject *parent = 0);
    ~ass();
    bool start(QString ip, int port);
    void init_sockets();
    bool writeData(QByteArray data);
    void init_cmd(exchangePack * cmd);
    bool check_packet(exchangePack cmd) ;
    QTcpSocket *socket;

    QTimer *alive_timer;
    QTimer off_timer;
    void run();
    quint8 evt_ctrl;

    quint8 get_image(quint8 format);
    void led_control(int num);
    void s_control(int num);
    void wieg_control(quint8 num);
    void turnstile_control(int num);
    void disconnect_network();
    void custom_cmd(quint8 cmd, quint32 p1, quint32 p2, quint8 flag);

signals:
    void error(QTcpSocket::SocketError socketerror);
    void image_ready(quint8 *data, quint32 w, quint32 h, quint32 size);
    void evt_image();

private slots:

    void timer_rt();
    void offtimer_rt();
    void disconnected();
    void connected();
    void readyRead();

protected:
    void incomingConnection(qintptr socketDescriptor);
    quint8 requested_image_format;

private:
    bool connection_active;
    bool data_stream;
    bool has_finger;
    QString host_ip;
    quint32 host_port;
    bool active;
    bool exec_cmd(exchangePack * cmd);


};

#endif // ass_H
