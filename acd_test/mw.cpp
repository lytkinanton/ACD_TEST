//#include <sys/socket.h>
//#include <arpa/inet.h>
#include <QMessageBox>
#include <acd5service.h>
#include "mw.h"
#include "ui_mw.h"
#include <wsq/swsq.h>


mw::mw(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::mw)
{
    ui->setupUi(this);
    ah = NULL;
    ui->ip_eb->setInputMask("000.000.000.000;");

    led_switcher = new QTimer();
    start_tm = new QTimer();

    connect(start_tm, SIGNAL(timeout()), this, SLOT(starting()));

    connect(led_switcher, SIGNAL(timeout()), this, SLOT(switch_leds()));
    led_switcher->setSingleShot(true);
    current_image_format = 3;
    state = 0;
}

mw::~mw()
{
    ah->terminate();

    delete ah;
    delete ui;
    delete start_tm;
}

bool mw::close()
{
    ah->terminate();
    delete ah;
    return false;
}

void mw::on_connect_bt_clicked()
{
    if (!state)
    {
        state = 1;
        ui->connect_bt->setText("Disconnect");
        ui->memo->append(QString("Connecting to %1:%2").arg(ui->ip_eb->text()).arg(ui->port_eb->text().toInt()));

        if (ah != NULL) {
            delete ah;
            ah = NULL;
        }

        ah = new ass();

        if (!ah->start(ui->ip_eb->text(), ui->port_eb->text().toInt()))
        {
            QMessageBox::warning(this, "No connection", "Cannot connect to server!", QMessageBox::Ok);
            return;
        } else
            ui->memo->append("Connected!");

        ui->memo->repaint();

        connect(ah, SIGNAL(image_ready(quint8*,quint32,quint32, quint32)), this, SLOT(image_dumped(quint8*,quint32,quint32, quint32)));
        connect(ah, SIGNAL(evt_image()), this, SLOT(get_finger()));

        ah->run();


    } else {
        state = 0;
        ui->connect_bt->setText("Connect");
        ui->memo->append("Disconnecting");
        ah->disconnect_network();
//        ah->terminate();
//        delete ah;
//        ah = NULL;
    }
}

void mw::starting()
{

    ui->ow_evt_en->setChecked(ah->evt_ctrl & 1);
    ui->ow_evt_en->setChecked(ah->evt_ctrl & 2);
    ui->in_evt_en->setChecked(ah->evt_ctrl & 4);
}

void mw::on_exit_bt_released()
{
    delete ah;
    exit(0);
}

void mw::on_pushButton_released()
{
    ah->get_image(current_image_format);
}

void  mw::image_dumped(quint8 * d,quint32 w ,quint32 h, quint32 size)
{
    quint8 decode_buff[1024*1280];
    quint8 *o;
    int ww = ui->frame->width();
    int wh = ui->frame->height();
    int _w, _h;

    switch (current_image_format)
    {

        case 1:
            _w = 640;
            _h = 512;
            o = d;
        break;
        case 3:
            _w = 384;
            _h = 600;
            o = d;
        break;
        case 7:
            o = d;
            _w = w;
            _h = size/w;
        break;

        case 6:
            wsq_decode(decode_buff, d, size, &_w, &_h);
            o = decode_buff;
        break;
    }


    QImage zoomed = QPixmap::fromImage(QImage(
                                              (unsigned char *) o,
                                              _w,
                                              _h,
                                      QImage::Format_Indexed8
                                      )).toImage();
//                    .scaled( ww, wh, Qt::KeepAspectRatio )
//                    .toImage();
    zoomed.save("Dump.jpg");
    ui->frame->setPixmap( QPixmap::fromImage(zoomed) );
    ah->s_control(3);
    return;
}


void mw::switch_leds() {

    static quint8 ledn = 0;
    if (++ledn == 8) ledn = 0;
    if (ledn == 0) {
        ah->led_control(0);
    } else if (ledn == 1)
    {
        ui->red_rb->setChecked(true);
    } else if (ledn == 2)
    {
        ui->green_bt->setChecked(true);
    } else if (ledn == 3)
    {
        ui->rg_led->setChecked(true);
    } else if (ledn == 4)
    {
        ui->b_led->setChecked(true);
    }
    else if (ledn == 5)
    {
        ui->rb_led->setChecked(true);
    }
    else if (ledn == 6)
    {
        ui->bg_led->setChecked(true);
    }
    else if (ledn == 7)
    {
        ui->rgb_led->setChecked(true);
    }
    led_switcher->start(1000);

}

void mw::on_auto_cb_toggled(bool checked)
{
    if (checked)
        led_switcher->start(1000);
    else
        led_switcher->stop();
}

void mw::on_red_rb_toggled(bool checked)
{
    if (checked)
        ah->led_control(LED_RED);
}

void mw::on_green_bt_toggled(bool checked)
{
    if (checked)
        ah->led_control(LED_GREEN);
}


void mw::on_wieg_bt_released()
{
    if (ui->wieg_random_cb->isChecked())
    {
        quint32 number = rand();
        ah->wieg_control(number);
    } else
    {
        ah->wieg_control(ui->wieg_et->text().toInt());
    }
}

void mw::on_b_led_toggled(bool checked)
{
    if (checked)
        ah->led_control(LED_BLUE);
}

void mw::on_rb_led_toggled(bool checked)
{
    if (checked)
        ah->led_control(LED_RB);
}

void mw::on_bg_led_toggled(bool checked)
{
    if (checked)
        ah->led_control(LED_GB);
}

void mw::on_rgb_led_toggled(bool checked)
{
    if (checked)
        ah->led_control(LED_RGB);
}

void mw::on_rg_led_toggled(bool checked)
{
    if (checked)
        ah->led_control(LED_ORANGE);
}

void mw::get_finger()
{
    ah->get_image(current_image_format);
}

void mw::on_fmt1_sb_toggled(bool checked)
{
    if (checked)
        current_image_format = 1;
}

void mw::on_fmt3_sb_toggled(bool checked)
{
    if (checked)
        current_image_format = 3;
}

void mw::on_fmt6_sb_toggled(bool checked)
{
    if (checked)
        current_image_format = 6;
}

void mw::on_fmt7_toggled(bool checked)
{
    if (checked)
        current_image_format = 7;
}


void mw::on_get_evt_bits_released()
{
    ah->custom_cmd(CMD_EVT_CONTROL, 0, 0, 0);
    start_tm->setSingleShot(true);
    start_tm->start(1000);
}


void mw::on_ow_evt_en_toggled(bool checked)
{
    if (checked)
        ah->evt_ctrl |= 1;
    else
        ah->evt_ctrl &= ~1;
    ah->custom_cmd(CMD_EVT_CONTROL, 1, ah->evt_ctrl, 0);
}

void mw::on_fp_evt_en_toggled(bool checked)
{
    if (checked)
        ah->evt_ctrl |= 2;
    else
        ah->evt_ctrl &= ~2;
    ah->custom_cmd(CMD_EVT_CONTROL, 1, ah->evt_ctrl, 0);
}

void mw::on_in_evt_en_toggled(bool checked)
{
    if (checked)
        ah->evt_ctrl |= 4;
    else
        ah->evt_ctrl &= ~4;
    ah->custom_cmd(CMD_EVT_CONTROL, 1, ah->evt_ctrl, 0);
}
