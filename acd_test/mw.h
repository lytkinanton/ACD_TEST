#ifndef MW_H
#define MW_H

#include <QMainWindow>
#include <QTimer>
#include <acd5service.h>

namespace Ui {
class mw;
}

class mw : public QMainWindow
{
    Q_OBJECT
    
public:
    ass *ah;
    QTimer * start_tm;
    QTcpServer *tcpServer;
    explicit mw(QWidget *parent = 0);
    ~mw();

    bool close();

private slots:
    void starting();

    void on_connect_bt_clicked();

    void on_exit_bt_released();

    void on_pushButton_released();

    void on_auto_cb_toggled(bool checked);

    void on_red_rb_toggled(bool checked);

    void on_green_bt_toggled(bool checked);


    void on_wieg_bt_released();



    void on_b_led_toggled(bool checked);

    void on_rb_led_toggled(bool checked);

    void on_bg_led_toggled(bool checked);

    void on_rgb_led_toggled(bool checked);

    void on_rg_led_toggled(bool checked);

    void on_fmt1_sb_toggled(bool checked);

    void on_fmt3_sb_toggled(bool checked);

    void on_fmt6_sb_toggled(bool checked);

    void on_fmt7_toggled(bool checked);

    void on_ow_evt_en_toggled(bool checked);

    void on_get_evt_bits_released();

    void on_fp_evt_en_toggled(bool checked);

    void on_in_evt_en_toggled(bool checked);

public  slots:
    void image_dumped(quint8 *data, quint32 w, quint32 h, quint32 size);
    void switch_leds();
    void get_finger();
private:
    Ui::mw *ui;
    quint8 state;
    quint8 current_image_format;
    QTimer * led_switcher;
};

#endif // MW_H
