#include "acd5service.h"
#include <QSettings>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <QFileInfo>
#include <QApplication>

char data_dump[1024*1024*2];

void ass::init_cmd(exchangePack * cmd)
{
    memset( cmd, 0, PACKSIZE);
    cmd->beginPack = FAM_START_BYTE;
    cmd->endPack = FAM_STOP_BYTE;
}

bool ass::exec_cmd(exchangePack * cmd)
{
    if (!connection_active)
        return false;

    quint16 temp = 0, i;
    quint8 *p = (quint8 *)cmd;
    for (i = 0; i < PACKSIZE-2; i++) {
        temp += (p[i]);
    }

    cmd->Sum = temp % 256;
    if (writeData(QByteArray((const char *)cmd, PACKSIZE))) {
        return true;
    } else {
        connection_active = false;
        return false;
    }
}

void ass::run()
{
    exchangePack cmd;
    has_finger = false;

    while (!connection_active)
        QApplication::processEvents();

    while (active)
    {
        QApplication::processEvents();
        while (connection_active)
        {

            QApplication::processEvents();

            if ((socket->bytesAvailable() < PACKSIZE))
                    continue;

            socket->read((char *)&cmd, PACKSIZE);

            if (!check_packet(cmd)){
                qDebug() << "error in packet";
                continue;
            }

            if ( cmd.Flag == ERR_DATA)
            {
                while ((socket->bytesAvailable() < cmd.Param2) )
                        QApplication::processEvents();
                //qDebug() << "OFF:" << cmd.Param1;
                socket->read(data_dump + cmd.Param1, cmd.Param2);
                continue;
            }

            if (cmd.Flag == ERR_LDATA) {
                qDebug() << "Image dumped completely!";
                socket->read(data_dump + cmd.Param1, cmd.Param2);
                emit image_ready((quint8 *)data_dump, cmd.Param2, 512, cmd.Param1 + cmd.Param2 );
            }



            switch (cmd.cmd)
            {
                case CMD_GET_VERSION:
                {
                    qDebug() << "version is 0x" << QString("%1").arg(cmd.Param1, 0, 16);
                    break;
                }

                case CMD_M162_CTL:
                {

                    break;
                }

                case EVT_COMMON:
                {
                    init_cmd(&cmd);
                    cmd.cmd = CMD_M162_CTL;
                    cmd.Flag = CMD162_BUZZER_CTL;
                    cmd.Param1 = 5;
                    cmd.Param2 = 0;
                    exec_cmd(&cmd);
                    cmd.cmd = CMD_M162_CTL;
                    cmd.Flag = CMD162_LED_CTL;
                    cmd.Param1 = 1;
                    cmd.Param2 = 0;
                    exec_cmd(&cmd);
                    has_finger = false;
                    break;
                }
                case EVT_OW: {

                    quint32 num = cmd.Param1;// + (cmd.Param2&0xFFFF)<<;
                    quint16 p1 = num/65536;
                    quint16 p2 = num - p1*65536;
                    qDebug() << "OW num " + QString("%1").arg(p1) + "." + QString("%1").arg(p2);
                    init_cmd(&cmd);
                    cmd.cmd = CMD_M162_CTL;
                    cmd.Flag = 100;
                    cmd.Param1 = num;
                    cmd.Param2 = 0;
                    exec_cmd(&cmd);

                    break;
                }
                case CMD_IMAGE_DUMP: {
                    qDebug() << "Image dump!";
                    if (requested_image_format > 5) // передача одним блоком
                        if (cmd.Param1 > 0)
                        {
                            qDebug() << "Receive " + QString("%1").arg(cmd.Param1) + " bytes";
                            while ((socket->bytesAvailable() < cmd.Param1) )
                                    QApplication::processEvents();

                            socket->read(data_dump, cmd.Param1);

                            emit image_ready((quint8 *)data_dump, cmd.Param2, 512, cmd.Param1);
                             qDebug() << "DONE!";
                        }


                break;
                }

                case EVT_VALID_IMAGE:  {
                    emit evt_image();
                    qDebug() << "Get finger evt";
                    break;
                }

                case CMD_EVT_CONTROL:
                {
                    qDebug() << "Receive " + QString("%1").arg(cmd.Param2) + " option bits";
                    evt_ctrl = cmd.Param2 & 0xFF;
                    break;
                }

                default: {


                }
            }
        }

    }

    socket->deleteLater();
}

ass::ass(QObject *parent) :
    QThread(parent)
{
    connection_active = data_stream = false;
    socket = new QTcpSocket();
    alive_timer = new QTimer();
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
//    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    requested_image_format = 3;
}

ass::~ass()
{
    active = false;
    connection_active = false;
    sleep(5);
}

bool ass::start(QString ip, int port)
{
    active = true;
    host_ip = ip;
    host_port = port;
    socket->connectToHost(ip, port);

    return socket->waitForConnected();
}



bool ass::writeData(QByteArray data)
{
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        socket->write(data); //write the data itself
        return socket->waitForBytesWritten();
    }
    else
        return false;
}

void ass::readyRead()
{
    qDebug() << "reading...";
}

void ass::connected()
{
    connection_active = true;
    qDebug() << " Connected!";
    connect(alive_timer, SIGNAL(timeout()), this, SLOT(timer_rt()));

    alive_timer->setSingleShot(true);
    alive_timer->start(5000);
}

void ass::disconnected()
{
    qDebug() << " Disconnected";
    connection_active = false;
    sleep(1);
//    socket->deleteLater();
}

void ass::timer_rt()
{
    exchangePack cmd;
    init_cmd(&cmd);
    cmd.cmd = CMD_GET_VERSION;
    cmd.Flag = 0;
    cmd.Param1 = 0;
    cmd.Param2 = 0;
    if (exec_cmd(&cmd))
        alive_timer->start(5000);
    return;
}

void ass::offtimer_rt()
{

    if ( !has_finger ) {
      //  led_control(a5c.standbyLed);
        has_finger = 0;
    }

}

bool ass::check_packet(exchangePack cmd)
{
    quint16 temp = 0, i;
    quint8 *p = (quint8 *)&cmd;
    for (i = 0; i < PACKSIZE-2; i++) {
        temp += (p[i]);
    }

    quint16 crc = temp % 256;

    if (crc != p[11]) {
        qDebug() << "Error checksum";
        return false;
    }

    return true;
}

void ass::s_control(int num)
{

    exchangePack cmd;
    init_cmd(&cmd);
    cmd.cmd = CMD_M162_CTL;
    cmd.Flag = CMD162_BUZZER_CTL;
    cmd.Param1 = num;
    cmd.Param2 = 0;

    exec_cmd(&cmd);

}

void ass::led_control(int num)
{
    exchangePack cmd;
    init_cmd(&cmd);
    cmd.cmd = CMD_M162_CTL;
    cmd.Flag = CMD162_LED_CTL;
    cmd.Param1 = num;
    cmd.Param2 = 0;
    exec_cmd(&cmd);
}

void ass::wieg_control(quint8 num)
{
    exchangePack cmd;
    init_cmd(&cmd);
    cmd.cmd = CMD_M162_CTL;
    cmd.Flag = 100;
    cmd.Param1 = num;
    cmd.Param2 = 0;
    exec_cmd(&cmd);
}

void ass::turnstile_control(int num)
{
    exchangePack cmd;
    init_cmd(&cmd);
    cmd.cmd = CMD_M162_CTL;
    cmd.Flag = CMD162_TURNSTILE_CTL;
    cmd.Param1 = num;
    cmd.Param2 = 0;
    exec_cmd(&cmd);
}

quint8 ass::get_image(quint8 format)
{
    exchangePack cmd;
    init_cmd(&cmd);
    requested_image_format = format;
    cmd.cmd = CMD_IMAGE_DUMP;
    cmd.Flag = format;    
    cmd.Param1 = 0;
    cmd.Param2 = 0;
    exec_cmd(&cmd);
    return 0;
}

void ass::disconnect_network()
{
    alive_timer->stop();
    connection_active = false;
    active = false;
    socket->disconnect();
}

void ass::custom_cmd(quint8 cmd, quint32 p1, quint32 p2, quint8 flag)
{
    exchangePack c;
    init_cmd(&c);
    c.cmd = cmd;
    c.Flag = flag;
    c.Param1 = p1;
    c.Param2 = p2;
    exec_cmd(&c);
    return;
}
