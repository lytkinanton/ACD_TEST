#ifndef ACD_PROTOCOL_V2_H
#define ACD_PROTOCOL_V2_H


#define	FAM_ERR_BASE (int)0x00

#define CMD_REMOTE_CAM_SELECT	200
#define CMD_REMOTE_CAM_READ     201
#define CMD_REMOTE_CAM_WRITE	202

#define CMD_IMAGE_DUMP		68//0x44
#define CMD_EVT_CONTROL         81

#define CMD_IMAGE_DUMP2		69
#define CMD_CHECK_FINGER	70
#define	CMD_M162_CTL		131
#define CMD_REBOOT			135
#define CMD_SET_GAIN		116
#define CMD_SET_HIGHLIGHT	117
#define CMD_SET_OFFSET		118

#define CMD_UPLOAD_DATA			122

#define CMD_FLASH				10
#define CMD_GET_DATA_ADDRESS 	11
#define CMD_SLEEP               12

#define CMD_FLASH_WRITE		123
#define CMD_FLASH_READ		124

#define CMD_READ_ROM		133
#define CMD_WRITE_ROM		134
#define CMD_GET_TIME		137//0x89
#define CMD_SET_TIME		138//0x8a

#define EVT_VALID_IMAGE		144
#define EVT_M162			146
#define EVT_DATA			147
#define EVT_OW				148
#define EVT_LOG				149
#define EVT_KEY				151
#define EVT_COMMON			152

#define CMD_FAM_SETUP		7
#define CMD_GET_SETTINGS	132//0x84

#define ERR_EVT_FINGER		0x01
#define ERR_EVT_KEY			0x02
#define ERR_EVT_OW1			0x03
#define ERR_EVT_OW2			0x04

#define CMD_GET_VERSION			1
#define CMD_DEBUG				5
#define CMD_SETUP				6
#define ARG_READ				1
#define ARG_WRITE				2
#define PR_IMGMODE				1
#define PR_IRQMODE				2
#define PR_BEEPERMODE			3
#define CMD_IMAGE_DUMP			68
#define CMD_CHECK_FINGER		70
#define CMD_GETTICKCOUNT		80


#define CMD162_OFFSET		50

#define	CMD162_LED_CTL		1 + CMD162_OFFSET

enum {
    LED_OFF,
    LED_GREEN,
    LED_RED,
    LED_ORANGE,
    LED_BLUE,
    LED_RB,
    LED_GB,
    LED_RGB

};

#define CMD162_PRINT		3 + CMD162_OFFSET
#define	CMD162_BUZZER_CTL	2 + CMD162_OFFSET
#define CMD162_EXDEV_INIT	4 + CMD162_OFFSET

#define CMD162_LOCK_CTL		6 + CMD162_OFFSET
#define CMD162_TURNSTILE_CTL	7 + CMD162_OFFSET

#define CAM_W 1280
#define CAM_H 1024

#define DOOR_LOCK_CMD           1
#define DOOR_UNLOCK_CMD         2
#define DOOR_PASS_CMD           3
#define DOOR_GETS_CMD           4
#define DOOR_FLOCK_CMD          5

enum A5_ERR
{
    ERR_OK                  =	0,
    ERR_ERROR               =	(FAM_ERR_BASE -1),
    ERR_BAD_ARGUMENT        =	(FAM_ERR_BASE -2),
    ERR_CRC_ERROR           =	(FAM_ERR_BASE -3),
    ERR_UNKNOWN_COMMAND     =   (FAM_ERR_BASE -4),
    ERR_INVALID_STOP_BYTE   =	(FAM_ERR_BASE -5),
    ERR_I2C_ERROR           =   (FAM_ERR_BASE -6),
    ERR_NO_VALID_IMAGE      =	(FAM_ERR_BASE -7),
    ERR_TOUT                =	(FAM_ERR_BASE -8),
    ERR_QUE_OVERFLOW        =	(FAM_ERR_BASE -9)
};


#define SERV_PORT 5000
#define CAM1_PORT 5001
#define CAM2_PORT 5002

#define EVT_DATA				147//0x93
#define	FAM_START_BYTE			0x40
#define	FAM_STOP_BYTE			0x0D

#define	ERR_1OK							(FAM_ERR_BASE + 0x40)
#define ERR_DATA						(FAM_ERR_BASE + 0x51)
#define ERR_LDATA						(FAM_ERR_BASE + 0x52)

#define RCV_MAXSIZE (1024*1280)

enum {
    TURN_CMD_UNLOCK_A = 1,
    TURN_CMD_UNLOCK_B,
    TURN_CMD_UNLOCK_AB,
    TURN_CMD_STOP,
    TURN_CMD_PASS_A,
    TURN_CMD_PASS_B,
    TURN_CMD_TIMEOUT,
    TURN_CMD_GET_STATE
};

enum ext_devices {
    NO_DEVICE = 0,
    LOCKER,
    TURNIKET
};

struct acdPv2Setup
{
    unsigned int contrLimit;
    unsigned int contrDelta;
};

#pragma pack(1)

struct exchangePack
{
    unsigned char beginPack;	//	sign of begin "own" packet
    unsigned char cmd;			//	command code
    int Param1;	//	1st Parameter
    int Param2;	//	2nd Parameter
    unsigned char Flag;			//	Code of Flag/Error
    unsigned char Sum;			//	sum of bytes (beginPack + cmd + Param1[0] + Param2[0] + Flag) / 0x100
    unsigned char endPack;		//	sign of end "own" packet
};

#define PACKSIZE sizeof(exchangePack)

#endif // ACD_PROTOCOL_V2_H
